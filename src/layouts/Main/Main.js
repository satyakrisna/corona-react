import React, { useState, useEffect, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/styles';
import { useMediaQuery, Fab, Tooltip, Badge, Hidden, Popover, Typography, ButtonBase, Dialog, DialogContentText, DialogContent } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import TimeAgo from 'react-timeago'
import notification from 'services/notification';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { NavLink as RouterLink } from 'react-router-dom';
import auth from 'services/auth'
import { useIdleTimer } from 'react-idle-timer'

import { Sidebar, Footer } from './components';

const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    [theme.breakpoints.up('sm')]: {
      // paddingTop: 64
    }
  },
  shiftContent: {
    paddingLeft: 240
  },
  row: {
    marginLeft: theme.spacing(3),
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(3),
    marginRight: theme.spacing(3)
  },
  spacer: {
    flexGrow: 1
  },
  top: {
    background: 'linear-gradient(45deg, rgba(0,65,195,1) 0%, rgba(0, 51, 153,1) 49%)',
    // backgroundColor: "#0041C3",
    color: "#FFFFFF",
    transition: "all ease 0.8s",
    '&:hover' : {
      background: '#CCCCCC'
    },
  },
  content: {
    height: '100%'
  },
  flash: {
    height : 200
  },
  hover: {
    '&:hover': {
      backgroundColor: '#FBFCFD'
    }
  },
  signOutButton: {
    marginLeft: theme.spacing(1),
    // backgroundColor: "#0041C3",
    background: 'linear-gradient(45deg, rgba(0,65,195,1) 0%, rgba(0, 51, 153,1) 49%)',
    color: "#FFFFFF",
    transition: "all ease 0.8s",
    '&:hover' : {
      background: '#CCCCCC'
    },
  }
}));

const Main = props => {
  const { children } = props;
  const history = useHistory();

  const [notifications, setNotifications] = useState([]);
  const [notifs, setNotifs] = useState([]);
  const [count, setCount] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;
  const rank_level = JSON.parse(localStorage.getItem("data")).rank_level;
  const user_uuid = JSON.parse(localStorage.getItem("data")).user_uuid;

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        if(err.response.status === 401) {
          localStorage.clear()
          history.push("/")
        }
    })
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleClick = (event) => {
    notification({
      method: 'PUT',
      url: '/api/notification',
      data: notifs,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
     setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  useEffect(() => {
      var interval = setInterval(() => {
        if(localStorage.getItem("data")) {
          notification.get(`/api/notification/${user_uuid}?admin=1`, {
          headers: {
            'Authorization': `Bearer ${access_token}`
          }
        }).then(res =>{
          const data = res.data.output_schema.notification
          setNotifications(data.content);
          
        }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleError401("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleError401(message)
            } else{
              const message = err.response.data.error_schema.error_message.indonesian
              handleError(message)
            }
          }else{
            handleError401("Something went wrong")
          }
        })

        notification.get(`/api/notification/${user_uuid}?read=false&size=9999&admin=1`, {
          headers: {
            'Authorization': `Bearer ${access_token}`
          }
        }).then(res =>{
          const data = res.data.output_schema.notification
          setCount(data.total_elements);
          setNotifs(data.content);
        }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleError401("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleError401(message)
            } else{
              const message = err.response.data.error_schema.error_message.indonesian
              handleError(message)
            }
          }else{
            handleError401("Something went wrong")
          }
        })
      } else {
        clearInterval(interval)
      }
    }  
    , 5000)
    
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const classes = useStyles();
  const theme = useTheme();
  const isDesktop = useMediaQuery(theme.breakpoints.up('lg'), {
    defaultMatches: true
  });

  const [openSidebar, setOpenSidebar] = useState(false);

  const handleSidebarOpen = () => {
    setOpenSidebar(true);
  };

  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  const handleOnIdle = event => {
    handleError401("YOU SEEM INACTIVE")
  }

  const shouldOpenSidebar = isDesktop ? true : openSidebar;

  const CustomRouterLink = forwardRef((props, ref) => (
    <div
      ref={ref}
      style={{ flexGrow: 1 }}
    >
      <RouterLink {...props} />
    </div>
  ));

  const buttonBase = notif => {
    let url, title;
    title = notif.title.split(";")

    if(title[1] === "VCREQ") {
      url = `/admin/virtual-card-request-ongoing?${title[2]}`
    } else if(title[1] === "REIMBURSEREQ") {
      url = `/admin/reimburses-ongoing?${title[2]}`
    } else if(title[1] === "TRXQR") {
      url = `/admin/transactions?${title[2]}`
    } else if(title[1] === "REPORTVC") {
      url = `/admin/virtual-card?${title[2]}`
    }

    return (
      <ButtonBase 
        component={CustomRouterLink}
        to={`${url}`} 
        key={notif.uuid}
        className={classes.hover} onClick={handleClose}>
          <div className={classes.notifications}>
          <div className={classes.row}>
            <img src={'/images/logos/logo.png'} alt='logo' style={{ width: 40, marginRight: 15 }}/>
              <Typography style={{ width: 400}}>{notif.message}</Typography>
          </div>
          <div className={classes.row} style={{marginTop: 5}}>
                <span className={classes.spacer} />
                <AccessTimeIcon style={{ fontSize: 12, marginRight: 5 }}/>
                <Typography variant="caption" style={{alignText:'right'}}><TimeAgo date={notif.created_date} /></Typography>
              </div>
        </div>
      </ButtonBase>
    )
  }

  const {time} = useIdleTimer({
    timeout: rank_level === 4 ? 1000 * 60 * 5 : 1000 * 60 * 30,
    onIdle: handleOnIdle,
    debounce: 500
  })

  return (
    <div
      className={clsx({
        [classes.root]: true,
        [classes.shiftContent]: isDesktop
      })}
    >
      <Sidebar
        onClose={handleSidebarClose}
        open={shouldOpenSidebar}
        variant={isDesktop ? 'persistent' : 'temporary'}
      />
      <main className={classes.content}>
        <div className={classes.row}>
          <Hidden lgUp>
            <Fab size="medium" onClick={handleSidebarOpen} className={classes.top} aria-label="open">
              <MenuIcon />
            </Fab>
          </Hidden>
          <span className={classes.spacer} />
          <Fab size="medium" className={classes.top} aria-label="open" onClick={handleClick}>
          <Tooltip title="Notification" placement="bottom">
              <Badge
                badgeContent={count}
                color="secondary"
              >
                <NotificationsIcon />
              </Badge>
          </Tooltip>
            </Fab>
            <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
          >
            {notifications.length===0?
             <div className={classes.notifications}>
               <div className={classes.row}>
                <Typography>No new notification</Typography>
               </div>
             </div>
             :
             <PerfectScrollbar>
                {
                  notifications.map(notif=>(buttonBase(notif)))
                }
              <ButtonBase 
               component={CustomRouterLink}
               to={'notifications'} 
               className={classes.hover} onClick={handleClose} style={{width:'100%'}}>
                  <Typography variant='body1' style={{textAlign: 'center', marginTop: 5, marginBottom: 5}}>Show all notifications</Typography>
              </ButtonBase>
            </PerfectScrollbar>
            }
          </Popover>
            <Fab size="medium" className={classes.signOutButton} aria-label="open" onClick={handleLogout}>
            <Tooltip title="Logout" placement="bottom">
              <MeetingRoomIcon />
          </Tooltip>
            </Fab>
        </div>
        {children}
        <Dialog
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
        <Footer />
      </main>
    </div>
  );
};

Main.propTypes = {
  children: PropTypes.node
};

export default Main;
