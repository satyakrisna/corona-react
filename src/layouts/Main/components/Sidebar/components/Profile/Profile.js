import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content'
  },
  avatar: {
    width: 60,
    height: 60,
    marginBottom: 5
  },
  name: {
    color: "#FFFFFF",
    marginTop: theme.spacing(0.5),
    backgroundColor: "rgba(16, 20, 35, 0.6)",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    textTransform: "uppercase",
    borderRadius: "5px"
  },
  hello: {
    color: "#FFFFFF",
    marginTop: theme.spacing(1),
    fontWeight: 400,
    fontFamily: "Poppins"
  },
  role: {
    color: "#FFFFFF",
    marginTop: theme.spacing(0.5),
    textTransform: "uppercase",
    fontFamily: "Poppins"
  },
  icon : {
    color: "#FFFFFF",
    fontSize: "75px"
  }
}));

const Profile = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

  let role;
  if(rank_level === 4) {
    role = "ADMIN MASTER"
  } else {
    role = "ADMIN DELEGATE"
  }

  const user = {
    name: JSON.parse(localStorage.getItem("data")).name,
    avatar: '/images/avatars/avatar_11.png',
    bio: role
  };

  return (
    <div
      {...rest}
      className={clsx(classes.root, className)}
    >
      <img src="/images/icon flat4.png" 
        width="100"
        style={{marginLeft: 'auto', marginRight: 'auto', marginTop: '30px', marginBottom: '30px'}}
        alt="Welcome"/>

      <Typography
        className={classes.name}
        variant="h5"
        style={{textAlign: 'center'}}
      >
        {user.name}
      </Typography>

      <Typography 
      className={classes.role}
      variant="body2">
        {user.bio}
      </Typography>
    </div>
  );
};

Profile.propTypes = {
  className: PropTypes.string
};

export default Profile;
