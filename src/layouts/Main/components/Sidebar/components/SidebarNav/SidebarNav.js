/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import React, { useState, forwardRef, Fragment } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { List, ListItem, Button, Collapse } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

const useStyles = makeStyles(theme => ({
  root: {},
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0
  },
  button: {
    color: "#FFFFFF",
    marginBottom: '3%',
    padding: '10px 8px',
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
    fontWeight: '400',
    '&:hover' : {
      backgroundColor: "rgba(255,255,255,0.1)"
    }
  },
  icon: {
    color: "#FFFFFF",
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    marginRight: theme.spacing(4)
  },
  active: {
    backgroundColor: "rgba(255,255,255,0.4)",
    '& $icon': {
      color: "#FFFFFF"
    },
    '&:hover' : {
      backgroundColor: "rgba(255,255,255,0.4)"
    }
  },
  nested: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
    '& $icon': {
      // color: theme.palette.primary.main
      color: "#FFFFFF"
    },
    paddingLeft: theme.spacing(2),
  },
  nested2: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
    '& $icon': {
      // color: theme.palette.primary.main
      color: "#FFFFFF"
    },
    paddingLeft: theme.spacing(4),
  },
  childIcon: {
    float: 'right',
  },
  spacer: {
    flexGrow: 1
  }
}));

const CustomRouterLink = forwardRef((props, ref) => (
  <div
    ref={ref}
    style={{ flexGrow: 1 }}
  >
    <RouterLink {...props} />
  </div>
));

const SidebarNav = props => {
  const { pages, className, onClose, ...rest } = props;

  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [clicked, setClicked] = useState('');

  const handleClick = (title) => {
    setClicked(title)
    if(clicked === title) {
      setOpen(!open);
    } else {
      setOpen(true);
    }
  };
  const handleClick1 = () => {
    setOpen1(!open1);
  };

  const classes = useStyles();
  
  return (
    <List
      {...rest}
      className={clsx(classes.root, className)}
    >
      {pages.map(page => page.children ? 
      (
        <Fragment key={page.title}>
          <ListItem
            className={classes.item}
            disableGutters
          >
            <Button
              className={classes.button}
              onClick={() => handleClick(page.title)}
            >
              <div className={classes.icon}>{page.icon}</div>
              {page.title}
              <span className={classes.spacer} />
              {open ? (page.title === clicked ? <ExpandLessIcon className={classes.childIcon}/> : <ExpandMoreIcon className={classes.childIcon}/>) : (page.title === clicked ? <ExpandMoreIcon className={classes.childIcon}/> : <ExpandMoreIcon className={classes.childIcon}/>)}
            </Button>
          </ListItem>
          <Collapse in={open} timeout="auto" unmountOnExit>
          {
            page.title === clicked ? 
            (<List component="div" disablePadding>

            {
              page.children.map(child => child.grandchildren ? 
                (
                  
                  <Fragment key={child.title}>
                  <ListItem
                    className={classes.nested}
                    disableGutters
                    key={child.title}
                  >
                    <Button
                      className={classes.button}
                      onClick={handleClick1}
                    >
                      <div className={classes.icon}>{child.icon}</div>
                      {child.title}
                      <span className={classes.spacer} />
                      {open1 ? <ExpandLessIcon className={classes.childIcon}/> : <ExpandMoreIcon className={classes.childIcon}/>}
                    </Button>
                  </ListItem>
                  <Collapse in={open1} timeout="auto" unmountOnExit>
                  
                  <List component="div" disablePadding>
                    {
                      child.grandchildren.map(grandChild => 
                        (
                          <ListItem
                            className={classes.nested2}
                            disableGutters
                            key={grandChild.title}
                          >
                            <Button
                              activeClassName={classes.active}
                              className={classes.button}
                              component={CustomRouterLink}
                              to={grandChild.href}
                            >
                              <div className={classes.icon}>{grandChild.icon}</div>
                              {grandChild.title}
                            </Button>
                          </ListItem>
                        )
                      )
                    }
                  </List>
                  </Collapse>
                  </Fragment>
                )
                :
                (
                  <ListItem
                    className={classes.nested}
                    disableGutters
                    key={child.title}
                  >
                    <Button
                      activeClassName={classes.active}
                      className={classes.button}
                      component={CustomRouterLink}
                      to={child.href}
                    >
                      <div className={classes.icon}>{child.icon}</div>
                      {child.title}
                    </Button>
                  </ListItem>
                )
              )
            }
  
            </List>)
            :
            (<div></div>)
          }
        </Collapse>

        </Fragment>
      )
      :
      (
        <Fragment key={page.title}>
          <ListItem
            className={classes.item}
            disableGutters
            key={page.title}
          >
            <Button
              activeClassName={classes.active}
              className={classes.button}
              component={CustomRouterLink}
              to={page.href}
              onClick={handleClick}
            >
              <div className={classes.icon}>{page.icon}</div>
              {page.title}
            </Button>
          </ListItem>
        </Fragment>
      )
      )}
      
    </List>
  );
};

SidebarNav.propTypes = {
  className: PropTypes.string,
  pages: PropTypes.array.isRequired
};

export default SidebarNav;
