import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Divider, Drawer } from '@material-ui/core';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import HistoryIcon from '@material-ui/icons/History';
import BranchIcon from '@material-ui/icons/StoreMallDirectory';
import TransactionIcon from '@material-ui/icons/Money';
import CategoryIcon from '@material-ui/icons/Category';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import DescriptionIcon from '@material-ui/icons/Description';
import DashboardIcon from '@material-ui/icons/Dashboard';
import PeopleIcon from '@material-ui/icons/People';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import ReceiptIcon from '@material-ui/icons/Receipt';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import SettingsIcon from '@material-ui/icons/Settings';
import { Scrollbars } from 'react-custom-scrollbars';

import { Profile, SidebarNav} from './components';

const useStyles = makeStyles(theme => ({
  drawer: {
    width: 240,
    // backgroundImage: `url("/images/circles2.png")`,
    background: 'linear-gradient(45deg, rgba(0,65,195,1) 0%, rgba(0, 51, 153,1) 49%)',
    backgroundSize: 'cover',
    backgroundPosition: "center center",
    [theme.breakpoints.up('lg')]: {
      height: '100%'
    }
  },
  root: {
    display: 'flex',
    flexDirection: 'column',
    padding: theme.spacing(2),
    minHeight: '100%'
  },
  divider: {
    margin: theme.spacing(2, 0),
    opacity: '0.2'
  },
  nav: {
    marginBottom: theme.spacing(2)
  },
  group: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    minHeight: 'fit-content',
    // minHeight: '100%',
    color: "#FFFFFF",
  },
  text: {
    color: "#FFFFFF",
    fontFamily: "Poppins !important"
  }
}));

const Sidebar = props => {
  const { open, variant, onClose, className, ...rest } = props;

  const classes = useStyles();
  const rank_level = JSON.parse(localStorage.getItem("data")).rank_level
  let pages = [];
  if(rank_level!==4){
    pages = [
      {
        title: 'Dashboard',
        href: '/admin/dashboard',
        icon: <DashboardIcon />
      },
      {
        title: 'Reimburse',
        href: '/#',
        open: false,
        icon: <ReceiptIcon />,
        children: [
          {
            title: 'On Going',
            href: '/admin/reimburses-ongoing',
            icon: <AccessTimeIcon />
          },
          {
            title: 'Finished',
            href: '/admin/reimburses-finished',
            icon: <CheckCircleOutlineIcon />
          }
        ]
      },
      {
        title: 'Virtual Card',
        href: '/#',
        open: false,
        icon: <CreditCardIcon />,
        children: [
          {
            title: 'Request',
            href: '/#',
            icon: <DescriptionIcon />,
            grandchildren: [
              {
                title: 'On Going',
                href: '/admin/virtual-card-request-ongoing',
                icon: <AccessTimeIcon />
              },
              {
                title: 'Finished',
                href: '/admin/virtual-card-request-finished',
                icon: <CheckCircleOutlineIcon />
              }
            ]
          },
          {
            title: 'Released',
            href: '/admin/virtual-card',
            icon: <CreditCardIcon />
          }
        ]
      },
      {
        title: 'Transaction',
        href: '/admin/transactions',
        icon: <TransactionIcon />
      },
      {
        title: 'Settings',
        href: '/admin/settings',
        icon: <SettingsIcon />
      }
    ];
  }else{
    pages = [
      {
        title: 'Dashboard',
        href: '/admin/dashboard',
        icon: <DashboardIcon />
      },
      {
        title: 'Reimburse',
        href: '/#',
        open: false,
        icon: <ReceiptIcon />,
        children: [
          {
            title: 'On Going',
            href: '/admin/reimburses-ongoing',
            icon: <AccessTimeIcon />
          },
          {
            title: 'Finished',
            href: '/admin/reimburses-finished',
            icon: <CheckCircleOutlineIcon />
          }
        ]
      },
      {
        title: 'Virtual Card',
        href: '/#',
        open: false,
        icon: <CreditCardIcon />,
        children: [
          {
            title: 'Request',
            href: '/#',
            icon: <DescriptionIcon />,
            grandchildren: [
              {
                title: 'On Going',
                href: '/admin/virtual-card-request-ongoing',
                icon: <AccessTimeIcon />
              },
              {
                title: 'Finished',
                href: '/admin/virtual-card-request-finished',
                icon: <CheckCircleOutlineIcon />
              }
            ]
          },
          {
            title: 'Released',
            href: '/admin/virtual-card',
            icon: <CreditCardIcon />
          }
        ]
      },
      {
        title: 'Transaction',
        href: '/admin/transactions',
        icon: <TransactionIcon />
      },
      {
        title: 'Category',
        href: '/admin/categories',
        icon: <CategoryIcon />
      },
      {
        title: 'User',
        href: '/admin/users',
        icon: <PeopleIcon />
      },
      {
        title: 'Division',
        href: '/admin/divisions',
        icon: <GroupWorkIcon />
      },
      {
        title: 'Branch',
        href: '/admin/branches',
        icon: <BranchIcon />
      },
      {
        title: 'Payment',
        href: '/#',
        open: false,
        icon: <AccountBalanceWalletIcon />,
        children: [
          {
            title: 'Bill',
            href: '/admin/payment',
            icon: <CreditCardIcon />
          },
          {
            title: 'History',
            href: '/admin/payment-history',
            icon: <HistoryIcon />
          }
        ]
      },
      {
        title: 'Settings',
        href: '/admin/settings',
        icon: <SettingsIcon />
      }
    ];
  }
  

  return (
    <Drawer
      anchor="left"
      classes={{ paper: classes.drawer }}
      onClose={onClose}
      open={open}
      variant={variant}
    >
      <Scrollbars>
      <div
        {...rest}
        className={clsx(classes.root, className)}
      >
        <Profile />
        <Divider className={classes.divider} />
        <SidebarNav
          onClose={onClose}
          className={classes.nav}
          pages={pages}
        />
      </div>
      </Scrollbars>
    </Drawer>
  );
};

Sidebar.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired
};

export default Sidebar;
