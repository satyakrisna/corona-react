import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { useHistory } from 'react-router-dom';
import transaction from 'services/transaction'
import moment from 'moment';
import { VirtualCardToolbar, VirtualCardTable } from './components';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography, Card, Box, CardContent,
  Grid, Tooltip, IconButton
} from '@material-ui/core';

import CreditCardIcon from '@material-ui/icons/CreditCard';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";
import auth from 'services/auth'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    // backgroundImage: `url("/images/danger.png")`,
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    // background: "linear-gradient(45deg, rgba(47,59,173,1) 0%, rgba(38,49,140,1) 49%)",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2)
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  }
}));

const VirtualCardList = () => {
  const classes = useStyles();
  const history = useHistory();

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const [virtualcards, setVirtualCards] = useState([]);
  const [exportData, setExportData] = useState([]);
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [keyword, setKeyword]=useState("");
  const [division, setDivision]=useState('');
  const [branch, setBranch]=useState('');
  var generated_date = moment(new Date()).format('DD/MM/YYYY');
  var date = new Date();
  var today = date.toISOString().substr(0,10);
  var lastMonth = moment().subtract(1, 'months').toISOString().substr(0,10);
  const [start, setStart]=useState(lastMonth);
  const [end, setEnd]=useState(today);
  const [orderBy, setOrderBy]=useState('modify_date');
  const [direction, setDirection]=useState('desc');

  // useEffect(()=>{
  //   if(location.state === null) {
      
  //   } else {
  //     if(location.state.success === true) {
  //       handleSuccess(location.state.msg)
  //     }
  //   }
  // },[])

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleErrorLogout = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;

  const getData = () => {
    transaction.get(`/api/admin/virtual-card?size=${size}&page=${page}&search=${keyword}&division=${division}&branch=${branch}&start=${start}&end=${end}&order_by=${orderBy}&direction=${direction}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const baseData = res.data.output_schema
        setVirtualCards(baseData.virtual_cards.content)
        setCount(baseData.virtual_cards.total_elements)
        setPages(baseData.virtual_cards.total_pages)
        setLoading(false)
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })

     transaction.get(`/api/admin/virtual-card?size=99999&page=0&search=${keyword}&division=${division}&branch=${branch}&start=${start}&end=${end}&order_by=${orderBy}&direction=${direction}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const baseData = res.data.output_schema
        setExportData(baseData.virtual_cards.content)
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })
  }

  useEffect(()=>{
    document.title = "Corona | Virtual Card List"
    getData()
    setLoading(true)
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[size,page,keyword,division,branch,start,end,orderBy,direction])

  const keywordChange = (word) => {
    setKeyword(word)
  }

  const filterChange = (division, branch, start, end, orderBy, direction) => {
    setDivision(division);
    setBranch(branch);
    setStart(start);
    setEnd(end);
    setOrderBy(orderBy);
    setDirection(direction);
  }

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "landscape";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    const title = "Virtual Card";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Card Number", "Employee Name", "Limit", "Used Amount",  "Start Date", "End Date", "Active"]];
    const date = `Generated on ${generated_date}`;

    const data = exportData.map(data=> [data.card_number, data.name, data.limit_transaction, data.limit_used, moment(data.start_date).format('DD/MM/YYYY'), moment(data.end_date).format('DD/MM/YYYY'), data.is_active? "Active": "Nonactive"]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };
    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 740, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`virtual_card_list_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
      <Grid>      
      <div className={classes.package}>
          <Box boxShadow={5} className={classes.row}>
            <Typography variant="h2" className={classes.iconColor}>
                <CreditCardIcon className={classes.icon}/>
              </Typography>
          </Box>
            <Typography variant="h4" className={classes.title} >
              Virtual Card
            </Typography>
        </div>
      <Card style={{paddingTop: '45px'}}>
        
        <CardContent>
        <div className={classes.export}>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`virtual_card_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="virtual_card_list">
          <ExcelColumn label="Card Number" value="card_number"/>
          <ExcelColumn label="Employee Name" value="name"/>
          <ExcelColumn label="Limit" value="limit_transaction"/>
          <ExcelColumn label="Used Amount" value="limit_used"/>
          <ExcelColumn label="Start Date" value="start_date"/>
          <ExcelColumn label="End Date" value="end_date"/>
          <ExcelColumn label="Active" value="status"/>
        </ExcelSheet>
      </ExcelFile>
    </div>
      <VirtualCardToolbar handleSuccess={handleSuccess} handleError={handleError} handleErrorLogout={handleErrorLogout} getData={getData}  onKeywordChange={keywordChange}  onFilterChange={filterChange} onLoadingChange={setLoading} onPageChange={setPage}/>
      <div className={classes.content}>
        <VirtualCardTable virtualcards={virtualcards} handleSuccess={handleSuccess} handleError={handleError} handleErrorLogout={handleErrorLogout} getData={getData} loading={loading}  onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages}/>
      </div>
      <Dialog
              onClose={handleFlashClose}
              open={openFlash}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogContent>
                <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
                  <Typography variant="h2" style={{textAlign:'center'}}>
                    Success!
                  </Typography>
                <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
                  {message}
                </DialogContentText>
              </DialogContent>
            </Dialog>

            <Dialog
          onClose={handleFlashErrClose}
          open={openFlashErr}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          >
          <DialogContent>
              <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
              <Typography variant="h2" style={{textAlign:'center'}}>
                  Error!
              </Typography>
              
              <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
              {message}
              </DialogContentText>
          </DialogContent>
        </Dialog>
        </CardContent>
      </Card>
    </Grid>
    </div>
  );
};

export default VirtualCardList;
