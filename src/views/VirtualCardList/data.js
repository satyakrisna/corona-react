import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7890',
    employee_id: 'u068725',
    name: 'Willy Mardianto',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'LIVE'
  },
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7891',
    employee_id: 'u06876',
    name: 'Satya Krisna',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'DEACTIVATE'
  },
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7892',
    employee_id: 'u068727',
    name: 'Stefanus Anugrah Pribadi',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'LIVE'
  },
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7893',
    employee_id: 'u068728',
    name: 'Ronaldo Kristianto',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'LIVE'
  },
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7894',
    employee_id: 'u068729',
    name: 'Nurul Khotimah',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'LIVE'
  },
  {
    id: uuid(),
    vc_id: '5432 4812 3456 7895',
    employee_id: 'u068730',
    name: 'Nicola',
    used_amount: '500000',
    limit: '5000000',
    startDate: 1555016400000,
    endDate: 1555016400000,
    createDate: 1555016400000,
    status: 'LIVE'
  },
];
