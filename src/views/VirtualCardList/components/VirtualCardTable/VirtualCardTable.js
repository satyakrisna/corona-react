import React, { useState, Fragment, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';
import PropTypes from 'prop-types';
import 'react-credit-cards/es/styles-compiled.css';
import Cards from 'react-credit-cards';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles, withStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Select,
  MenuItem,
  Typography,
  IconButton,
  LinearProgress
} from '@material-ui/core';
import { StatusBullet } from 'components';
import transaction from 'services/transaction';
import CurrencyFormat from 'react-currency-format';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import TimelineIcon from '@material-ui/icons/Timeline';
import PaymentIcon from '@material-ui/icons/Payment';
import validate from 'validate.js';

const schema = {
  note: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
};

const BorderLinearProgress = withStyles((theme) => ({
    root: {
      height: 10,
      borderRadius: 5,
    },
    colorPrimary: {
      backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
      borderRadius: 5,
      backgroundColor: '#1a90ff',
    },
  }))(LinearProgress);

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
      padding: 0
    },
    inner: {
      minWidth: 1050
    },
    nameContainer: {
      display: 'flex',
      alignItems: 'center'
    },
    avatar: {
      marginRight: theme.spacing(2)
    },
    active: {
      marginTop: theme.spacing(1)
    },
    status: {
      marginRight: theme.spacing(1)
    },
    actions: {
      justifyContent: 'flex-end'
    },
    row: {
        height: '42px',
        display: 'flex',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    },
    spacer: {
      flexGrow: 1
    },
    table: {
      width: "25%",
      backgroundColor: '#FAFAFA'
    },
    pagination:{
      marginLeft: theme.spacing(3)
    }
}));

const statusColors = {
    Active: 'success',
    Nonactive: 'danger'
  };

const VirtualCardTable = props => {
    const { virtualcards, getData, handleSuccess, handleError, handleErrorLogout, loading, size, page, count, pages } = props;
    const classes = useStyles();
    const location = useLocation();

    const [virtualcard, setVirtualCard] = useState({
      isValid: false,
      values: {},
      touched: {},
      errors: {}
    });
    const [user, setUser] = useState({});
    const [reports, setReports] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [openDetail, setOpenDetail] = useState(false);
    const [openDeactivate, setOpenDeactivate] = useState(false);
    const [openTransaction, setOpenTransaction] = useState(false);
    const [openTimeline, setOpenTimeline] = useState(false);
    const [active, setActive] = useState();

    useEffect(() => {
      const errors = validate(virtualcard.values, schema);
  
      setVirtualCard(virtualcard => ({
        ...virtualcard,
        isValid: errors ? false : true,
        errors: errors || {}
      }));
    }, [virtualcard.values]);

    const handleOnChange = (event) => {
      event.persist();
  
      setVirtualCard(virtualcard => ({
        ...virtualcard,
        values: {
          ...virtualcard.values,
          [event.target.name]:
            event.target.type === 'checkbox'
              ? event.target.checked
              : event.target.value
        },
        touched: {
          ...virtualcard.touched,
          [event.target.name]: true
        }
      }));
    }

    const access_token = JSON.parse(localStorage.getItem("data")).access_token
    const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

    useEffect(()=>{
      let url = location.search
      let token = url.split("?")
  
      if(url !== "" && token[1] !== "") {
        transaction.get(`/api/admin/virtual-card/${token[1]}`, {
          headers: {
            'Authorization': `Bearer ${access_token}`
          }
         }).then(res =>{
            const baseData = res.data.output_schema
            setVirtualCard({
              isValid: false,
              values: baseData.virtual_card,
              touched: {},
              errors: {}
            })
            setUser(baseData.user)
            setReports(baseData.reports)
            setTransactions(baseData.transactions)
            setActive(baseData.virtual_card.is_active)
            setOpenDetail(true)
         }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleErrorLogout("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleErrorLogout(message)
            } else{
              const message = err.response.data.error_schema.error_message.indonesian
              handleError(message)
            }
          }else{
            handleErrorLogout("Something went wrong")
          }
         })
      }
      location.search = ""
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },[location.search])

    const handleClickOpenDetail = (id) => {
        transaction.get(`/api/admin/virtual-card/${id}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const baseData = res.data.output_schema
          setVirtualCard({
            isValid: false,
            values: baseData.virtual_card,
            touched: {},
            errors: {}
          })
          setUser(baseData.user)
          setReports(baseData.reports)
          setTransactions(baseData.transactions)
          setActive(baseData.virtual_card.is_active)
          setOpenDetail(true)
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    };
    
    const handleCloseDetail = () => {
        setOpenDetail(false);
    };

    const handleClickOpenTransaction = () => {
        setOpenTransaction(true);
    };
    
    const handleCloseTransaction = () => {
        setOpenTransaction(false);
    };
      
    const handleClickOpenTimeline = () => {
        setOpenTimeline(true);
    };
    
    const handleCloseTimeline = () => {
        setOpenTimeline(false);
    };

    const handleClickOpenDeactivate = () => {
      console.log(virtualcard)
      setVirtualCard({
        isValid: false,
        values: {
          uuid: virtualcard.values.uuid,
          card_number: virtualcard.values.card_number,
          note: virtualcard.values.note,
          is_active: false
        },
        touched: {},
        errors: {}
      });
        setOpenDeactivate(true);
    };

    const handleClickOpenReport = () => {
        setOpenDeactivate(true);
    };
    
    const handleCloseDeactivate = () => {
        setOpenDeactivate(false);
    };

    const handleDeactivate = () => {
      console.log(virtualcard)
      transaction({
        method: 'PUT',
        url: '/api/admin/virtual-card',
        data: virtualcard.values,
        headers: {
            'Authorization': `Bearer ${access_token}`, 
        }
       }).then(res =>{
          setOpenDeactivate(false);
          setOpenDetail(false);
          getData();
          handleSuccess('Virtual card successfully deactivated');
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    }

    const handleReport = () => {
      transaction({
        method: 'POST',
        url: '/api/admin/virtual-card/report',
        data: virtualcard.values,
        headers: {
            'Authorization': `Bearer ${access_token}`, 
        }
       }).then(res =>{
        setOpenDeactivate(false);
        setOpenDetail(false);
        getData();
        handleSuccess('Report has been submitted');
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    }

    const handleBackButtonClick = () => {
        if(page>0){
          props.onPageChange(page-1);
          props.onLoadingChange(true);
        }else{
          alert("this is min page")
        }
      };
    
      const handleNextButtonClick = () => {
        if(page+1<pages){
          props.onPageChange(page+1);
          props.onLoadingChange(true);
        }else{
          alert("this is max page")
        }
      };
    
      const handleRowsPerPageChange = event => {
        props.onSizeChange(event.target.value);
        props.onPageChange(0);
        props.onLoadingChange(true);
      };

      const hasError = field =>
  virtualcard.touched[field] && virtualcard.errors[field] ? true : false;

    return (
        <Fragment>
            <PerfectScrollbar>
            <div className={classes.inner}>
                <Table>
                <TableHead>
                    <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Virtual Card ID</TableCell>
                    <TableCell>Employee Name</TableCell>
                    <TableCell>Used Amount</TableCell>
                    <TableCell>Limit</TableCell>
                    <TableCell>Active</TableCell>
                    </TableRow>
                </TableHead>
                {loading ?
              <TableBody>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
            </TableBody>
          : 
                <TableBody>
                  {virtualcards.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='6' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                    virtualcards.slice(0, size).map(virtualcard => (
                    <TableRow
                        className={classes.tableRow}
                        hover
                        key={virtualcard.uuid}
                    >
                        <TableCell><IconButton onClick={() => handleClickOpenDetail(virtualcard.uuid)}><VisibilityIcon/></IconButton></TableCell>
                        <TableCell><CurrencyFormat value={virtualcard.card_number.slice(0,4) + virtualcard.card_number.slice(12,16)} format={'#### **** **** ####'} displayType={'text'}  renderText={value => <div>{value}</div>} /></TableCell>
                        <TableCell>{virtualcard.name}</TableCell>
                        <TableCell><CurrencyFormat value={virtualcard.limit_used} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                        <TableCell><CurrencyFormat value={virtualcard.limit_transaction} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                        <TableCell>
                        <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={statusColors[virtualcard.is_active ? "Active": "Nonactive"]}
                          size="sm"
                        />
                        {virtualcard.is_active? "Active": "Nonactive"}
                      </div>
                        </TableCell>
                    </TableRow>
                    ))}
                </TableBody>
            }
                </Table>
                <Dialog
                open={openDetail}
                onClose={handleCloseDetail}
                fullWidth
                maxWidth="md"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">Virtual Card Detail</DialogTitle>
                <DialogContent>
                    <Card>
                    <CardContent>
                        <div>
                        <div style={{marginBottom: '5%', marginTop: '2%'}}>
                            {virtualcard.values.card_number && user.name ? (
                            <Fragment>
                              <Cards
                                number={virtualcard.values.card_number.slice(0,4) + "********" + virtualcard.values.card_number.slice(12,16)}
                                name={user.name}
                                expiry={moment(virtualcard.values.end_date).format('MM/YY')}
                                cvc="342"
                                focused="number"
                                preview={true}
                            />
                            <center className={classes.active}>
                            {active ? 
                      (
                        
                        <Typography variant="body1">
                        <StatusBullet
                          className={classes.status}
                          color={'success'}
                          size="sm"
                        />
                        Active</Typography>
                      ) : 
                      (
                        
                        <Typography variant="body1">
                        <StatusBullet
                          className={classes.status}
                          color={'danger'}
                          size="sm"
                        />
                        Nonactive</Typography>
                      )}
                      </center>
                      </Fragment>
                      ): null}
                            
                            <IconButton onClick={handleClickOpenTransaction}>
                                <PaymentIcon/>
                            </IconButton>
                            <IconButton onClick={handleClickOpenTimeline}>
                                <TimelineIcon/>
                            </IconButton>
                            <BorderLinearProgress variant="determinate" value={virtualcard.values.limit_used/virtualcard.values.limit_transaction*100}/>
                        </div>
                        <Table>
                        <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Virtual Card Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Limit Transaction</TableCell>
                        <TableCell><CurrencyFormat value={virtualcard.values.limit_transaction} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Limit Used</TableCell>
                        <TableCell><CurrencyFormat value={virtualcard.values.limit_used} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Validity Period</TableCell>
                        <TableCell>{moment(virtualcard.values.start_date).format('DD/MM/YYYY')} to {moment(virtualcard.end_date).format('DD/MM/YYYY')}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Validity Period</TableCell>
                        <TableCell><div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={statusColors[virtualcard.values.is_active ? "Active": "Nonactive"]}
                          size="sm"
                        />
                        {virtualcard.values.is_active? "Active": "Nonactive"}
                      </div></TableCell>
                      </TableRow>
                      </TableBody>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Employee Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                        </div>
                    </CardContent>
                </Card>
                </DialogContent>
                <DialogActions>
                { virtualcard.values.is_active ? rank_level===4 ? (
                      <Button onClick={handleClickOpenDeactivate} color="secondary" autoFocus>
                          Deactivate
                      </Button>
                  ): (
                      <Button onClick={handleClickOpenReport} color="secondary" autoFocus>
                          Report
                      </Button>
                  ):
                  null
                  }
                
                </DialogActions>
                </Dialog>
                <Dialog
                open={openDeactivate}
                onClose={handleCloseDeactivate}
                fullWidth
                maxWidth="sm"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{"Reason"}</DialogTitle>
                <DialogContent>
                    <Card>
                        <CardContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            error={hasError('note')}
                            helperText={
                              hasError('note') ? virtualcard.errors.note[0] : null
                            }
                            name="note"
                            label="Description"
                            type="text"
                            value={virtualcard.values.note || ''}
                            onChange={handleOnChange}
                            fullWidth
                        />
                        </CardContent>
                    </Card>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDeactivate} color="default">
                    Close
                    </Button>
                    {  rank_level===4 ? (
                        <Button onClick={handleDeactivate} color="secondary" autoFocus disabled={!virtualcard.isValid}>
                            Deactivate
                        </Button>
                    ): (
                        <Button onClick={handleReport} color="secondary" autoFocus disabled={!virtualcard.isValid}>
                            Report
                        </Button>
                    )}
                    
                </DialogActions>
                </Dialog>
                
                <Dialog
                open={openTransaction}
                onClose={handleCloseTransaction}
                maxWidth="md"
                fullWidth
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{"Transaction History"}</DialogTitle>
                <DialogContent>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Type</TableCell>
                                <TableCell>Merchant</TableCell>
                                <TableCell>Amount</TableCell>
                                <TableCell>Status</TableCell>
                                <TableCell>Date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {transactions.map(transaction => (
                                <TableRow key={transaction.uuid}>
                                    <TableCell>{transaction.flag_qr ? "QR" : "Non QR"}</TableCell>
                                    <TableCell>{transaction.merchant}</TableCell>
                                    <TableCell><CurrencyFormat value={transaction.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                                    <TableCell>{transaction.transaction_success ? "Succeed" : "Failed"}</TableCell>
                                    <TableCell>{moment(transaction.created_date).format('DD/MM/YYYY')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                </DialogActions>
                </Dialog>
                <Dialog
                open={openTimeline}
                onClose={handleCloseTimeline}
                maxWidth="md"
                fullWidth
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{"Timeline"}</DialogTitle>
                <DialogContent>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Actor</TableCell>
                                <TableCell>Action</TableCell>
                                <TableCell>Note</TableCell>
                                <TableCell>Date</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {reports.map(report => (
                                <TableRow key={report.uuid}>
                                    <TableCell>{report.actor_name}</TableCell>
                                    <TableCell>{report.action}</TableCell>
                                    <TableCell>{report.note}</TableCell>
                                    <TableCell>{moment(report.created_date).format('DD/MM/YYYY')}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </DialogContent>
                <DialogActions>
                </DialogActions>
                </Dialog>
            </div>
            </PerfectScrollbar>
            <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
        </Fragment>
    )
};

VirtualCardTable.propTypes = {
    className: PropTypes.string,
    virtualcards: PropTypes.array.isRequired
};
  
export default VirtualCardTable;