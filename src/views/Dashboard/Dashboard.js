import React , {useEffect, useState} from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { 
  Grid,
  Dialog,
  DialogContent,
  DialogContentText,
  Typography
} from '@material-ui/core';

import {
  Budget,
  TotalUsers,
  LatestSales,
} from './components';
import dashboard from 'services/dashboard'
import auth from 'services/auth'
import companyService from 'services/company'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4)
  },
  flash: {
    height : 200
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const history = useHistory();
  
  const access_token = JSON.parse(localStorage.getItem("data")).access_token;
  const company_id = JSON.parse(localStorage.getItem("data")).company_id;
  const [data, setData] = useState({});
  const [data1, setData1] = useState({});
  const [data2, setData2] = useState({});
  const [company, setCompany] = useState({});
  const [month, setMonth] = useState('6')

  const [message, setMessage]= useState('');
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [imgAge, setImgAge]=useState(Date.now);

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  useEffect(() => {
    document.title = "Corona | Dashboard"
    dashboard.get(`/api/getdashboard?companyid=${company_id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
      setData(res.data.output_schema.result);
      console.log(res.data.output_schema.result)
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     dashboard.get(`/api/getlistvcrem?companyid=${company_id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
      console.log(res.data.output_schema.result)
      setData1(res.data.output_schema.result);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     dashboard.get(`/api/getdatamonthly?data=${month}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
      console.log(res.data.output_schema)
      setData2(res.data.output_schema);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     companyService.get(`/bank-side/api/company/${company_id}`)
     .then(res =>{
        const data = res.data.output_schema.company_request
        setCompany(data)
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
    })
     // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [month]);

  const handleMonthChange = month => {
    setMonth(month);
  };

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={6}
          sm={12}
          xl={12}
          xs={12}
        >
          <Budget percentage={data.reimburse_perc} total={company.balance} usage={data.reimburse_usg} highest_amount={data1.branch_max_reimburse_amt} branch={data1.branch_max_reimburse_nm} division={data1.division_max_reimburse_nm}/>
        </Grid>
        <Grid
          item
          lg={6}
          sm={12}
          xl={12}
          xs={12}
        >
          <TotalUsers percentage={data.vc_perc_transaction} total={company.credit_limit}  usage={data.vc_usg_limit_transaction} highest_amount={data1.branch_max_vc_amt} branch={data1.branch_max_vc_nm} division={data1.division_max_vc_nm}/>
        </Grid>
        <Grid
          item
          lg={12}
          sm={12}
          xl={12}
          xs={12}
        >
          <LatestSales data2={data2} month={month} onMonthChange={handleMonthChange}/>
        </Grid>
      </Grid>


      <Dialog
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>

    </div>
  );
};

export default Dashboard;
