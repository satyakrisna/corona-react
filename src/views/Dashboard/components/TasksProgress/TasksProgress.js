import React, {Fragment} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  Avatar,
  LinearProgress,
  Box
} from '@material-ui/core';
import InsertChartIcon from '@material-ui/icons/InsertChartOutlined';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    height: '100%'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: '5px',
    marginTop: theme.spacing(1),
    background: 'linear-gradient(25deg, rgba(238,81,78,1) 0%, rgba(230,59,55,1) 50%)'
  },
  icon: {
    height: 32,
    width: 32
  },
  progress: {
    marginTop: theme.spacing(3)
  },
  box: {
    width: 60,
    height: 60,
    marginLeft: 'auto',
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(-8),
    zIndex: "1000",
    borderRadius: '5px',
  }
}));

const TasksProgress = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <Fragment>
      <Box className={classes.box} boxShadow={5}>
        <Avatar className={classes.avatar}>
          <InsertChartIcon className={classes.icon} />
        </Avatar>
      </Box>
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
                variant="body2"
              >
                TOTAL BUDGET
              </Typography>
              <Typography variant="h3">25.5%</Typography>
            </Grid>
            {/* <Grid item>
              <Avatar className={classes.avatar}>
                <InsertChartIcon className={classes.icon} />
              </Avatar>
            </Grid> */}
          </Grid>
          <LinearProgress
            className={classes.progress}
            value={25.5}
            variant="determinate"
          />
        </CardContent>
      </Card>
    </Fragment>
  );
};

TasksProgress.propTypes = {
  className: PropTypes.string
};

export default TasksProgress;
