import React, {Fragment} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar,LinearProgress, Box, Divider, Table, TableHead, TableRow, TableBody, TableCell } from '@material-ui/core';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import CurrencyFormat from 'react-currency-format';

const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#1a90ff',
  },
}))(LinearProgress);

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    height: '100%'
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700
  },
  row: {
    display: 'flex',
    alignItems: 'center',
  },
  spacer: {
    flexGrow: 1
  },
  table: {
    backgroundColor: '#FAFAFA'
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: '5px',
    marginTop: theme.spacing(1),
    background: 'linear-gradient(25deg, rgba(100,186,104,1) 0%, rgba(69,162,73,1) 50%)'
  },
  icon: {
    height: 32,
    width: 32
  },
  difference: {
    marginTop: theme.spacing(2),
    display: 'flex',
    alignItems: 'center'
  },
  differenceIcon: {
    color: theme.palette.success.dark
  },
  differenceValue: {
    color: theme.palette.success.dark,
    marginRight: theme.spacing(1)
  },
  progress: {
    marginTop: theme.spacing(3)
  },
  box: {
    width: 60,
    height: 60,
    marginLeft: 'auto',
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(-8),
    zIndex: "1000",
    borderRadius: '5px',
  }
}));

const TotalUsers = props => {
  const { className, percentage, total, usage, highest_amount, division, branch, ...rest } = props;

  const classes = useStyles();

  return (
    <Fragment>
      <Box className={classes.box} boxShadow={5}>
        <Avatar className={classes.avatar}>
          <CreditCardIcon className={classes.icon} />
        </Avatar>
      </Box>
      <Card
        {...rest}
        className={clsx(classes.root, className)}
      >
        <CardContent>
          <Grid
            container
            justify="space-between"
          >
            <Grid item>
              <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
                variant="body2"
              >
                VIRTUAL CARD
              </Typography>
              <Typography variant="h3">{percentage} %</Typography>
            </Grid>
          </Grid>
          <BorderLinearProgress
            className={classes.progress}
            value={percentage}
            variant="determinate"
          />
          
        <Typography variant="body1">Rp <CurrencyFormat value={usage} thousandSeparator={true} displayType={'text'}  renderText={value =>value} /> / <CurrencyFormat value={total} thousandSeparator={true} displayType={'text'}  renderText={value => value} /></Typography>
        <Divider style={{marginTop:10}}/>
        <Table style={{marginTop: 10}} size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Highest Spending</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell><CurrencyFormat value={highest_amount} thousandSeparator={true} prefix={'Rp '} displayType={'text'}  renderText={value => value} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
        </CardContent>
      </Card>
    </Fragment>
  );
};

TotalUsers.propTypes = {
  className: PropTypes.string
};

export default TotalUsers;
