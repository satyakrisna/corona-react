import React, {Fragment} from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Card, CardContent, Grid, Typography, Avatar, Box } from '@material-ui/core';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    height: '100%',
    // backgroundColor: theme.palette.primary.main,
    // color: theme.palette.primary.contrastText
  },
  content: {
    alignItems: 'center',
    display: 'flex'
  },
  title: {
    fontWeight: 700
  },
  avatar: {
    height: 60,
    width: 60,
    borderRadius: '5px',
    marginTop: theme.spacing(1),
    background: 'linear-gradient(25deg, rgba(37,197,217,1) 0%, rgba(1,173,194,1) 50%)'
  },
  icon: {
    height: 32,
    width: 32
  },
  box: {
    width: 60,
    height: 60,
    marginLeft: 'auto',
    marginRight: theme.spacing(2),
    marginBottom: theme.spacing(-8),
    zIndex: "1000",
    borderRadius: '5px',
  }
}));

const TotalProfit = props => {
  const { className, ...rest } = props;

  const classes = useStyles();

  return (
    <Fragment>
      <Box className={classes.box} boxShadow={5}>
        <Avatar className={classes.avatar}>
          <AttachMoneyIcon className={classes.icon} />
        </Avatar>
      </Box>
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardContent>
        <Grid
          container
          justify="space-between"
        >
          <Grid item>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
              variant="body2"
            >
              TOTAL SPENDING
            </Typography>
            <Typography
              color="inherit"
              variant="h3"
            >
              Rp 2,200,000
            </Typography>
          </Grid>
          {/* <Grid item>
            <Avatar className={classes.avatar}>
              <AttachMoneyIcon className={classes.icon} />
            </Avatar>
          </Grid> */}
        </Grid>
      </CardContent>
    </Card>
  </Fragment>
  );
};

TotalProfit.propTypes = {
  className: PropTypes.string
};

export default TotalProfit;
