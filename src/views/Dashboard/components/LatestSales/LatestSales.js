import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Divider,
  Select, 
  MenuItem,
  FormControl
} from '@material-ui/core';
import palette from 'theme/palette';

import {options } from './chart';

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(1)
  },
  chartContainer: {
    height: 400,
    position: 'relative'
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));

const LatestSales = props => {
  const { className, data2, onMonthChange, ...rest } = props;

  const data = {
    labels: data2.date,
    datasets: [
      {
        label: 'Reimbursement',
        backgroundColor: palette.primary.main,
        data: data2.reimburse_payment
      },
      {
        label: 'Virtual Card',
        backgroundColor: palette.neutral,
        data: data2.virtual_card_payment
      }
    ]
  };

  const handleMonthChange = event => {
    onMonthChange(event.target.value);
  };

  const classes = useStyles();

  return (
    <Card
      {...rest}
      className={clsx(classes.root, className)}
    >
      <CardHeader
        action={
          <FormControl variant="outlined" style={{width: '100%', marginTop: '5%', marginBottom: '3%'}} className={classes.searchInput}>
          <Select
            labelId="demo-simple-select-outlined-label"
            margin="dense"
            id="demo-simple-select-outlined"
            name="type"
            value={props.month}
            onChange={handleMonthChange}
          >
            <MenuItem value={'1'}>Last Month</MenuItem>
            <MenuItem value={'2'}>Last 2 Months </MenuItem>
            <MenuItem value={'3'}>Last 3 Months </MenuItem>
            <MenuItem value={'4'}>Last 4 Months </MenuItem>
            <MenuItem value={'5'}>Last 5 Months </MenuItem>
            <MenuItem value={'6'}>Last 6 Months </MenuItem>
            <MenuItem value={'7'}>Last 7 Months </MenuItem>
            <MenuItem value={'8'}>Last 8 Months </MenuItem>
            <MenuItem value={'9'}>Last 9 Months </MenuItem>
            <MenuItem value={'10'}>Last 10 Months </MenuItem>
            <MenuItem value={'11'}>Last 11 Months </MenuItem>
            <MenuItem value={'12'}>Las 12 Months</MenuItem>
          </Select>
        </FormControl>
        }
        title="Latest Spending"
      />
      <Divider />
      <CardContent>
        <div className={classes.chartContainer}>
          <Bar
            data={data}
            options={options}
          />
        </div>
      </CardContent>
      <Divider />
      <CardActions className={classes.actions}>
      </CardActions>
    </Card>
  );
};

LatestSales.propTypes = {
  className: PropTypes.string
};

export default LatestSales;
