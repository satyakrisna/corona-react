import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Button, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Tooltip,
  Hidden,
  IconButton, Typography } from '@material-ui/core';

import { SearchInput } from 'components';
import SearchIcon from '@material-ui/icons/Search';
import PostAddIcon from '@material-ui/icons/PostAdd';
import CloseIcon from '@material-ui/icons/Close';
import auth from 'services/auth';
import validate from 'validate.js';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  location: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  success: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    float: 'right'
  },
  primary: {
    background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    marginRight: '0'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  successMobile: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    color: '#FFFFFF'
  },
}));

const BranchToolbar = props => {
  const { getData, handleSuccess, handleError, handleError401 } = props;
  const [openAdd, setOpenAdd] = useState(false);

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const company_id = JSON.parse(localStorage.getItem("data")).company_id

  const initialBranchState = {
    uuid: null,
    company_id: company_id,
    name: "",
    location: ""
  };

  const [word, setWord] = useState("");
  const [branch, setBranch] = useState({
    isValid: false,
    values: initialBranchState,
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(branch.values, schema);

    setBranch(branch => ({
      ...branch,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [branch.values]);

  const handleSave = () => {
    auth({
      method: 'POST',
      url: '/api/branch',
      data: branch.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenAdd(false);
        getData();
        setBranch({
          isValid: false,
          values: initialBranchState,
          touched: {},
          errors: {}
        });
        handleSuccess('Data successfully added');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleInputChange = event => {
    event.persist();

    setBranch(branch => ({
      ...branch,
      values: {
        ...branch.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...branch.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleClickOpenAdd = () => {
    setOpenAdd(true);
  };

  const handleCloseAdd = () => {
    setOpenAdd(false);
    setBranch({
      isValid: false,
      values: initialBranchState,
      touched: {},
      errors: {}
    })
  };

  const handleSearchChange = (event) => {
    setWord(event.target.value);
  };

  const handleSearch = () => {
    props.onKeywordChange(word);
    props.onLoadingChange(true);
    props.onPageChange(0);
    props.getData();
  };

  const classes = useStyles();

  const hasError = field =>
    branch.touched[field] && branch.errors[field] ? true : false;

  return (
    <div
      className={classes.root}
    >
      <div className={classes.row}>
        <SearchInput
          className={classes.searchInput}
          placeholder="Search branch name or location"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>

        <Hidden mdDown>
        <span className={classes.spacer} />

        <Tooltip title="Add Branch" placement="top">
          <Button
            onClick={handleClickOpenAdd}
            className={classes.success}
            variant="contained"
          >
            <PostAddIcon/>
          </Button>
        </Tooltip>
        </Hidden>
      </div>

      <Hidden mdUp>
        <Button
          fullWidth
          onClick={handleClickOpenAdd}
          className={classes.successMobile}
          variant="contained"
          startIcon={<PostAddIcon/>}
        >
          Add New Branch
        </Button>
      </Hidden>

      <Dialog
              open={openAdd}
              onClose={handleCloseAdd}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Add Branch
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton aria-label="close" onClick={handleCloseAdd}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
                <DialogContent>
                  <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('name')}
                    helperText={
                      hasError('name') ? branch.errors.name[0] : null
                    }
                    name="name"
                    label="Branch Name"
                    type="text"
                    onChange={handleInputChange} 
                    value={branch.values.name}
                    fullWidth
                  />
                  <TextField
                    margin="dense"
                    error={hasError('location')}
                    helperText={
                      hasError('location') ? branch.errors.location[0] : null
                    }
                    name="location"
                    label="Branch Location"
                    type="text"
                    onChange={handleInputChange} 
                    value={branch.values.location}
                    fullWidth
                  />
                </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseAdd} color="secondary">
                  Cancel
                </Button>
                <Button onClick={handleSave} color="primary" autoFocus disabled={!branch.isValid}>
                  Save
                </Button>
              </DialogActions>
            </Dialog>
    </div>
  );
};

BranchToolbar.propTypes = {
  className: PropTypes.string
};

export default BranchToolbar;
