import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {useHistory,useLocation} from 'react-router-dom';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from '@material-ui/styles';
import {
  Card,
  CardContent,
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  TextField,
  IconButton,
  Select,
  MenuItem,
  Stepper,
  Step,
  StepLabel
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import CurrencyFormat from 'react-currency-format';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import requestService from 'services/request'
import validate from 'validate.js';

const schema = {
  note: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
}

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
      padding: 0
    },
    inner: {
      minWidth: 1050
    },
    nameContainer: {
      display: 'flex',
      alignItems: 'center'
    },
    avatar: {
      marginRight: theme.spacing(2)
    },
    status: {
      marginRight: theme.spacing(1)
    },
    row: {
      height: '42px',
      display: 'flex',
      alignItems: 'center',
      marginTop: theme.spacing(1)
    },
    spacer: {
      flexGrow: 1
    },
    actions: {
      justifyContent: 'flex-end'
    },
    table: {
      width: "20%",
      backgroundColor: '#FAFAFA'
    },
    dialog: {
      width: "100%",
      float: 'left',
      marginRight:50
    },
    image: {
      display: 'inline-block',
      maxWidth: '100%',
      width: 300
    },
    pagination:{
      marginLeft: theme.spacing(3)
    }
}));

function getSteps() {
  return ['Employee', 'Head of Division', 'Head of Branch', 'Head Office'];
}

const VirtualCardRequestTable = props => {
    const history = useHistory();
    const location = useLocation();
    const {requests, loading, getData, handleSuccess, handleError, handleErrorLogout, size, page, count, pages} = props;
    const classes = useStyles();

    const [openDetail, setOpenDetail] = useState(false);
    const [openApprove, setOpenApprove] = useState(false);
    const [openDeny, setOpenDeny] = useState(false);
    const [request, setRequest] = useState({
      isValid: false,
      values: {},
      touched: {},
      errors: {}
    });
    const [user, setUser] = useState({});
    const [steps] = useState(getSteps());
    const [activeStep, setActiveStep] = useState(0);

    useEffect(() => {
      const errors = validate(request.values, schema);
  
      setRequest(request => ({
        ...request,
        isValid: errors ? false : true,
        errors: errors || {}
      }));
    }, [request.values]);

    const handleOnChange = (event) => {
      event.persist();
  
      setRequest(request => ({
        ...request,
        values: {
          ...request.values,
          [event.target.name]:
            event.target.type === 'checkbox'
              ? event.target.checked
              : event.target.value
        },
        touched: {
          ...request.touched,
          [event.target.name]: true
        }
      }));
    }

    const access_token = JSON.parse(localStorage.getItem("data")).access_token
    const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

    useEffect(()=>{
      let url = location.search
      let token = url.split("?")
  
      if(url !== "" && token[1] !== "") {
        requestService.get(`/api/vcr/${token[1]}`, {
          headers: {
            'Authorization': `Bearer ${access_token}`
          }
         }).then(res =>{
          const data =  res.data.output_schema
          if(data.virtual_card_request.status === "SUBMITTED") {
            setRequest({
              isValid: false,
              values: data.virtual_card_request,
              touched: {},
              errors: {}
            })
            setUser(data.user)
            if(rank_level===2){
              setActiveStep(1);
            }else if(rank_level===3){
              setActiveStep(2);
            }else{
              setActiveStep(3);
            }
            setOpenDetail(true);
         } else {
            history.push(`/admin/virtual-card-request-finished?${token[1]}`)
         }
          
         }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleErrorLogout("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleErrorLogout(message)
            } else {
              history.push(`/admin/virtual-card-request-finished?${token[1]}`)
            }
          }else{
            handleErrorLogout("Something went wrong")
          }
         })
      }
      location.search = ""
      // eslint-disable-next-line react-hooks/exhaustive-deps
    },[location.search])

    const handleClickOpenDetail = (id) => {
      requestService.get(`/api/vcr/${id}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
        const data =  res.data.output_schema
        setRequest({
          isValid: false,
          values: data.virtual_card_request,
          touched: {},
          errors: {}
        })
        setUser(data.user)
        if(rank_level===2){
          setActiveStep(1);
          }else if(rank_level===3){
          setActiveStep(2);
          }else{
          setActiveStep(3);
          }
          setOpenDetail(true);
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    };

    const handleApprove = () => {
      if(rank_level === 4) {
        history.push("/admin/set-virtual-card",
        {
          user_uuid: user.uuid,
          invoice_request: request.values.invoice,
          limit_transaction: request.values.amount,
          start_date: request.values.start_date,
          end_date: request.values.end_date,
          name: user.name,
          request_uuid: request.values.uuid
        });
      } else {
        requestService({
          method: 'PUT',
          url: '/api/vcr',
          data: {
            "uuid": request.values.uuid,
            "accepted": true,
            "note": ""
          },
          headers: {
              'Authorization': `Bearer ${access_token}`, 
          }
        }).then(res =>{
            setOpenDetail(false);
            setOpenApprove(false);
            getData();
            handleSuccess('Data successfully updated');
        }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleErrorLogout("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleErrorLogout(message)
            } else{
              const message = err.response.data.error_schema.error_message.indonesian
              handleError(message)
            }
          }else{
            handleErrorLogout("Something went wrong")
          }
        })
      }
    };
  
    const handleDeny = () => {
      requestService({
        method: 'PUT',
        url: '/api/vcr',
        data: {
          "uuid": request.values.uuid,
          "accepted": false,
          "note": request.values.note
        },
        headers: {
            'Authorization': `Bearer ${access_token}`, 
        }
       }).then(res =>{
          setOpenDetail(false);
          setOpenDeny(false);
          getData();
          handleSuccess('Data successfully updated');
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    };
    
    const handleCloseDetail = () => {
        setOpenDetail(false);
    };
    
    const handleClickOpenApprove = () => {
        setOpenApprove(true);
    };
    
    const handleCloseApprove = () => {
        setOpenApprove(false);
    };
      
    const handleClickOpenDeny = () => {
        setOpenDeny(true);
    };
    
    const handleCloseDeny = () => {
        setOpenDeny(false);
    };

    const handleBackButtonClick = () => {
        if(page>0){
          props.onPageChange(page-1);
          props.onLoadingChange(true);
        }else{
          alert("this is min page")
        }
      };
    
      const handleNextButtonClick = () => {
        if(page+1<pages){
          props.onPageChange(page+1);
          props.onLoadingChange(true);
        }else{
          alert("this is max page")
        }
      };
    
      const handleRowsPerPageChange = event => {
        props.onSizeChange(event.target.value);
        props.onPageChange(0);
        props.onLoadingChange(true);
      };

      const hasError = field =>
  request.touched[field] && request.errors[field] ? true : false;

    return (
        <Fragment>
            <PerfectScrollbar>
            <div className={classes.inner}>
                <Table>
                <TableHead>
                    <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Request ID</TableCell>
                    <TableCell>Employee Name</TableCell>
                    <TableCell>Category</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Date</TableCell>
                    </TableRow>
                </TableHead>
                {loading ?
              <TableBody>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
            </TableBody>
          : 
                <TableBody>
                  {requests.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='7' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                    requests.slice(0, size).map(request => (
                    <TableRow
                        className={classes.tableRow}
                        hover
                        key={request.uuid}
                    >
                        <TableCell><Button onClick={() => { handleClickOpenDetail(request.uuid)}}><VisibilityIcon/></Button></TableCell>
                        <TableCell>{request.invoice}</TableCell>
                        <TableCell>{request.name}</TableCell>
                        <TableCell>{request.categories}</TableCell>
                        <TableCell><CurrencyFormat value={request.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                        <TableCell>
                        {moment(request.created_date).format('DD/MM/YYYY')}
                        </TableCell>
                    </TableRow>
                    ))}
                </TableBody>
                        }
                </Table>
                <Dialog
                open={openDetail}
                onClose={handleCloseDetail}
                fullWidth
                maxWidth="md"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                   Detail Request
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDetail}>
                    <CloseIcon />
                  </IconButton>
                </div>
                </DialogTitle>
                <DialogContent>
                    <Card>
                    <CardContent>
                    <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                      <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                      </Step>
                    ))}
                  </Stepper>
                        <div className={classes.dialog}>
                        <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Request Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Request ID</TableCell>
                        <TableCell>{request.values.invoice}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Category</TableCell>
                        <TableCell>{request.values.categories}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell><CurrencyFormat value={request.values.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Description</TableCell>
                        <TableCell>{request.values.description}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date</TableCell>
                        <TableCell>
                            {moment(request.values.start_date).format('D MMM YYYY')} - {moment(request.values.end_date).format('D MMM YYYY')}
                            {/* {moment(request.start_date).format('DD/MM/YYYY')} - {moment(request.end_date).format('DD/MM/YYYY')} */}
                          </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Attachment</TableCell>
                        <TableCell>
                          {request.values.file_path!==""&&request.values.file_path!==null? <a href = {request.values.file_path} target = "_blank" rel="noopener noreferrer"><FileCopyIcon/></a>: "-"}
                          </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date Submitted</TableCell>
                        <TableCell>
                            {moment(request.values.created_date).format('D MMM YYYY')}
                          </TableCell>
                      </TableRow>
                    </TableBody>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Employee Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                        </div>
                    </CardContent>
                </Card>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClickOpenDeny} color="secondary">
                    Deny
                    </Button>
                    <Button onClick={handleClickOpenApprove} color="primary" autoFocus>
                    Approve
                    </Button>
                </DialogActions>
                </Dialog>
                <Dialog
                open={openDeny}
                onClose={handleCloseDeny}
                fullWidth
                maxWidth="sm"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                   Deny Reason
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDeny}>
                    <CloseIcon />
                  </IconButton>
                </div>
                </DialogTitle>
                <DialogContent>
                    <Card>
                        <CardContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            error={hasError('note')}
                            helperText={
                              hasError('note') ? request.errors.note[0] : null
                            }
                            name="note"
                            label="Description"
                            type="text"
                            fullWidth
                            value={request.values.note || ''}
                            onChange={handleOnChange}
                        />
                        </CardContent>
                    </Card>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDeny}>
                    Cancel
                    </Button>
                    <Button onClick={handleDeny} color="secondary" autoFocus disabled={!request.isValid}>
                    Deny
                    </Button>
                </DialogActions>
                </Dialog>
                <Dialog
                open={openApprove}
                onClose={handleCloseApprove}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                   Confirmation
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseApprove}>
                    <CloseIcon />
                  </IconButton>
                </div>
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                    Are you sure want to approve this request?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseApprove} color="default">
                    Cancel
                    </Button>
                    <Button onClick={handleApprove} color="primary" autoFocus>
                    Approve
                    </Button>
                </DialogActions>
                </Dialog>
            </div>
            </PerfectScrollbar>
            <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
        </Fragment>
    )
};

VirtualCardRequestTable.propTypes = {
    className: PropTypes.string,
    requests: PropTypes.array.isRequired
};
  
export default VirtualCardRequestTable;