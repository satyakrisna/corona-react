import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    request_id: 'RQ0002',
    employee_id: 'u068722',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'done'
  },
  {
    id: uuid(),
    request_id: 'RQ0003',
    employee_id: 'u068723',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'done'
  },
  {
    id: uuid(),
    request_id: 'RQ0004',
    employee_id: 'u068724',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'rejected'
  },
  {
    id: uuid(),
    request_id: 'RQ0006',
    employee_id: 'u068726',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'done'
  },
  {
    id: uuid(),
    request_id: 'RQ0007',
    employee_id: 'u068727',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'done'
  },
];
