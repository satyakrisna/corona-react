import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    request_id: 'RQ0001',
    employee_id: 'u068721',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'pending'
  },
  {
    id: uuid(),
    request_id: 'RQ0005',
    employee_id: 'u068725',
    name: 'Willy Mardianto',
    amount: '5000000',
    createdAt: 1555016400000,
    status: 'pending'
  }
];
