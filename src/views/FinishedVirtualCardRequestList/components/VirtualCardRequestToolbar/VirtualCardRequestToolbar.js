import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Button, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Tooltip,
  Hidden,
  IconButton, Typography, Card, CardContent } from '@material-ui/core';

import request from 'services/request'
import auth from 'services/auth'
import moment from 'moment';
import { SearchInput } from 'components';
import SearchIcon from '@material-ui/icons/Search';
import FilterListIcon from '@material-ui/icons/FilterList';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    root: {},
    row: {
      height: '42px',
      display: 'flex',
      alignItems: 'center',
      marginTop: theme.spacing(1)
    },
    spacer: {
      flexGrow: 1
    },
    searchInput: {
      marginRight: theme.spacing(1)
    },
    default: {
      background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
      padding: theme.spacing(1.25),
      backgroundSize: "cover",
      backgroundPosition: "center center",
      margin: theme.spacing(0.5)
    },
    primary: {
      background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
      padding: theme.spacing(1.25),
      backgroundSize: "cover",
      backgroundPosition: "center center",
      color: '#FFFFFF',
      margin: theme.spacing(0.5),
      marginRight: '0'
    },
    buttonFilterMobile: {
      background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2)
    }
}));

const VirtualCardRequestToolbar = props => {
    const { handleError, handleErrorLogout } = props;
    const classes = useStyles();
    const [word, setWord] = useState("");
    const [openFilter, setOpenFilter] = useState(false);
    const [categories, setCategories] = useState([]);
    const [divisions, setDivisions] = useState([]);
    const [branches, setBranches] = useState([]);
    var date = new Date();
    var today = date.toISOString().substr(0,10);
    var lastMonth = moment().subtract(1, 'months').toISOString().substr(0,10);
    const [filter, setFilter] = useState({
      category: "",
      status: "",
      division: "",
      branch: "",
      start: lastMonth,
      end: today,
      orderBy: '',
      direction: 'desc'
    });
  
    const access_token = JSON.parse(localStorage.getItem("data")).access_token;

    const handleSearchChange = (event) => {
      setWord(event.target.value);
    };
  
    const handleSearch = () => {
      props.onKeywordChange(word);
      props.onPageChange(0);
    };

    const getCategories = ()=>{
      request.get(`/api/category?size=1000`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const baseData = res.data.output_schema
          setCategories(baseData.categories.content)
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    }
  
    const getDivisions = ()=>{
      auth.get(`/api/division?size=1000`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const baseData = res.data.output_schema
          setDivisions(baseData.division.content)
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    }
  
    const getBranches = ()=>{
      auth.get(`/api/branch?size=1000`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const baseData = res.data.output_schema
          setBranches(baseData.branch.content)
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    }

    const handleFilterChange = event => {
      const { name, value } = event.target;
      setFilter({ ...filter, [name]: value });
    };
  
    const handleClickOpenFilter = () => {
      getCategories();
      getDivisions();
      getBranches();
      setOpenFilter(true);
    };
  
    const handleCloseFilter = () => {
      setOpenFilter(false);
    };
  
    const handleFilter = () => {
      setOpenFilter(false);
      console.log(filter.status);
      props.onFilterChange(filter.category, filter.status, filter.division, filter.branch, filter.start, filter.end, filter.orderBy, filter.direction);
      props.onPageChange(0);
    };
  
    const handleClearFilter = () => {
      setOpenFilter(false);
      props.onFilterChange('','','','',lastMonth,today,'modify_date','desc');
      props.onPageChange(0);
    };

    return (
        <div
        className={classes.root}
        >
        <div className={classes.row}>
        <SearchInput
          className={classes.searchInput}
          placeholder="Search request"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>

        <Hidden mdDown>
        <span className={classes.spacer} />
        <Tooltip title="Filter & Sort" placement="top">
        <Button
          onClick={handleClickOpenFilter}
          className={classes.default}
          variant="contained"
        >
          <FilterListIcon/>
        </Button>
        </Tooltip>
        </Hidden>

        </div>

        <Hidden mdUp>
          <Button
            onClick={handleClickOpenFilter}
            className={classes.buttonFilterMobile}
            variant="contained"
            startIcon={<FilterListIcon />}
            fullWidth
          >
            Filter & Sort
          </Button>
      </Hidden>

        <Dialog
              open={openFilter}
              onClose={handleCloseFilter}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="sm"
              fullWidth
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Filter & Sort
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseFilter}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <Card>
                  <CardContent>
                    <Typography variant="h5">Filter</Typography>
                    <TextField
                      className={classes.textField}
                      select
                      margin="dense"
                      label="Category"
                      name="category"
                      onChange={handleFilterChange}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                      value={filter.category}
                    >
                      <option value=""></option>
                    {categories.map((category) => (
                      <option key={category.uuid} value={category.name}>
                        {category.name}
                      </option>
                    ))
                    }
                  </TextField>
                  <TextField
                      className={classes.textField}
                      select
                      margin="dense"
                      label="Division"
                      name="division"
                      onChange={handleFilterChange}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                      value={filter.division}
                    >
                      <option value=""></option>
                    {divisions.map((division) => (
                      <option key={division.uuid} value={division.uuid}>
                        {division.name}
                      </option>
                    ))
                    }
                  </TextField>
                  <TextField
                      select
                      margin="dense"
                      label="Branch"
                      name="branch"
                      onChange={handleFilterChange}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                      value={filter.branch}
                    >
                      <option value=""></option>
                    {branches.map((branch) => (
                      <option key={branch.uuid} value={branch.uuid}>
                        {branch.name}
                      </option>
                    ))
                    }
                  </TextField>
                  <TextField
                      select
                      margin="dense"
                      label="Status"
                      name="status"
                      onChange={handleFilterChange}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                      value={filter.status}
                    >
                      <option value=""></option>
                      <option value="submitted">SUBMITTED</option>
                      <option value="accepted">ACCEPTED</option>
                      <option value="rejected">REJECTED</option>
                      <option value="done">DONE</option>
                    </TextField>
                    <div className={classes.row}>
                    <TextField
                                label="From"
                                type="date"
                                name="start"
                                fullWidth
                                value={filter.start}
                                onChange={handleFilterChange}
                            />
                      <span className={classes.spacer} />
                      <TextField
                                  label="To"
                                  type="date"
                                  name="end"
                                  fullWidth
                                  value={filter.end}
                                  onChange={handleFilterChange}
                              />
                    </div>
                  </CardContent>
                </Card>
                <Card>
                  <CardContent>
                    <Typography variant="h5">Sort</Typography>
                    <div className={classes.row}>
                      <TextField
                          select
                          margin="dense"
                          label="Order by"
                          name="orderBy"
                          onChange={handleFilterChange}
                          SelectProps={{
                            native: true,
                          }}
                          fullWidth
                          value={filter.orderBy}
                        >
                          <option value=""></option>
                          <option value="amount">Amount</option>
                          <option value="modify_date">Modify Date</option>
                      </TextField>
                      <TextField
                          select
                          margin="dense"
                          label="Direction"
                          name="direction"
                          onChange={handleFilterChange}
                          SelectProps={{
                            native: true,
                          }}
                          fullWidth
                          value={filter.direction}
                        >
                          <option value="asc">ASC</option>
                          <option value="desc">DESC</option>
                      </TextField>
                    </div>
                  </CardContent>
                </Card>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClearFilter} color="secondary" >
                  Clear Filter
                </Button>
                <Button onClick={handleFilter} color="primary" autoFocus>
                  Filter
                </Button>
              </DialogActions>
            </Dialog>
        </div>
    );
}

VirtualCardRequestToolbar.propTypes = {
    className: PropTypes.string
};
  
export default VirtualCardRequestToolbar;