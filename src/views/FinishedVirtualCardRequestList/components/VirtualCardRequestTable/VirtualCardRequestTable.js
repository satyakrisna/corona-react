import React, { useState, Fragment, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from '@material-ui/styles';
import {
    Card,
  CardContent,
  Table,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Select,
  MenuItem,
  Typography,
  IconButton,
  Stepper,
  Step,
  StepLabel
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import CurrencyFormat from 'react-currency-format';
import { StatusBullet } from 'components';
import requestService from 'services/request'

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
      padding: 0
    },
    inner: {
      minWidth: 1050
    },
    nameContainer: {
      display: 'flex',
      alignItems: 'center'
    },
    avatar: {
      marginRight: theme.spacing(2)
    },
    status: {
      marginRight: theme.spacing(1)
    },
    row: {
      height: '42px',
      display: 'flex',
      alignItems: 'center',
      marginTop: theme.spacing(1)
    },
    spacer: {
      flexGrow: 1
    },
    actions: {
      justifyContent: 'flex-end'
    },
    table: {
      width: "20%",
      backgroundColor: '#FAFAFA'
    },
    dialog: {
      width: "100%",
      float: 'left',
      marginRight:50
    },
    image: {
      display: 'inline-block',
      maxWidth: '100%',
      width: 300
    },
    pagination:{
      marginLeft: theme.spacing(3)
    }
}));

const statusColors = {
  DONE: 'success',
  ACCEPTED: 'success',
  SUBMITTED: 'warning',
  REJECTED: 'danger'
};

function getSteps() {
  return ['Employee', 'Head of Division', 'Head of Branch', 'Head Office'];
}

const VirtualCardRequestTable = props => {
    const location = useLocation();

    const {requests, loading, size, page, count, pages, handleError, handleErrorLogout} = props;
    const classes = useStyles();

    const [openDetail, setOpenDetail] = useState(false);
    const [request, setRequest] = useState({});
    const [user, setUser] = useState({});
    const [steps] = useState(getSteps());
    const [activeStep, setActiveStep] = useState(0);

    const access_token = JSON.parse(localStorage.getItem("data")).access_token;
    const rank_level = JSON.parse(localStorage.getItem("data")).rank_level;

    const isStepFailed = (step) => {
    
      if(rank_level===2){
        return step === 1;
       }else if(rank_level===3){
        return step === 2;
       }else{
        return step === 3;
       }
    };

    useEffect(()=>{
      let url = location.search
      let token = url.split("?")
  
      if(url !== "" && token[1] !== "") {
        requestService.get(`/api/vcr/${token[1]}`, {
          headers: {
            'Authorization': `Bearer ${access_token}`
          }
         }).then(res =>{
            const data =  res.data.output_schema
            setRequest(data.virtual_card_request)
            setUser(data.user)
            if(rank_level===2){
              setActiveStep(2);
             }else if(rank_level===3){
              setActiveStep(3);
             }else{
              setActiveStep(4);
             }
             setOpenDetail(true);
         }).catch(err=>{
          if(err.response){
            if(401 === err.response.status ) {
              handleErrorLogout("Your session was invalid")
            } else if (403 === err.response.status) {
              const message = err.response.data.error_schema.error_message.indonesian
              handleErrorLogout(message)
            } else{
              const message = err.response.data.error_schema.error_message.indonesian
              handleError(message)
            }
          }else{
            handleErrorLogout("Something went wrong")
          }
         })
      }
      location.search = ""
      
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[location.search])
  
    const handleClickOpenDetail = (id) => {
      requestService.get(`/api/vcr/${id}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const data =  res.data.output_schema
          setRequest(data.virtual_card_request)
          setUser(data.user)
          if(rank_level===2){
            setActiveStep(2);
           }else if(rank_level===3){
            setActiveStep(3);
           }else{
            setActiveStep(4);
           }
           setOpenDetail(true);
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
       })
    };
    
    const handleCloseDetail = () => {
        setOpenDetail(false);
    };
    

    const handleBackButtonClick = () => {
      if(page>0){
        props.onPageChange(page-1);
        props.onLoadingChange(true);
      }else{
        alert("this is min page")
      }
    };
  
    const handleNextButtonClick = () => {
      if(page+1<pages){
        props.onPageChange(page+1);
        props.onLoadingChange(true);
      }else{
        alert("this is max page")
      }
    };
  
    const handleRowsPerPageChange = event => {
      props.onSizeChange(event.target.value);
      props.onPageChange(0);
      props.onLoadingChange(true);
    };

    return (
        <Fragment>
            <PerfectScrollbar>
            <div className={classes.inner}>
                <Table>
                <TableHead>
                    <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Request ID</TableCell>
                    <TableCell>Employee Name</TableCell>
                    <TableCell>Category</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Date</TableCell>
                    <TableCell>Status</TableCell>
                    </TableRow>
                </TableHead>
                {loading ?
              <TableBody>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
            </TableBody>
          : 
                <TableBody>
                  {requests.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='7' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                    requests.slice(0, size).map(request => (
                    <TableRow
                        className={classes.tableRow}
                        hover
                        key={request.uuid}
                    >
                        <TableCell style={{width: '17%'}}><IconButton onClick={() => { handleClickOpenDetail(request.uuid)}}><VisibilityIcon/></IconButton></TableCell>
                        <TableCell>{request.invoice}</TableCell>
                        <TableCell>{request.name}</TableCell>
                        <TableCell>{request.categories}</TableCell>
                        <TableCell><CurrencyFormat value={request.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                        <TableCell>
                        {moment(request.created_date).format('DD/MM/YYYY')}
                        </TableCell>
                        <TableCell>
                        <div className={classes.statusContainer}>
                            <StatusBullet
                            className={classes.status}
                            color={statusColors[request.status]}
                            size="sm"
                            />
                            {request.status}
                        </div>
                        </TableCell>
                    </TableRow>
                    ))}
                </TableBody>
                    }
                </Table>
                <Dialog
                open={openDetail}
                onClose={handleCloseDetail}
                fullWidth
                maxWidth="md"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">
                    <div className={classes.row}>
                  <Typography variant="h4" >
                  Detail Request
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDetail}>
                    <CloseIcon />
                  </IconButton>
                </div>
                </DialogTitle>
                <DialogContent>
                    <Card>
                    <CardContent>
                    <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label, index) => {
                      const labelProps = {};
                      if(request.status==="REJECTED"){
                        if(isStepFailed(index)){
                          labelProps.error = true;
                        }
                      }
                        return (
                          <Step key={label}>
                            <StepLabel {...labelProps}>{label}</StepLabel>
                          </Step>
                        )
                        })}
                  </Stepper>
                        <div className={classes.dialog}>
                        <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Request Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Request ID</TableCell>
                        <TableCell>{request.invoice}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Category</TableCell>
                        <TableCell>{request.categories}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell><CurrencyFormat value={request.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Description</TableCell>
                        <TableCell>{request.description}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date</TableCell>
                        <TableCell>
                            {moment(request.start_date).format('DD/MM/YYYY')} - {moment(request.end_date).format('DD/MM/YYYY')}
                          </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Attachment</TableCell>
                        <TableCell>
                          {request.file_path!==""&&request.file_path!==null? <a href = {request.file_path} target = "_blank" rel="noopener noreferrer"><FileCopyIcon/></a>: "-"}
                          </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date Submitted</TableCell>
                        <TableCell>
                            {moment(request.created_date).format('DD/MM/YYYY')}
                          </TableCell>
                      </TableRow>
                      {request.status==="REJECTED"?
                      <TableRow>
                        <TableCell className={classes.table}>Deny Reason</TableCell>
                        <TableCell>{request.note}</TableCell>
                      </TableRow>
                      :null}
                    </TableBody>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Employee Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                        </div>
                    </CardContent>
                </Card>
                </DialogContent>
                <DialogActions>
                </DialogActions>
                </Dialog>
            </div>
            </PerfectScrollbar>
            <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
        </Fragment>
    )
};

VirtualCardRequestTable.propTypes = {
    className: PropTypes.string,
    requests: PropTypes.array.isRequired
};
  
export default VirtualCardRequestTable;