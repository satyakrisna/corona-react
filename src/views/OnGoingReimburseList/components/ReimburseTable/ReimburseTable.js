import React, { useState, useEffect, Fragment } from 'react';
import {useLocation, useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from '@material-ui/styles';
import Geocode from "react-geocode";
import {
  Card,
  CardMedia,
  CardContent,
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  IconButton,
  TextField,
  Select,
  MenuItem,
  Stepper,
  Step,
  Tooltip,
  StepLabel,
  Hidden
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import CurrencyFormat from 'react-currency-format';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import request from 'services/request'
import GoogleMapReact from 'google-map-react';
import validate from 'validate.js';

const schema = {
  note: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
}

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    width: "25%",
    backgroundColor: '#FAFAFA'
  },
  danger: {
    color: '#E53935'
  },
  dialog: {
    width: "60%",
    float: 'left'
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  pagination:{
    marginLeft: theme.spacing(3)
  },
  text: {
    paddingBottom: '1%',
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      marginTop: theme.spacing(2)
    }
  }, gridMapsKiri: {
    paddingRight: '1%',
    [theme.breakpoints.down('md')]: {
      padding: '0'
    }
  }, gridMapsKanan: {
    paddingLeft: '1%',
    [theme.breakpoints.down('md')]: {
      padding: '0'
    }
  }
}));

function getSteps() {
  return ['Employee', 'Head of Division', 'Head of Branch', 'Head Office'];
}

const ReimburseTable = props => {
  const { reimburses, getData, loading, handleSuccess, handleError, handleError401, size, page, count, pages} = props;

  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();

  const [openDetail, setOpenDetail] = useState(false);
  const [openApprove, setOpenApprove] = useState(false);
  const [openDeny, setOpenDeny] = useState(false);
  const [reimburse, setReimburse] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });
  const [user, setUser] = useState({});
  const [steps] = useState(getSteps());
  const [activeStep, setActiveStep] = useState(0);
  const [coorStart, setCoorStart] = useState({})
  const [coorEnd, setCoorEnd] = useState({})
  const [loginLocation, setLoginLocation] = useState();
  const [transactionLocation, setTransactionLocation] = useState();
  const [center, setCenter] = useState({});
  const [zoom, setZoom] = useState();

  // GeoCoding
  Geocode.setApiKey("AIzaSyC914Z_xet-plrGvaCX9fAj2yAtcvXN_sA");
  Geocode.setLanguage("en");
  Geocode.enableDebug();

  const AnyReactComponent = ({ text }) => (
    <Tooltip title={`${text}`} placement="top">
      <img src="/images/marker_2.png" height="50px" style={{marginTop: '-50px'}} alt="loading"/>
    </Tooltip>
  );

  const AnyReactComponentTransaction = ({ text }) => (
    <Tooltip title={`${text}`} placement="top">
      <img src="/images/marker_1.png" height="50px" style={{marginTop: '-50px'}} alt="loading"/>
    </Tooltip>
  );

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

  useEffect(()=>{
    let url = location.search
    let token = url.split("?")

    if(url !== "" && token[1] !== "") {
      request.get(`/api/reimburse/${token[1]}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
          const data = res.data.output_schema
          if(data.reimburse.status === "SUBMITTED") {
            setReimburse({
              isValid: false,
              values: data.reimburse,
              touched: {},
              errors: {}
            })
            setUser(data.user)
            if(rank_level===2){
              setActiveStep(1);
            }else if(rank_level===3){
              setActiveStep(2);
            }else{
              setActiveStep(3);
            }
            setOpenDetail(true);
    
            googleMaps(data.reimburse)
          } else {
            history.push(`/admin/reimburses-finished?${token[1]}`)
          }
  
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleError401("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleError401(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleError401("Something went wrong")
        }
       })
    }
    location.search = ""
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[location.search])

  useEffect(() => {
    const errors = validate(reimburse.values, schema);

    setReimburse(reimburse => ({
      ...reimburse,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [reimburse.values]);

  const handleOnChange = (event) => {
    event.persist();

    setReimburse(reimburse => ({
      ...reimburse,
      values: {
        ...reimburse.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...reimburse.touched,
        [event.target.name]: true
      }
    }));
  }

  const handleClickOpenEdit = (id) => {

    request.get(`/api/reimburse/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema
        setReimburse({
          isValid: false,
          values: data.reimburse,
          touched: {},
          errors: {}
        })
        setUser(data.user)
        if(rank_level===2){
          setActiveStep(1);
        }else if(rank_level===3){
          setActiveStep(2);
        }else{
          setActiveStep(3);
        }
        setOpenDetail(true);

        googleMaps(data.reimburse)

     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const googleMaps = maps => {
    let latStart = Number(maps.coordinate_start.split(",")[0])
    let lngStart = Number(maps.coordinate_start.split(",")[1])
    let latEnd = Number(maps.coordinate_end.split(",")[0])
    let lngEnd = Number(maps.coordinate_end.split(",")[1])

    setCoorStart({
      lat: latStart,
      lng: lngStart
    })

    setCoorEnd({
      lat: latEnd,
      lng: lngEnd
    })

    setCenter({
      lat: (latStart + latEnd) / 2,
      lng: (lngStart + lngEnd) / 2
    })

    Geocode.fromLatLng(latStart, lngStart).then(
      response => {
        const address = response.results[0].formatted_address;
        setLoginLocation(address)
      },
      error => {
        console.error(error);
      }
    );

    Geocode.fromLatLng(latEnd, lngEnd).then(
      response => {
        const address = response.results[0].formatted_address;
        setTransactionLocation(address)
      },
      error => {
        console.error(error);
      }
    );

    console.log(latStart + 0.001)
    console.log(lngStart + 0.001)

    if (Math.abs(latStart - latEnd) <= 0.001 || Math.abs(lngStart - lngEnd) <=0.001) {
      setZoom(22)
    } else if (Math.abs(latStart - latEnd) <= 1 || Math.abs(lngStart - lngEnd) <= 1) {
      setZoom(14)
    } else if (Math.abs(latStart - latEnd) <= 1.5 || Math.abs(lngStart - lngEnd) <= 1.5) {
      setZoom(7)
    } else if (Math.abs(latStart - latEnd) <= 2 || Math.abs(lngStart - lngEnd) <= 2) {
      setZoom(5)
    } else {
      setZoom(1)
    }
  }

  const handleApprove = () => {
    request({
      method: 'PUT',
      url: '/api/reimburse',
      data: {
        "uuid": reimburse.values.uuid,
        "accepted": true,
        "note": reimburse.values.note
      },
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenDetail(false);
      setOpenApprove(false);
      getData();
      handleSuccess('Data successfully updated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleDeny = () => {
    request({
      method: 'PUT',
      url: '/api/reimburse',
      data: {
        "uuid": reimburse.values.uuid,
        "accepted": false,
        "note": reimburse.values.note
      },
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenDetail(false);
       setOpenDeny(false);
       getData();
       handleSuccess('Data successfully updated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleCloseDetail = () => {
    setOpenDetail(false);
  };

  const handleClickOpenApprove = () => {
    setOpenApprove(true);
  };

  const handleCloseApprove = () => {
    setOpenApprove(false);
  };
  const handleClickOpenDeny = () => {
    setOpenDeny(true);
  };

  const handleCloseDeny = () => {
    setOpenDeny(false);
  };

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
    props.getData();
  };

  const hasError = field =>
  reimburse.touched[field] && reimburse.errors[field] ? true : false;

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell>Reimburse ID</TableCell>
                  <TableCell>Employee Name</TableCell>
                  <TableCell>Category</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Date</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
              <TableBody>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
              <TableRow>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
                <TableCell><Skeleton/></TableCell>
              </TableRow>
            </TableBody>
          : 
              <TableBody>
                {reimburses.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='6' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :reimburses.slice(0, size).map(reimburse => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={reimburse.uuid}
                  >
                    <TableCell><IconButton onClick={() => { handleClickOpenEdit(reimburse.uuid)}}><VisibilityIcon/></IconButton></TableCell> 
                    <TableCell>{reimburse.invoice}</TableCell>
                    <TableCell>{reimburse.name}</TableCell>
                    <TableCell>{reimburse.categories}</TableCell>
                    <TableCell><CurrencyFormat value={reimburse.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                    <TableCell>
                      {moment(reimburse.created_date).format('DD/MM/YYYY')}
                    </TableCell> 
                    </TableRow>
                ))}
              </TableBody>
              }
            </Table>
            <Dialog
              open={openDetail}
              onClose={handleCloseDetail}
              fullWidth
              maxWidth="md"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
              <div className={classes.row}>
                  <Typography variant="h4" >
                   Detail Reimburse
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDetail}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <Card>
                <CardContent>
                  <Stepper activeStep={activeStep} alternativeLabel>
                    {steps.map((label) => (
                      <Step key={label}>
                        <StepLabel>{label}</StepLabel>
                      </Step>
                    ))}
                  </Stepper>
                  <div>
                  <Hidden smDown>
                    <div className={classes.dialog}>
                      
                    <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Reimburse Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Reimburse ID</TableCell>
                        <TableCell>{reimburse.values.invoice}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Category</TableCell>
                        <TableCell>{reimburse.values.categories}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell><CurrencyFormat value={reimburse.values.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Description</TableCell>
                        <TableCell>{reimburse.values.description}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date</TableCell>
                        <TableCell>
                            {moment(reimburse.values.created_date).format('DD/MM/YYYY')}
                          </TableCell>
                      </TableRow>
                    </TableBody>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Employee Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                    </div>
                    <div> 
                    <Card>
                    <CardMedia
                      component="img"
                      alt="Bill"
                      image={reimburse.values.file_path}
                    />
                    </Card>
                    </div>
                    </Hidden>
                    <Hidden lgUp>
                    <div >
                      
                    <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Reimburse Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Reimburse ID</TableCell>
                        <TableCell>{reimburse.values.invoice}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Category</TableCell>
                        <TableCell>{reimburse.values.categories}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell>{reimburse.values.amount}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Description</TableCell>
                        <TableCell>{reimburse.values.description}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date</TableCell>
                        <TableCell>
                            {moment(reimburse.values.created_date).format('DD/MM/YYYY')}
                          </TableCell>
                      </TableRow>
                    </TableBody>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Employee Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                    </div>
                    <div> 
                    <Card>
                    <CardMedia
                      component="img"
                      alt="Bill"
                      image={reimburse.values.file_path}
                    />
                    </Card>
                    </div>
                    </Hidden>
                  </div>
                    
                  </CardContent>
              </Card>

              <div
                style={{
                  marginTop: '3%',
                  width: '100%',
                  height: '60vh'
                }}>

            <GoogleMapReact
                bootstrapURLKeys={{ key: "AIzaSyC914Z_xet-plrGvaCX9fAj2yAtcvXN_sA" }}
                defaultCenter={center}
                defaultZoom={zoom}
            >
                <AnyReactComponent
                    lat={coorStart.lat}
                    lng={coorStart.lng}
                    text={`${loginLocation}`}
                />

                <AnyReactComponentTransaction
                    lat={coorEnd.lat}
                    lng={coorEnd.lng}
                    text={`${transactionLocation}`}
                />

            </GoogleMapReact>
              </div>

              </DialogContent>
              <DialogActions>
                <img style={{marginLeft: '15px'}} src="/images/transactionBox.png" width="15px" alt="loading"/>
                <Typography style={{fontWeight: '500'}}>
                  Login Location
                </Typography>
                
                <img style={{marginLeft: '15px'}} src="/images/loginBox.png" width="15px" alt="loading"/>
                <Typography style={{fontWeight: '500'}}>
                  Transaction Location
                </Typography>

                <div style={{flexGrow: 1}}></div>
                <Button onClick={handleClickOpenDeny} color="secondary">
                  Deny
                </Button>
                <Button onClick={handleClickOpenApprove} color="primary" autoFocus>
                  Approve
                </Button>
              </DialogActions>
            </Dialog>
            <Dialog
              open={openDeny}
              onClose={handleCloseDeny}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
              <div className={classes.row}>
                  <Typography variant="h4" >
                   Deny Reason
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDeny}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                  <Card>
                    <CardContent>
                      <TextField
                        autoFocus
                        margin="dense"
                        error={hasError('note')}
                            helperText={
                              hasError('note') ? reimburse.errors.note[0] : null
                            }
                        name="note"
                        label="Description"
                        type="text"
                        fullWidth
                        onChange={handleOnChange} 
                        value={reimburse.values.note|| ''}
                      />
                    </CardContent>
                  </Card>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDeny} >
                  Cancel
                </Button>
                <Button onClick={handleDeny} color="secondary" autoFocus disabled={!reimburse.isValid}>
                  Deny
                </Button>
              </DialogActions>
            </Dialog>
            <Dialog
              open={openApprove}
              onClose={handleCloseApprove}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
              <div className={classes.row}>
                  <Typography variant="h4" >
                   Confirmation
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseApprove}>
                    <CloseIcon />
                  </IconButton>
                </div>
                </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Are you sure want to approve this reimburse?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseApprove} >
                  Cancel
                </Button>
                <Button onClick={handleApprove} color="primary" autoFocus>
                  Approve
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
    </Fragment>
  );
};

ReimburseTable.propTypes = {
  className: PropTypes.string,
  reimburses: PropTypes.array.isRequired
};

export default ReimburseTable;
