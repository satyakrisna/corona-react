import React, { useState, useEffect, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import Countdown from 'react-countdown';
import {
  Grid,
  Button,
  TextField,
  InputAdornment,
  IconButton,
  Box,
  Typography,
  Dialog,
  DialogContent,
  DialogContentText
} from '@material-ui/core';
import auth from 'services/auth'
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import ReCAPTCHA from "react-google-recaptcha";

const schema = {
  username: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    background: 'linear-gradient(45deg, rgba(0,65,195,1) 0%, rgba(0,77,230,1) 49%)',
    display: 'block',
    minHeight: '100vh',
    padding: theme.spacing(8),
    [theme.breakpoints.down('md')]: {
      padding: 0
    },
  },
  box: {
    background: '#FFFFFF',
    display: 'block',
    minHeight: '100%',
    borderRadius: '5px',
    [theme.breakpoints.down('md')]: {
      minHeight: '100vh',
      borderRadius: '0'
    },
  },
  loginGrid: {
    display: 'block',
    height: 'auto',
    top: '0',
    bottom: '0',
    padding: theme.spacing(4)
  }, 
  row: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.down('md')]: {
      display: 'block',
    },
  },
  logo: {
    fontFamily: 'Poppins',
    marginLeft: theme.spacing(1),
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      fontSize: '1.5em'
    },
  },
  welcome: {
    marginTop: theme.spacing(5),
    [theme.breakpoints.down('md')]: {
      textAlign: 'center'
    },
  },
  manage: {
    fontFamily: 'Poppins',
    color: '#808080',
    marginBottom: theme.spacing(4),
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      marginBottom: theme.spacing(5)
    },
  },
  textField: {
    marginBottom: theme.spacing(1)
  },
  label: {
    // fontWeight: '700',
    fontSize: '0.7em',
    fontFamily: 'Poppins',
  },
  labelError: {
    color: '#FF0000',
    fontSize: '0.7em',
    fontFamily: 'Poppins'
  },
  signInButton: {
    // background: 'linear-gradient(45deg, rgba(0,65,195,1) 0%, rgba(0,77,230,1) 80%)',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(5),
    fontSize: '0.9em',
    transition: "all ease 0.8s"
  },
  signInDisabled: {
    background: '#d9d9d9',
    color: '#FFFFFF',
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(5),
    fontSize: '0.9em',
  },
  imageGrid: {
    backgroundColor: 'rgba(242,242,242,0.2)',
    backgroundImage: `url('/images/Invoice-amico.svg')`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'auto 100%',
    backgroundPosition:'center center'
  },
  imageLogo: {
    width: '40px',
    [theme.breakpoints.down('md')]: {
      display: 'block',
      marginLeft: 'auto',
      marginRight: 'auto',
      width: '80px',
      marginBottom: theme.spacing(2.5),
      marginTop: theme.spacing(4)
    },
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
}));

const SignIn = props => {

  const { history } = props;

  const classes = useStyles();

  const [recaptcha, setRecaptcha] = useState(false)
  const [recaptchaResponse, setRecaptchaResponse] = useState(true)
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [title, setTitle]= useState();
  const [imgAge, setImgAge]=useState(Date.now);

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  useEffect(() => {
    document.title = "Corona | Login Page"
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const [passwordShown, setPasswordShown] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const togglePasswordVisiblity = () => {
    setPasswordShown(!passwordShown);
  };

  const handleError= (title, msg) =>{
    console.log('Handle Error')
    setTitle(title);
    setMessage(msg);
    setOpenFlashErr(true);
    setImgAge(Date.now());
  }

  useEffect(()=>{
    if(recaptcha) {
      setRecaptchaResponse(false)
    }
  },[recaptcha])

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      handleFlashErrClose()
      return <SignIn />
    } else {
        // Render a countdown
        return <span> {hours < 10 ? `0${hours}` : hours} : {minutes < 10 ? `0${minutes}` : minutes} : {seconds < 10 ? `0${seconds}` : seconds}</span>;
        // setTimer(`${hours} : ${minutes} : ${seconds}`)
    }
};

  const handleSignIn = event => {
    event.preventDefault();
    const token = Buffer.from(`webapp:abcd`, 'utf8').toString('base64');
    var bodyFormData = new FormData();
    bodyFormData.set('grant_type', 'password');
    bodyFormData.set('username', `${formState.values.username}`);
    bodyFormData.set('password', `${formState.values.password}`);

    // Axios({
    auth({
      method: 'POST',
      crossdomain: true,
      url: '/login',
      data: bodyFormData,
      headers: {
        'Authorization': `Basic ${token}`,  
        'Content-Type': 'application/json'
      }
    }).then(Response => {
      let statusCode = Response.status
      if(statusCode === 200 && recaptchaResponse) {
        localStorage.setItem("data", JSON.stringify(Response.data))
        history.push('/admin/dashboard');
      }
    }).catch(Error => {
      console.warn(Error.response)

      if(Error.response.data.recaptcha_on) {
        setRecaptcha(true)
      }

      if(Error.response.data.banned_due_date !== undefined) {
        handleError(`${Error.response.data.banned_due_date}`, `Your account is disabled until ${Error.response.data.banned_due_date}`)
      }
      setErrorMessage('The email and password you entered did not match our records. Please double-check and try again.')
    })
  };

  const onChange = event => {
    setRecaptchaResponse(true)
  }

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (

    <div className={classes.root}>
      <Box
        boxShadow={5}
        className={classes.box}>
          <Grid container>
            <Grid item
              className={classes.loginGrid}
              xl={5}
              lg={5}
              md={12}
              sm={12}
              xs={12}>

              {/* Logo Corona */}
                <div className={classes.row}>
                  <img className={classes.imageLogo} src="/images/logos/logo1.png" alt="logo"/>
                  <Typography
                    variant="h4"
                    className={classes.logo}>corona</Typography>
                </div>

              {/* Greeting Welcome */}
                <Typography
                    variant="h3"
                    className={classes.welcome}>Welcome to Corona</Typography>
                <Typography
                  className={classes.manage}>Manage your expenses seamlessly & intuitively</Typography>

              {/* Form Login */}
                <form
                  className={classes.form}
                  onSubmit={handleSignIn}
                >
                <Typography style={{marginBottom: '5%'}} variant="body1" color="secondary">{errorMessage}</Typography>

                <Typography
                  className={hasError('username') ? classes.labelError : classes.label}>Email Address</Typography>
                <TextField
                  className={classes.textField}
                  variant="outlined"
                  error={hasError('username')}
                  fullWidth
                  helperText={
                    hasError('username') ? formState.errors.username[0] : null
                  }
                  name="username"
                  onChange={handleChange}
                  type="text"
                  value={formState.values.username || ''}
                />

                <Typography
                  className={hasError('password') ? classes.labelError : classes.label}>Password</Typography>
                <TextField
                  className={classes.textField}
                  variant="outlined"
                  error={hasError('password')}
                  fullWidth
                  helperText={
                    hasError('password') ? formState.errors.password[0] : null
                  }
                  name="password"
                  onChange={handleChange}
                  type={passwordShown ? "text" : "password"}
                  value={formState.values.password || ''}
                  InputProps={{ endAdornment:
                    <InputAdornment position="end">
                      <IconButton
                        onClick={togglePasswordVisiblity}
                      >
                        {passwordShown ? <VisibilityIcon /> : <VisibilityOffIcon />}
                      </IconButton>
                    </InputAdornment>
                  }}
                />

                  {
                    recaptcha ?
                    <center>
                      <ReCAPTCHA
                        style={{
                          marginTop: '5%'
                        }}
                        sitekey="6LdmP7MZAAAAAJdDAAcDItvEQn2CIlrKTq0w9oaS"
                        render="explicit"
                        onChange={onChange}
                      />
                    </center>
                    :

                    <Fragment></Fragment>

                  }
                <Button
                  className={classes.signInButton}
                  fullWidth
                  color="primary"
                  disabled={!formState.isValid || !recaptchaResponse }
                  // fullWidth
                  size="large"
                  type="submit"
                  variant="contained"
                >
                  Sign in
                </Button>
              </form>
            </Grid>
            <Grid item
              className={classes.imageGrid}
              xl={7}
              lg={7}>

            </Grid>
          </Grid>
      </Box>

      <Dialog
        onClose={handleFlashErrClose}
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center', marginBottom: '10px'}}>
              <Countdown
                  // date={`2020-08-10 12:00:22.009`}
                  date={title}
                  renderer={renderer}
              />
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
      </Dialog>

    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object
};

export default withRouter(SignIn);