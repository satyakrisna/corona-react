import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    name: 'CTS',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000
  },
  {
    id: uuid(),
    name: 'DIS',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000
  },
  {
    id: uuid(),
    name: 'IBO',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000
  },
];
