import React, { useState, Fragment, useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  IconButton,
  Typography,
  Select,
  MenuItem
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import auth from 'services/auth';
import validate from 'validate.js';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  status: {
    marginRight: theme.spacing(1)
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  loading: {
    maxHeight: 75,
    marginLeft:450,
    marginTop:10,
    marginBottom:10,
  },
  pagination:{
    marginLeft: theme.spacing(3)
  }
}));

const DivisionTable = props => {
  const { getData, divisions, loading, handleSuccess, size, page, count, pages, handleError, handleError401} = props;

  const classes = useStyles();

  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [division, setDivision] = useState({});

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleOnChange = (event) => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  const handleClickOpenEdit = (id) => {
    auth.get(`/api/division/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
      const data = res.data.output_schema.division
      setFormState({
        isValid: false,
        values: data,
        touched: {},
        errors: {}
      })
       setOpenEdit(true);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleUpdate = () => {
    auth({
      method: 'PUT',
      url: '/api/division',
      data: formState.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenEdit(false);
        getData();
        handleSuccess('Data successfully updated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleClickOpenDelete = (id) => {
    setOpenDelete(true);
    setDivision({
      'uuid': id,
      'is_deleted': true
    })
  };

  const handleDelete = () => {
    auth({
      method: 'PUT',
      url: '/api/division',
      data: division,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenDelete(false);
        getData();
        handleSuccess('Data successfully deleted');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Created Date</TableCell>
                  <TableCell>Last Updated</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
                <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                </TableBody>
              : 
              <TableBody>
                {divisions.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='4' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                divisions.slice(0, size).map(division => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={division.uuid}
                  >
                    <TableCell style={{width: '17%'}}>
                      <Tooltip title={`Edit ${division.name}`} placement="top">
                        <IconButton onClick={() => { handleClickOpenEdit(division.uuid)}}><CreateIcon/></IconButton>
                      </Tooltip>
                      <Tooltip title={`Delete ${division.name}`} placement="top">
                        <IconButton onClick={() => { handleClickOpenDelete(division.uuid)}}><DeleteIcon/></IconButton>
                      </Tooltip>
                    </TableCell>
                    <TableCell>{division.name}</TableCell>
                    <TableCell>
                      {moment(division.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell>
                      {moment(division.modify_date).format('DD/MM/YYYY')}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              
            }
            </Table>
            <Dialog
              open={openEdit}
              onClose={handleCloseEdit}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Edit Division
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseEdit}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
                <DialogContent>
                  <TextField
                      autoFocus
                      margin="dense"
                      error={hasError('name')}
                      helperText={
                        hasError('name') ? formState.errors.name[0] : null
                      }
                      name="name"
                      label="Division Name"
                      type="text"
                      onChange={handleOnChange} 
                      value={formState.values.name}
                      fullWidth
                  />
                </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseEdit} color="secondary">
                  Cancel
                </Button>
                <Button onClick={handleUpdate} color="primary" autoFocus disabled={!formState.isValid}>
                  Save
                </Button>
              </DialogActions>
            </Dialog>
            <Dialog
              open={openDelete}
              onClose={handleCloseDelete}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Confirmation
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDelete}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Are you sure want to delete this division?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDelete} >
                  Cancel
                </Button>
                <Button onClick={handleDelete} color="secondary" autoFocus>
                  Delete
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
  </Fragment>
  );
};

DivisionTable.propTypes = {
  className: PropTypes.string,
  divisions: PropTypes.array.isRequired
};

export default DivisionTable;
