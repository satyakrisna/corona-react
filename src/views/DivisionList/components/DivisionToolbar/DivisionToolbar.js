import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Button, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Tooltip,
  Hidden,
  IconButton, Typography} from '@material-ui/core';

import { SearchInput } from 'components';
import SearchIcon from '@material-ui/icons/Search';
import PostAddIcon from '@material-ui/icons/PostAdd';
import CloseIcon from '@material-ui/icons/Close';
import auth from 'services/auth';
import validate from 'validate.js';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  success: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    float: 'right'
  },
  primary: {
    background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    marginRight: '0'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  successMobile: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    color: '#FFFFFF'
  },
}));

const DivisionToolbar = props => {
  const { getData, handleSuccess, handleError, handleError401} = props;
  const [openAdd, setOpenAdd] = useState(false);

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const company_id = JSON.parse(localStorage.getItem("data")).company_id

  const initialDivisionState = {
    uuid: null,
    company_id: company_id,
    name: ""
  };
  
  const [division, setDivision] = useState({
    isValid: false,
    values: initialDivisionState,
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(division.values, schema);

    setDivision(division => ({
      ...division,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [division.values]);

  const [word, setWord] = useState("");
  
  const handleSave = () => {
    auth({
      method: 'POST',
      url: '/api/division',
      data: division.values ,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenAdd(false);
        getData();
        setDivision({
          isValid: false,
          values: initialDivisionState,
          touched: {},
          errors: {}
        });
        handleSuccess('Data successfully added');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleInputChange = event => {
    event.persist();

    setDivision(division => ({
      ...division,
      values: {
        ...division.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...division.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleClickOpenAdd = () => {
    setOpenAdd(true);
  };

  const handleCloseAdd = () => {
    setOpenAdd(false);
    setDivision({
      isValid: false,
      values: initialDivisionState,
      touched: {},
      errors: {}
    })
  };

  const handleSearchChange = (event) => {
    setWord(event.target.value);
  };

  const handleSearch = () => {
    props.onKeywordChange(word);
    props.onPageChange(0);
  };

  const classes = useStyles();

  const hasError = field =>
    division.touched[field] && division.errors[field] ? true : false;

  return (
    <div
      className={classes.root}
    >
      <div className={classes.row}>
        <SearchInput
          className={classes.searchInput}
          placeholder="Search division name"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>
        <span className={classes.spacer} />

        <Hidden mdDown>
        <Tooltip title="Add Division" placement="top">
          <Button
            onClick={handleClickOpenAdd}
            className={classes.success}
            variant="contained"
          >
            <PostAddIcon/>
          </Button>
        </Tooltip>
        </Hidden>
      </div>

      <Hidden mdUp>
          <Button
            fullWidth
            onClick={handleClickOpenAdd}
            className={classes.successMobile}
            variant="contained"
            startIcon={<PostAddIcon/>}
          >
            Add New Division
          </Button>
        </Hidden>

      <Dialog
              open={openAdd}
              onClose={handleCloseAdd}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Add Division
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton aria-label="close" onClick={handleCloseAdd}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
                <DialogContent>
                  <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('name')}
                    helperText={
                      hasError('name') ? division.errors.name[0] : null
                    }
                    name="name"
                    label="Division Name"
                    type="text"
                    onChange={handleInputChange} 
                    value={division.values.name}
                    fullWidth
                  />
                </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseAdd} color="secondary">
                  Cancel
                </Button>
                <Button onClick={handleSave} color="primary" autoFocus disabled={!division.isValid}>
                  Save
                </Button>
              </DialogActions>
            </Dialog>
    </div>
  );
};

DivisionToolbar.propTypes = {
  className: PropTypes.string
};

export default DivisionToolbar;
