import React, { useState, useEffect } from 'react';
import { withRouter, useLocation, useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import auth from 'services/auth'
import { makeStyles } from '@material-ui/styles';
import {
  Grid,
  Button,
  TextField,
  Dialog,
  DialogContent,
  DialogContentText,
  Typography
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    background: '#FFFFFF',
    height: '100%',
    width: '100%',
    '& label.Mui-focused': {
      color: '#0041c3',
      fontWeight: 'bold'
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#a6a6a6',
      },
      '&:hover fieldset': {
        borderColor: '#B3B3B3',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#0041c3',
      },
    },
  },
  imageLogo: {
    width: '25vh',
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '10%',
    marginBottom: '5%',
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  grid: {
    top: '0',
    bottom: '0',
    paddingLeft: '3%',
    paddingRight: '3%',
    height: '100vh',
    border: '1px solid #f2f2f2',
    borderTop: '0',
    borderBottom: '0'
  },
  formTextField: {
    marginBottom: '3vh'
  },
  btnSubmit: {
    background: '#0041C3',
    color: '#FFFFFF',
    marginTop: '2vh',
    transition: "all ease 0.8s",
    '&:hover' : {
    //   boxShadow: "inset 1100px 0 0 0 #002266"
        background: '#002266'
    },
  },
  btnDisabled: {
    background: '#cccccc',
    color: '#FFFFFF',
    marginTop: '2vh',
    '&:hover' : {
        background: '#cccccc'
    },
    cursor: 'not-allowed'
  }
}));

const Confirmation = props => {
  const location = useLocation();
  const history = useHistory();
  const classes = useStyles();
  const [error, setError] = useState(false)
  const [data, setData] = useState({});
  const [form, setForm] = useState({
    password: '',
    confirmPassword: ''
  })

  const [message, setMessage]= useState('');
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [imgAge, setImgAge]=useState(Date.now);

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
        history.push("/admin/dashboard")
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleErrorHome = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        history.push("/home")
      },
      1500
    );
    setImgAge(Date.now());
  }

  useEffect(()=>{
    let url = location.search
    let token = url.split("?")

    if(url === "" || token[1] === "") {
       let splitToken = token[1].split("=")

       if(splitToken[0] !== "token") {
          history.push("/not-found")
       }
    }

    console.log(token[1])

    auth({
        method: 'GET',
        url: `/api/verification?${token[1]}`
    }).then(res=>{
        const baseData = res.data.output_schema
        setData(baseData.user)
    }).catch(err=>{
      history.push("/not-found")
      // const error = err.response.data.error_schema.error_message.indonesian
      // handleErrorNotFound(error)
    })
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  useEffect(()=>{
    if(form.password !== form.confirmPassword) {
        setError(true)
    } else {
        setError(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[form.password,form.confirmPassword])

  const handleFormChange = event => {
    const { name, value } = event.target;
    setForm({ ...form, [name]: value });
  }

  const handleSubmit = event => {
      event.preventDefault();

      if((form.password !== '' && form.confirmPassword !== '') || error) {
        auth({
            method: 'PUT',
            url: `/api/verification`,
            data: {
                id: data.uuid,
                new_password: form.password
            }
        }).then(res=>{
            // alert(`Success!\nLogin with ${data.email}`)
            handleSuccess('Your account has been activated');
        }).catch(err=>{
            handleErrorHome('Your access key was not valid');
        })
      }
  }

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid
            item
            lg={4}
        >
        </Grid>

        <Grid
            item
            lg={4}
            md={12}
            sm={12}
            xs={12}
            className={classes.grid}
        >
            <img
            className={classes.imageLogo}
            src="images/logos/logo.png" alt="logo"/>

            <Typography
                variant="h2"
                style={{
                    textAlign: 'center',
                    marginBottom: '1vh'
                }}
            >
                Welcome
            </Typography>

            <Typography
                variant="h4"
                style={{
                    textAlign: 'center',
                    marginBottom: '7vh'
                }}
            >
                {data.name}
            </Typography>

            <Typography
                variant="h6"
                style={{
                    marginBottom: '3vh'
                }}
            >
                Please enter your password
            </Typography>

            <form onSubmit={handleSubmit}>
            <TextField
                className={classes.formTextField}
                onChange={handleFormChange}
                error={error ? true : false}
                type="password"
                name="password"
                label="Password"
                variant="outlined"
                fullWidth
                helperText={error ? "Password doesn't match" : false}
            />

            <TextField
                className={classes.formTextField}
                error={error ? true : false}
                onChange={handleFormChange}
                type="password"
                name="confirmPassword"
                label="Confirm Password"
                variant="outlined"
                fullWidth
                helperText={error ? "Confirm password doesn't match" : false}
            />

            <Button
                type="submit"
                // style={error ? {background: '#cccccc'} : (form.password === '' && form.confirmPassword === '') ? {background: '#cccccc'} : {} }
                className={error ? classes.btnDisabled : (form.password === '' && form.confirmPassword === '') ? classes.btnDisabled : classes.btnSubmit}
                fullWidth
            >
                SET PASSWORD
            </Button>
            </form>
        </Grid>

      </Grid>


      <Dialog
        open={openFlash}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
              Success!
            </Typography>
            
          <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
        </DialogContent>
      </Dialog>


      <Dialog
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>

    </div>
  );
};

Confirmation.propTypes = {
  history: PropTypes.object
};

export default withRouter(Confirmation);