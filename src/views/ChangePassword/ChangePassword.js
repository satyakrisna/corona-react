import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import validate from 'validate.js';
import 'react-credit-cards/es/styles-compiled.css';
import SettingsIcon from '@material-ui/icons/Settings';
import auth from 'services/auth'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography,
  CardContent,
  Card,
  Grid,
  Box,
  TextField,
  Button,
  InputAdornment,
  IconButton,
} from '@material-ui/core'

import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const schema = {
  currentPassword: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  newPassword: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  },
  retypePassword: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    // backgroundImage: `url("/images/danger.png")`,
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  gridContainer: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(3)
  },
  containerSplitKiri: {
    marginBottom: '20px',
    [theme.breakpoints.up('md')]: {
        paddingRight: theme.spacing(1),
      }
  },
  containerSplitKanan: {
    marginBottom: '20px',
    [theme.breakpoints.up('md')]: {
        paddingLeft: theme.spacing(1),
      }
  },
  buttonRelease: {
      float: "right",
      backgroundImage: `url("/images/success.png")`,
      backgroundSize: "cover",
      backgroundPosition: "center center",
      color: "#FFFFFF",
      [theme.breakpoints.down('md')]: {
        width: "100%"
      }
  },
  textfield: {
    marginBottom: theme.spacing(2)
  },
  buttonSubmit: {
    float: 'right',
    marginTop: '1%',
    marginBottom: '4%',
    paddingLeft: '6%',
    paddingRight: '6%',
    background: 'linear-gradient(90deg, rgba(19,64,116,1) 0%, rgba(19,49,92,1) 49%)',
    color: '#FFFFFF',
    [theme.breakpoints.down('md')]: {
      width: "100%"
    },
    transition: "all ease 0.8s",
    '&:hover' : {
      boxShadow: "inset 500px 0 0 0 #578edb"
  },
  flexGrow: {
    flexGrow: 1
  }
  },
  buttonDisabled: {
    float: 'right',
    marginTop: '1%',
    marginBottom: '4%',
    paddingLeft: '6%',
    paddingRight: '6%',
    background: '#d9d9d9',
    color: '#FFFFFF',
    [theme.breakpoints.down('md')]: {
      width: "100%"
    }
  },
}));

const ChangePassword = props => {
  const history = useHistory();
  const classes = useStyles();

  const [errorMsg, setErrorMsg] = useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);

  const [CurrentPasswordShown, setCurrentPasswordShown] = useState(false);
  const [NewPasswordShown, setNewPasswordShown] = useState(false);
  const [RetypePasswordShown, setRetypePasswordShown] = useState(false);

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }
  
  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
        history.push("/admin/dashboard")
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleErrorLogout = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    document.title = "Corona | Change Password"
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  useEffect(()=>{
    if(formState.values.newPassword !== formState.values.retypePassword) {
      setErrorMsg(true)
    } else {
      setErrorMsg(false)
    }
  },[formState.values.newPassword, formState.values.retypePassword])

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  const togglePasswordVisiblity = () => {
    setCurrentPasswordShown(!CurrentPasswordShown);
  };

  const toggleNewPasswordVisiblity = () => {
    setNewPasswordShown(!NewPasswordShown);
  };

  const toggleRetypePasswordVisiblity = () => {
    setRetypePasswordShown(!RetypePasswordShown);
  };

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  }

  const [message, setMessage]= useState('');
  const [openFlash, setOpenFlash]= useState(false);
  const [imgAge, setImgAge]=useState(Date.now);

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  const handleSubmit = event => {
    event.preventDefault();

    auth({
      method: 'PUT',
      url: '/api/change-password',
      data: {
        id: JSON.parse(localStorage.getItem("data")).user_uuid,
        old_password: formState.values.currentPassword,
        new_password: formState.values.newPassword
      },
      headers: {
        'Authorization': `Bearer ${access_token}`, 
      }
    }).then(res=>{
      handleSuccess('Password successfully updated');
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  return (
    <div className={classes.root}>
    <Grid>
      <div className={classes.package}>
        <Box boxShadow={5} className={classes.row}>
          <Typography variant="h2" className={classes.iconColor}>
            <SettingsIcon className={classes.icon}/>
          </Typography>
        </Box>
          <Typography variant="h4" className={classes.title}>
          Settings
        </Typography>
      </div>  
      <Card style={{paddingTop: '65px'}}>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <TextField
              name="currentPassword"
              onChange={handleChange}
              className={classes.textfield}
              type={CurrentPasswordShown ? "text" : "password"}
              variant="outlined"
              label="Current Password"
              error={hasError('currentPassword')}
              helperText={
                hasError('currentPassword') ? formState.errors.currentPassword[0] : null
              }
              fullWidth
              InputProps={{ endAdornment:
                <InputAdornment position="end">
                  <IconButton
                    onClick={togglePasswordVisiblity}
                  >
                    {CurrentPasswordShown ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </InputAdornment>
              }}
            />

            <TextField
              className={classes.textfield}
              onChange={handleChange}
              name="newPassword"
              type={NewPasswordShown ? "text" : "password"}
              variant="outlined"
              label="New Password"
              fullWidth
              error={errorMsg ? true : hasError('newPassword') }
              helperText={
                errorMsg ? "New password doesn't match" : hasError('newPassword') ? formState.errors.newPassword[0] : null
              }
              InputProps={{ endAdornment:
                <InputAdornment position="end">
                  <IconButton
                    onClick={toggleNewPasswordVisiblity}
                  >
                    {NewPasswordShown ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </InputAdornment>
              }}
            />

            <TextField
              className={classes.textfield}
              onChange={handleChange}
              name="retypePassword"
              type={RetypePasswordShown ? "text" : "password"}
              variant="outlined"
              label="Retype New Password"
              error={errorMsg ? true : hasError('retypePassword') }
              helperText={
                errorMsg ? "Retype new password doesn't match" : hasError('retypePassword') ? formState.errors.retypePassword[0] : null
              }
              fullWidth
              InputProps={{ endAdornment:
                <InputAdornment position="end">
                  <IconButton
                    onClick={toggleRetypePasswordVisiblity}
                  >
                    {RetypePasswordShown ? <VisibilityIcon /> : <VisibilityOffIcon />}
                  </IconButton>
                </InputAdornment>
              }}
            />

          <Button
            type="submit"
            disabled={errorMsg || hasError('currentPassword') || hasError('newPassword') || hasError('retypePassword') || !formState.isValid ? true : false}
            className={errorMsg || hasError('currentPassword') || hasError('newPassword') || hasError('retypePassword') || !formState.isValid ? classes.buttonDisabled : classes.buttonSubmit}
          >
            Submit
          </Button>
        </form>
        </CardContent>
      </Card>
    </Grid>

    <Dialog
      onClose={handleFlashClose}
      open={openFlash}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
          <Typography variant="h2" style={{textAlign:'center'}}>
            Success!
          </Typography>
          
        <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
          {message}
        </DialogContentText>
      </DialogContent>
    </Dialog>

    <Dialog
      onClose={handleFlashErrClose}
      open={openFlashErr}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      >
      <DialogContent>
          <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
          <Typography variant="h2" style={{textAlign:'center'}}>
              Error!
          </Typography>
          
          <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
          {message}
          </DialogContentText>
      </DialogContent>
      </Dialog>

    </div>
  );
};

export default ChangePassword;
