import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';

import { BillTable } from './components';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography,
  CardContent,
  Card, Box, Grid, Tooltip, IconButton, Button, DialogTitle, DialogActions, Table, TableHead, TableRow, TableCell, TableBody
} from '@material-ui/core';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import payment from 'services/payment'
import moment from 'moment';
import CloseIcon from '@material-ui/icons/Close';
import CurrencyFormat from 'react-currency-format';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";
import auth from 'services/auth'
import { StatusBullet } from 'components';

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    loopCount: 0
  },
  table: {
    width: '50%',
    backgroundColor: '#FAFAFA'
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    // backgroundImage: `url("/images/danger.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  },
  inCard: {
    marginTop: theme.spacing(2)
  },
  row2: {
    display: 'flex',
    alignItems: 'center',
    width: '100%'
  },
  success: {
    backgroundImage: `url("/images/success.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5)
  }
}));

const BillList = () => {
  const history = useHistory();
  const classes = useStyles();

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const [bills, setBills] = useState([]);
  const [bill, setBill] = useState({});
  const [company, setCompany] = useState({});
  const [exportData, setExportData] = useState([]);
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [openPay, setOpenPay] = useState(false);
  var generated_date = moment(new Date()).format('DD/MM/YYYY');

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;

  const getData = () => {

    payment.get(`/api/bill`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema
        setBill(data.bill);
        setCompany(data.company);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

    payment.get(`/api/bills?size=${size}&page=${page}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.bills
        setBills(data.content);
        setCount(data.total_elements);
        setPages(data.total_pages);
        setLoading(false);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     payment.get(`/api/bills?size=99999&page=0`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.bills
        setExportData(data.content);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  }

  const handleClickOpenPay = (id) => {
    setOpenPay(true);
  };

  const handlePay = () => {
    payment({
      method: 'POST',
      url: '/api/payment',
      data: {
        'object_uuid': bill.uuid,
        'amount': bill.outstanding_balance,
        'type': 'virtual_card'
      },
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenPay(false);
        getData();
        handleSuccess('Data successfully updated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleClosePay = () => {
    setOpenPay(false);
  };

  useEffect(()=>{
    document.title = "Corona | Payment"
    getData()
    setLoading(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[size,page])

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "portrait";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    var today = moment(new Date()).format('DD/MM/YYYY');
    const title = "Bill History";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Date", "Amount"]];
    const date = `Generated on ${today}`;

    const data = exportData.map(data=> [moment(data.created_date).format('DD/MM/YYYY'), data.amount]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };

    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 495, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`bill_history_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
      <Grid>    
      <div className={classes.package}>
          <Box boxShadow={5} className={classes.row}>
            <Typography variant="h2" className={classes.iconColor}>
                <AccountBalanceWalletIcon className={classes.icon}/>
              </Typography>
          </Box>
            <Typography variant="h4" className={classes.title} >
              Payment
            </Typography>
        </div>  
      <Card style={{paddingTop: '45px'}}>
        <CardContent>
        <Card>
          <CardContent>
          <div className={classes.row2}>
            <Typography variant="h4" >Corona</Typography>
            <span className={classes.spacer} />
            <img src="/images/icon flat4.png" alt="logo" style={{height:75}}/>
          </div>
          <div className={classes.row2}>
            <Typography variant="body1" ><b>Statement of Account</b></Typography>
          </div>
          <Grid
            container
            spacing={4}
          >
            <Grid
              item
              lg={6}
              sm={12}
              xl={12}
              xs={12}
            >
              <center>
                <img src={company.logo} alt="logo" style={{height:150, marginTop: 50, marginBottom: 20}}/>
                <Typography variant="h3" >{bill.company}</Typography>
              </center>
            </Grid>
            <Grid
              item
              lg={6}
              sm={12}
              xl={12}
              xs={12}
            >
              <Table size="small">
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}><b>Bill Summary</b></TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Statement Date </TableCell>
                        <TableCell>{moment(bill.created_date).format('MMM DD, YYYY')}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Credit Limit </TableCell>
                        <TableCell><CurrencyFormat value={company.credit_limit} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => value} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Outstanding Balance</TableCell>
                        <TableCell><CurrencyFormat value={bill.outstanding_balance} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => value} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Interest Charges (1%)</TableCell>
                        <TableCell><CurrencyFormat value={bill.interest} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => value} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Total Outstanding Blance</TableCell>
                        <TableCell><CurrencyFormat value={bill.total_outstanding_balance} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => value} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Payment Due Date </TableCell>
                        <TableCell>{moment(bill.payment_due_date).format('MMM DD, YYYY')}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Status </TableCell>
                        <TableCell>{bill.flag_paid ? 
                          <div className={classes.statusContainer}>
                            <StatusBullet
                              className={classes.status}
                              color={'success'}
                              size="sm"
                            />
                            {" Paid"}
                          </div> : 
                          <div className={classes.statusContainer}>
                            <StatusBullet
                              className={classes.status}
                              color={'danger'}
                              size="sm"
                            />
                            {" Unpaid"}
                          </div>}
                        </TableCell>
                      </TableRow>
                    </TableBody>
                  </Table>
            </Grid>
            
          </Grid>
            <div className={classes.row2} style={{marginTop: 20}}>
              <span className={classes.spacer} />
              {!bill.flag_paid ?
                <Button
                  className={classes.success}
                  variant="contained"
                  onClick={handleClickOpenPay}
                >
                  Pay
                </Button>
              :
              null}
            </div>
            
          </CardContent>
        </Card>
        <Card className={classes.inCard}>
          <CardContent>
          <div className={classes.export}>
          <Typography variant="h5" >
              Bill History
            </Typography>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`payment_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="payment_list">
                <ExcelColumn label="Transaction Date" value="created_date"/>
                <ExcelColumn label="Amount" value="amount"/>
              </ExcelSheet>
      </ExcelFile>
    </div>
      <div className={classes.content}>
        <BillTable bills={bills} handleSuccess={handleSuccess} handleError={handleError} handleError401={handleError401} getData={getData} loading={loading} onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages}/>
      </div>
          </CardContent>
        </Card>
      
      <Dialog
        onClose={handleFlashClose}
        open={openFlash}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
            <Typography variant="h2" style={{textAlign: "center"}}>
              Success!
            </Typography>
            
          <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
        </DialogContent>
      </Dialog>

      <Dialog
        onClose={handleFlashErrClose}
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
        <Dialog
              open={openPay}
              onClose={handleClosePay}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row2}>
                  <Typography variant="h4" >
                    Confirmation
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleClosePay}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Confirm payment with amount of <CurrencyFormat value={bill.outstanding_balance} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => value} /> ?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClosePay} >
                  Cancel
                </Button>
                <Button onClick={handlePay} color="primary" autoFocus>
                  Pay
                </Button>
              </DialogActions>
            </Dialog>
      
      </CardContent>
      </Card>
    </Grid>
    </div>
  );
};

export default BillList;
