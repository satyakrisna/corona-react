import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Button,FormControl,FormGroup,FormLabel,FormControlLabel, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Tooltip,
  Hidden,
  IconButton, Typography, Switch, MenuItem,Select} from '@material-ui/core';

import { SearchInput } from 'components';
import SearchIcon from '@material-ui/icons/Search';
import PostAddIcon from '@material-ui/icons/PostAdd';
import CloseIcon from '@material-ui/icons/Close';
import request from 'services/request';
import validate from 'validate.js';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  limit_transaction: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  flag_reimburse: {
    presence: { allowEmpty: false, message: 'is required' },
  },
  flag_virtual_card: {
    presence: { allowEmpty: false, message: 'is required' },
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  success: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    float: 'right'
  },
  primary: {
    background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    marginRight: '0'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  successMobile: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    color: '#FFFFFF'
  },
}));

const CategoryToolbar = props => {
  const { getData, handleSuccess, handleError, handleError401} = props;

  
  const classes = useStyles();

  const [openAdd, setOpenAdd] = useState(false);

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const company_id = JSON.parse(localStorage.getItem("data")).company_id

  const initialCategoryState = {
    uuid: null,
    company_id: company_id,
    name: "",
    limit_transaction: 0,
    flag_reimburse: false,
    flag_virtual_card: false
  };

  const [category, setCategory] = useState({
    isValid: false,
    values: initialCategoryState,
    touched: {},
    errors: {}
  });
  const [word, setWord] = useState("");

  useEffect(() => {
    const errors = validate(category.values, schema);

    setCategory(category => ({
      ...category,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [category.values]);

  const handleSave = () => {
    request({
      method: 'POST',
      url: '/api/category',
      data: category.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenAdd(false);
      getData();
      setCategory({
        isValid: false,
        values: initialCategoryState,
        touched: {},
        errors: {}
      });
      handleSuccess('Data successfully added');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleInputChange = event => {
    event.persist();

    setCategory(category => ({
      ...category,
      values: {
        ...category.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...category.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleClickOpenAdd = () => {
    setOpenAdd(true);
  };

  const handleTypeChange = event => {
    props.onTypeChange(event.target.value);
  };

  const handleCloseAdd = () => {
    setOpenAdd(false);
    setCategory({
      isValid: false,
      values: initialCategoryState,
      touched: {},
      errors: {}
    })
  };

  const handleSearchChange = (event) => {
    setWord(event.target.value);
  };

  const handleSearch = () => {
    props.onKeywordChange(word);
    props.onPageChange(0);
  };

  const hasError = field =>
  category.touched[field] && category.errors[field] ? true : false;


  return (
    <div
      className={classes.root}
    >

      <Hidden mdUp>
      <FormControl variant="outlined" style={{width: '100%', marginTop: '5%', marginBottom: '3%'}} className={classes.searchInput}>
          <Select
            labelId="demo-simple-select-outlined-label"
            margin="dense"
            id="demo-simple-select-outlined"
            name="type"
            value={props.type}
            onChange={handleTypeChange}
          >
            <MenuItem value={'all'}>All</MenuItem>
            <MenuItem value={'reimburse'}>Reimburse</MenuItem>
            <MenuItem value={'virtualcard'}>Virtual Card</MenuItem>
          </Select>
        </FormControl>
        </Hidden>

      <div className={classes.row}>
        <Hidden mdDown>
        <FormControl variant="outlined" style={{marginRight: '2%'}} className={classes.searchInput}>
          <Select
            labelId="demo-simple-select-outlined-label"
            margin="dense"
            id="demo-simple-select-outlined"
            name="type"
            value={props.type}
            onChange={handleTypeChange}
          >
            <MenuItem value={'all'}>All</MenuItem>
            <MenuItem value={'reimburse'}>Reimburse</MenuItem>
            <MenuItem value={'virtualcard'}>Virtual Card</MenuItem>
          </Select>
        </FormControl>
        </Hidden>


        <SearchInput
          className={classes.searchInput}
          placeholder="Search category name"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>
        
        <Hidden mdDown>
        <span className={classes.spacer} />
        <Tooltip title="Add Category" placement="top">
          <Button
            onClick={handleClickOpenAdd}
            className={classes.success}
            variant="contained"
          >
            <PostAddIcon/>
          </Button>
        </Tooltip>
        </Hidden>

      </div>

      <Hidden mdUp>
      <span className={classes.spacer} />
          <Button
            fullWidth
            onClick={handleClickOpenAdd}
            className={classes.successMobile}
            variant="contained"
            startIcon={<PostAddIcon/>}
          >
            Add New Category
          </Button>
      </Hidden>
      <Dialog
              open={openAdd}
              onClose={handleCloseAdd}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Add Category
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton aria-label="close" onClick={handleCloseAdd}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
                <DialogContent>
                  <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('name')}
                    helperText={
                      hasError('name') ? category.errors.name[0] : null
                    }
                    name="name"
                    label="Category Name"
                    type="text"
                    onChange={handleInputChange} 
                    value={category.values.name}
                    fullWidth
                  />
                  <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('limit_transaction')}
                    helperText={
                      hasError('limit_transaction') ? category.errors.limit_transaction[0] : null
                    }
                    name="limit_transaction"
                    label="Limit"
                    type="number"
                    onChange={handleInputChange} 
                    value={category.values.limit_transaction}
                    fullWidth
                  />
                  <FormControl component="fieldset" margin="dense">
                    <FormLabel component="legend">Type</FormLabel>
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Switch
                            onChange={handleInputChange}
                            color="primary"
                            value={category.values.flag_reimburse}
                            name="flag_reimburse"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          />
                        }
                        label="Reimburse"
                      />
                      <FormControlLabel
                        control={
                          <Switch
                            onChange={handleInputChange}
                            color="primary"
                            value={category.values.flag_virtual_card}
                            name="flag_virtual_card"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          />
                        }
                        label="Virtual Card"
                      />
                    </FormGroup>
                  </FormControl>
                  
                </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseAdd} color="secondary">
                  Cancel
                </Button>
                <Button onClick={handleSave} color="primary" autoFocus disabled={!category.isValid}>
                  Save
                </Button>
              </DialogActions>
            </Dialog>
    </div>
  );
};

CategoryToolbar.propTypes = {
  className: PropTypes.string
};

export default CategoryToolbar;
