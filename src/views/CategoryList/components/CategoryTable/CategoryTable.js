import React, { useState, Fragment, useEffect} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Hidden,
  TextField,
  Tooltip,
  IconButton,
  Typography,
  FormControl,FormGroup,FormLabel,FormControlLabel, Switch,
  Select,
  MenuItem
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import CurrencyFormat from 'react-currency-format';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import request from 'services/request';
import validate from 'validate.js';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  limit_transaction: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  flag_reimburse: {
    presence: { allowEmpty: false, message: 'is required' },
  },
  flag_virtual_card: {
    presence: { allowEmpty: false, message: 'is required' },
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  status: {
    marginRight: theme.spacing(1)
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  loading: {
    maxHeight: 75,
    marginLeft:450,
    marginTop:10,
    marginBottom:10,
  },
  pagination:{
    marginLeft: theme.spacing(3)
  }
}));

const CategoryTable = props => {
  const {getData, categories, loading, handleSuccess, handleError, handleError401, size, page, count, pages} = props;

  const classes = useStyles();

  
  const [openEdit, setOpenEdit] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [category, setCategory] = useState({});

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleOnChange = (event) => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  const handleClickOpenEdit = (id) => {
   
    request.get(`/api/category/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.category
        setFormState({
          isValid: false,
          values: data,
          touched: {},
          errors: {}
        })
         setOpenEdit(true);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleUpdate = () => {
    console.log(category)
    request({
      method: 'PUT',
      url: '/api/category',
      data: formState.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenEdit(false);
      getData();
      handleSuccess('Data successfully updated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const handleClickOpenDelete = (id) => {
    setOpenDelete(true);
    setCategory({
      'uuid': id,
      'is_deleted': true
    })
  };

  const handleDelete = () => {
    request({
      method: 'PUT',
      url: '/api/category',
      data: category,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenDelete(false);
      getData();
      handleSuccess('Data successfully deleted');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Limit</TableCell>
                  <TableCell>Created Date</TableCell>
                  <TableCell>Last Updated</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
                <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                </TableBody>
              : 
              <TableBody>
                {categories.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='5' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                categories.slice(0, size).map(category => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={category.uuid}
                  >
                    <TableCell style={{width: '17%'}}>
                      <Tooltip title={`Edit ${category.name}`} placement="top">
                        <IconButton onClick={() => { handleClickOpenEdit(category.uuid)}}><CreateIcon/></IconButton>
                      </Tooltip>
                      <Tooltip title={`Edit ${category.name}`} placement="top">
                        <IconButton onClick={() => { handleClickOpenDelete(category.uuid)}}><DeleteIcon/></IconButton>
                      </Tooltip>
                    </TableCell>
                    <TableCell>{category.name}</TableCell>
                    <TableCell><CurrencyFormat value={category.limit_transaction} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                    <TableCell>
                      {moment(category.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell>
                      {moment(category.modify_date).format('DD/MM/YYYY')}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
              }
            </Table>
            <Dialog
              open={openEdit}
              onClose={handleCloseEdit}
              fullWidth
              maxWidth="sm"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Edit Category
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseEdit}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('name')}
                      helperText={
                        hasError('name') ? formState.errors.name[0] : null
                      }
                    name="name"
                    label="Category Name"
                    type="text"
                    onChange={handleOnChange} 
                    value={formState.values.name}
                    fullWidth
                  />
                  <TextField
                    autoFocus
                    margin="dense"
                    error={hasError('limit_transaction')}
                      helperText={
                        hasError('limit_transaction') ? formState.errors.limit_transaction[0] : null
                      }
                    name="limit_transaction"
                    label="Limit"
                    type="number"
                    onChange={handleOnChange} 
                    value={formState.values.limit_transaction}
                    fullWidth
                  />
                  <FormControl component="fieldset" margin="dense">
                    <FormLabel component="legend">Type</FormLabel>
                    <FormGroup>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={formState.values.flag_reimburse}
                            onChange={handleOnChange}
                            color="primary"
                            name="flag_reimburse"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          />
                        }
                        label="Reimburse"
                      />
                      <FormControlLabel
                        control={
                          <Switch
                            checked={formState.values.flag_virtual_card}
                            onChange={handleOnChange}
                            color="primary"
                            name="flag_virtual_card"
                            inputProps={{ 'aria-label': 'primary checkbox' }}
                          />
                        }
                        label="Virtual Card"
                      />
                    </FormGroup>
                  </FormControl>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseEdit} color="secondary">
                  Cancel
                </Button>
                {/* <Button onClick={handleUpdate} color="primary" autoFocus disabled={!formState.isValid}> */}
                <Button onClick={handleUpdate} color="primary" autoFocus>
                  Save
                </Button>
              </DialogActions>
            </Dialog>
            <Dialog
              open={openDelete}
              onClose={handleCloseDelete}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Confirmation
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDelete}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Are you sure want to delete this category?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDelete}>
                  Cancel
                </Button>
                <Button onClick={handleDelete} color="secondary" autoFocus>
                  Delete
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Hidden mdDown>
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          </Hidden>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
    </Fragment>
  );
};

CategoryTable.propTypes = {
  className: PropTypes.string,
  categories: PropTypes.array.isRequired
};

export default CategoryTable;
