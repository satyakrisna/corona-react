import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { CategoryTable, CategoryToolbar } from './components';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography, CardContent,
  Card, Box, Grid, Tooltip, IconButton
} from '@material-ui/core';
import request from 'services/request'
import CategoryIcon from '@material-ui/icons/Category';
import moment from 'moment';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";
import auth from 'services/auth'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    // backgroundImage: `url("/images/danger.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2)
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  }
}));

const CategoryList = () => {
  const classes = useStyles();
  const history = useHistory();

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const [categories, setCategories] = useState([]);
  const [exportData, setExportData] = useState([]);
  const [type, setType] = useState('all');
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [keyword, setKeyword]=useState("");
  var generated_date = moment(new Date()).format('DD/MM/YYYY');

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleChange = newType => {
    setType(newType);
  };

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401 = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;

  
  const getData = () => {
    request.get(`/api/category?type=${type}&size=${size}&page=${page}&search=${keyword}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
       const data = res.data.output_schema.categories
       setCategories(data.content);
       setCount(data.total_elements);
       setPages(data.total_pages);
       setLoading(false);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

      request.get(`/api/category?type=${type}&size=99999&page=0&search=${keyword}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.categories
        setExportData(data.content);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  }

  useEffect(()=>{
    document.title = "Corona | Category List"
    getData()
    setLoading(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[type, size, page, keyword])

  const keywordChange = (word) => {
    setKeyword(word)
  }

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "portrait";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    var today = moment(new Date()).format('DD/MM/YYYY');
    const title = "Category List";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Category Name", "Limit", "Flag Reimburse", "Flag Virtual Card"]];
    const date = `Generated on ${today}`;

    const data = exportData.map(data=> [data.name, data.limit_transaction, data.flag_reimburse, data.flag_virtual_card]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };

    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 495, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`category_list_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
      <Grid> 
      <div className={classes.package}>
          <Box boxShadow={5} className={classes.row}>
            <Typography variant="h2" className={classes.iconColor}>
                <CategoryIcon className={classes.icon}/>
              </Typography>
          </Box>
            <Typography variant="h4" className={classes.title} >
              Categories
            </Typography>
        </div>     
      <Card style={{paddingTop: '45px'}}>
        <CardContent>
        <div className={classes.export}>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`category_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="category_list">
                <ExcelColumn label="Category Name" value="name"/>
                <ExcelColumn label="Limit" value="limit_transaction"/>
                <ExcelColumn label="Flag Reimburse" value="flag_reimburse"/>
                <ExcelColumn label="Flag Virtual Card" value="flag_virtual_card"/>
              </ExcelSheet>
      </ExcelFile>
    </div>
      <CategoryToolbar type={type} onTypeChange={handleChange} handleSuccess={handleSuccess} handleError={handleError} getData={getData} onKeywordChange={keywordChange} onLoadingChange={setLoading} onPageChange={setPage}/>
      <div className={classes.content}>
        <CategoryTable categories={categories} handleSuccess={handleSuccess} handleError={handleError} getData={getData} loading={loading} onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages}/>
      </div>
      <Dialog
      onClose={handleFlashClose}
        open={openFlash}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
          <Typography variant="h2" style={{textAlign:'center'}}>
              Success!
            </Typography>
          <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
        </DialogContent>
      </Dialog>
      </CardContent>

      <Dialog
      onClose={handleFlashErrClose}
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
      </Card>
    </Grid>
    </div>
  );
};

export default CategoryList;
