import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    name: 'Makan',
    limit: '50000',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000,
  },
  {
    id: uuid(),
    name: 'Obat',
    limit: '200000',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000,
  },{
    id: uuid(),
    name: 'Kaca Mata',
    limit: '2000000',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000,
  },{
    id: uuid(),
    name: 'Transport',
    limit: '100000',
    createdDate: 1555016400000,
    lastUpdated: 1555016400000,
  },
];
