import React, { useState, useEffect, forwardRef } from 'react';
import { makeStyles } from '@material-ui/styles';
import notification from 'services/notification'
import {
  CardContent, Typography, Card, Box, ButtonBase, Dialog, DialogContent, DialogContentText

} from '@material-ui/core';
import { NavLink as RouterLink } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import TimeAgo from 'react-timeago'
import NotificationsIcon from '@material-ui/icons/Notifications';
import auth from 'services/auth'



const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3),
      },
      content: {
        marginTop: theme.spacing(2)
      },
      flash: {
        height : 200
      },
      row: {
        display: 'flex',
        alignItems: 'center',
        marginLeft: theme.spacing(2),
        zIndex: "100",
        backgroundImage: `url("/images/danger.png")`,
        // background: "linear-gradient(25deg, rgba(238,81,78,1) 0%, rgba(232,65,61,1) 35%)",
        backgroundSize: "cover",
        backgroundPosition: "center center",
        borderRadius: '5px',
        padding: '12px',
        width: 57
      },
      rows: {
        marginLeft: theme.spacing(3),
        height: '42px',
        display: 'flex',
        alignItems: 'center',
        marginTop: theme.spacing(3),
        marginRight: theme.spacing(3)
      },
      iconColor: {
        color: '#FFFFFF'
      },
      export: {
        height: '42px',
        display: 'flex',
        alignItems: 'center',
      },
      spacer: {
        flexGrow: 1
      },
      title: {
        color: '#1A2038',
        paddingLeft: '16px',
        marginTop: "auto",
      },
      icon:{
        marginLeft:5, 
        marginRight:10
      },
      package: {
        display: "flex",
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(-4.8),
        zIndex: "100"
      },
      exportIcons: {
        maxWidth: 30,
        margin: '5%'
      },
      hover: {
        '&:hover': {
          backgroundColor: '#FBFCFD'
        }
      },
}));

const Notifications = props => {
    const classes = useStyles();
    const history = useHistory();

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const user_uuid = JSON.parse(localStorage.getItem("data")).user_uuid
  const [notifications, setNotifications] = useState([]);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const getData = () => {
    notification.get(`/api/notification/${user_uuid}?admin=1`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
        const data = res.data.output_schema.notification
        setNotifications(data.content);
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleError401("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleError401(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleError401("Something went wrong")
        }
       })
  }

  useEffect(()=>{
    document.title = "Corona | Notifications"
    getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  const buttonBase = notif => {
    let url, title;
    title = notif.title.split(";")

    if(title[1] === "VCREQ") {
      url = `/admin/virtual-card-request-ongoing?${title[2]}`
    } else if(title[1] === "REIMBURSEREQ") {
      url = `/admin/reimburses-ongoing?${title[2]}`
    } else if(title[1] === "TRXQR") {
      url = `/admin/transactions?${title[2]}`
    } else if(title[1] === "REPORTVC") {
      url = `/admin/virtual-card?${title[2]}`
    }

    return (

      <ButtonBase 
        component={CustomRouterLink}
        to={`${url}`} 
        key={notif.uuid}
        className={classes.hover}>
          <div className={classes.notifications}>
          <div className={classes.rows}>
            <img src={'/images/logos/logo.png'} alt='logo' style={{ width: 40, marginRight: 15 }}/>
              <Typography>{notif.message}</Typography>
          </div>
          <div className={classes.rows} style={{marginTop: 5}}>
                <span className={classes.spacer} />
                <AccessTimeIcon style={{ fontSize: 12, marginRight: 5 }}/>
                <Typography variant="caption" style={{alignText:'right'}}><TimeAgo date={notif.created_date} /></Typography>
              </div>
        </div>
      </ButtonBase>
    )
  }

  const CustomRouterLink = forwardRef((props, ref) => (
    <div
      ref={ref}
      style={{ flexGrow: 1 }}
    >
      <RouterLink {...props} />
    </div>
  ));

  return (
    <div className={classes.root}>
        <div className={classes.package}>
      <Box boxShadow={5} className={classes.row}>
        <Typography variant="h2" className={classes.iconColor}>
            <NotificationsIcon className={classes.icon}/>
          </Typography>
      </Box>
        <Typography variant="h4" className={classes.title} >
          Notifications
        </Typography>
    </div>  
        <Card style={{paddingTop: '45px'}}>
            <CardContent>
            {notifications.map(notif=>(buttonBase(notif)))}
            <Dialog
            onClose={handleFlashErrClose}
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
            </CardContent>
        </Card>

    </div>
  );
};

export default Notifications;