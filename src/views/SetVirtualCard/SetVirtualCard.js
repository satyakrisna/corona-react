import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import moment from 'moment';
import CurrencyTextField from '@unicef/material-ui-currency-textfield'
import { makeStyles } from '@material-ui/styles';
import 'react-credit-cards/es/styles-compiled.css';
import Cards from 'react-credit-cards';
import transaction from 'services/transaction'
import request from 'services/request'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography,
  CardContent,
  Card,
  Grid,
  Box,
  TextField,
  Button
} from '@material-ui/core'

import CreditCardIcon from '@material-ui/icons/CreditCard';
import auth from 'services/auth'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    backgroundImage: `url("/images/danger.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  gridContainer: {
      marginTop: theme.spacing(4),
      marginBottom: theme.spacing(3)
  },
  containerSplitKiri: {
    marginBottom: '20px',
    [theme.breakpoints.up('md')]: {
        paddingRight: theme.spacing(1),
      }
  },
  containerSplitKanan: {
    marginBottom: '20px',
    [theme.breakpoints.up('md')]: {
        paddingLeft: theme.spacing(1),
      }
  },
  buttonRelease: {
      float: "right",
      backgroundImage: `url("/images/success.png")`,
      backgroundSize: "cover",
      backgroundPosition: "center center",
      color: "#FFFFFF",
      [theme.breakpoints.down('md')]: {
        width: "100%"
      }
  }
}));

const SetVirtualCard = props => {
  const location = useLocation();
  const history = useHistory();
  const classes = useStyles();

  const [virtualCardData, setVirtualCardData] = useState({
    user_uuid: '',
    invoice_request: '',
    limit_transaction: '',
    start_date: '',
    end_date: ''
  });
  const [name, setName] = useState('');
  const [message, setMessage]= useState('');
  const [openFlash, setOpenFlash]= useState(false);
  const [imgAge, setImgAge]=useState(Date.now);
  const [openFlashErr, setOpenFlashErr]= useState(false);

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  useEffect(()=>{
    if (location.state === undefined) {
      history.push("/not-found")
    } else {
      setName(location.state.name)
      setVirtualCardData({
        user_uuid: location.state.user_uuid,
        invoice_request: location.state.invoice_request,
        limit_transaction: location.state.limit_transaction,
        start_date: moment(location.state.start_date).toISOString().substr(0,10),
        end_date: moment(location.state.end_date).toISOString().substr(0,10)
      })
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
        history.push("/admin/virtual-card")
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleErrorLogout = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const handleChange = (event) => {
    const { name, value } = event.target;
    setVirtualCardData({ ...virtualCardData, [name]: value });
  }

  useEffect(()=>{
    console.log(virtualCardData)
  },[virtualCardData])

  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(virtualCardData)
    transaction({
      method: 'POST',
      url: '/api/admin/virtual-card',
      data: virtualCardData,
      headers: {
        'Authorization': `Bearer ${access_token}`, 
      }
    }).then(res=>{
      handleDone();
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  const handleDone = () => {
    request({
      method: 'PUT',
      url: '/api/vcr',
      data: {
        "uuid": location.state.request_uuid,
        "accepted": true,
        "note": ""
      },
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
    }).then(res =>{
      handleSuccess('Card successfully released');
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  var curr = new Date();
  var date = curr.toISOString().substr(0,10);

  useEffect(()=>{
  },[virtualCardData])

  return (
    <div className={classes.root}>
    <Grid>
      <div className={classes.package}>
        <Box boxShadow={5} className={classes.row}>
          <Typography variant="h2" className={classes.iconColor}>
            <CreditCardIcon className={classes.icon}/>
          </Typography>
        </Box>
          <Typography variant="h4" className={classes.title}>
          Set Virtual Card
        </Typography>
      </div>  
      <Card style={{paddingTop: '45px'}}>
        <CardContent>
          <form onSubmit={handleSubmit}>
            <Grid container className={classes.gridContainer}>
                <Grid item
                    style={{marginBottom: "60px"}}
                    xl={4}
                    lg={4}
                    md={12}
                    xs={12}
                >
                    <Cards
                        number="**** **** **** ****"
                        name={name}
                        expiry={moment(virtualCardData.end_date).format('MM/YY')}
                        focused="number"
                        cvc="000"
                        preview={true}
                        issuer="visa"
                    />
                </Grid>
                <Grid item
                    xl={1}
                    lg={1}
                >
                </Grid>
                <Grid item
                    xl={6}
                    lg={6}
                    md={12}
                    xs={12}
                >
                    <Grid item
                        xl={12}
                        lg={12}
                        md={12}
                        xs={12}
                    >
                        <TextField
                            style={{marginBottom: '20px'}}
                            id="outlined"
                            label="Name"
                            value={name}
                            variant="outlined"
                            fullWidth
                        />
                    </Grid>

                    <Grid item
                        xl={12}
                        lg={12}
                        md={12}
                        xs={12}
                    >

                      <CurrencyTextField
                        style={{marginBottom: '20px'}}
                        label="Amount"
                        value={virtualCardData.limit_transaction}
                        currencySymbol="Rp"
                        minimumValue="0"
                        outputFormat="string"
                        decimalCharacter="."
                        digitGroupSeparator=","
                        onChange={(event, value)=> setVirtualCardData({
                          ...virtualCardData, 'limit_transaction': value
                        })}
                        fullWidth
                        variant="outlined"
                        textAlign="left"
                      />
                    </Grid>

                    <Grid container>
                        <Grid item
                            className={classes.containerSplitKiri}
                            xl={6}
                            lg={6}
                            md={12}
                            xs={12}
                        >
                            <TextField
                            id="outlined"
                            label="Start Date"
                            type="date"
                            name="start_date"
                            defaultValue={date}
                            value={virtualCardData.start_date}
                            // value={new Date(virtualCardData.start_date).toISOString()}
                            // value={date}
                            variant="outlined"
                            fullWidth
                            onChange={handleChange}
                        />
                        </Grid>
                        <Grid item
                            className={classes.containerSplitKanan}
                            xl={6}
                            lg={6}
                            md={12}
                            xs={12}
                        >
                            <TextField
                            id="outlined"
                            label="End Date"
                            type="date"
                            name="end_date"
                            defaultValue={date}
                            value={virtualCardData.end_date}
                            variant="outlined"
                            onChange={handleChange}
                            fullWidth
                        />
                        </Grid>
                    </Grid>
                    <Button type="submit" className={classes.buttonRelease}>Release</Button>
                </Grid>
            </Grid>
          </form>        
        </CardContent>
      </Card>
    </Grid>

    <Dialog
      onClose={handleFlashClose}
      open={openFlash}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogContent>
        <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
          <Typography variant="h2" style={{textAlign:'center'}}>
            Success!
          </Typography>
          
        <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
          {message}
        </DialogContentText>
      </DialogContent>
    </Dialog>

    <Dialog
    onClose={handleFlashErrClose}
    open={openFlashErr}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    >
    <DialogContent>
        <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
        <Typography variant="h2" style={{textAlign:'center'}}>
            Error!
        </Typography>
        
        <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
        {message}
        </DialogContentText>
    </DialogContent>
  </Dialog>

    </div>
  );
};

export default SetVirtualCard;
