import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  IconButton,
  Typography,
  Select,
  MenuItem
} from '@material-ui/core';
import Skeleton from 'react-loading-skeleton';
import CurrencyFormat from 'react-currency-format';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  status: {
    marginRight: theme.spacing(1)
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  loading: {
    maxHeight: 75,
    marginLeft:450,
    marginTop:10,
    marginBottom:10,
  },
  pagination:{
    marginLeft: theme.spacing(3)
  }
}));

const PaymentTable = props => {
  const { payments, loading, size, page, count, pages} = props;

  const classes = useStyles();

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Transaction Date</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Amount</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
                <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                </TableBody>
              : 
              <TableBody>
                {payments.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='3' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                payments.slice(0, size).map(payment => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={payment.uuid}
                  >
                    <TableCell>
                      {moment(payment.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell>{payment.type==="reimburse"? "Reimburse" : "Virtual Card"}</TableCell>
                    <TableCell><CurrencyFormat value={payment.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                  </TableRow>
                ))}
              </TableBody>
              
            }
            </Table>
            
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
  </Fragment>
  );
};

PaymentTable.propTypes = {
  className: PropTypes.string,
  payments: PropTypes.array.isRequired
};

export default PaymentTable;
