import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { Button, Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
  Tooltip,
  IconButton, Typography, Card, CardContent, FormControl, Select, MenuItem, Hidden} from '@material-ui/core';

import { SearchInput } from 'components';
import moment from 'moment';
import SearchIcon from '@material-ui/icons/Search';
import FilterListIcon from '@material-ui/icons/FilterList';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  success: {
    backgroundImage: `url("/images/success.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5)
  },
  primary: {
    background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    marginRight: '0'
  },
  default: {
    background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    margin: theme.spacing(0.5),
  },
  table: {
    minWidth: 200,
    backgroundColor: '#F4F6F8'
  },
  buttonFilterMobile: {
    background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const PaymentToolbar = props => {

  const [word, setWord] = useState("");
  const [openFilter, setOpenFilter] = useState(false);
  var date = new Date();
  var today = date.toISOString().substr(0,10);
  var lastMonth = moment().subtract(1, 'months').toISOString().substr(0,10);
  const [filter, setFilter] = useState({
    start: lastMonth,
    end: today,
    orderBy: '',
    direction: 'desc'
  });

  const handleSearchChange = (event) => {
    setWord(event.target.value);
  };

  const handleSearch = () => {
    props.onKeywordChange(word);
    props.onPageChange(0);
  };

  const handleTypeChange = event => {
    props.onTypeChange(event.target.value);
  };

  const handleFilterChange = event => {
    const { name, value } = event.target;
    setFilter({ ...filter, [name]: value });
  };

  const handleClickOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleCloseFilter = () => {
    setOpenFilter(false);
  };

  const handleFilter = () => {
    setOpenFilter(false);
    props.onFilterChange(filter.start, filter.end, filter.orderBy, filter.direction);
    props.onPageChange(0);
  };

  const handleClearFilter = () => {
    setOpenFilter(false);
    props.onFilterChange(lastMonth,today,'modify_date','desc');
    props.onPageChange(0);
  };

  const classes = useStyles();

  return (
    <div
      className={classes.root}
    >
      <Hidden mdUp>
      <FormControl variant="outlined" style={{width: '100%', marginTop: '5%', marginBottom: '3%'}} className={classes.searchInput}>
          <Select
            labelId="demo-simple-select-outlined-label"
            margin="dense"
            id="demo-simple-select-outlined"
            name="type"
            value={props.type}
            onChange={handleTypeChange}
          >
            <MenuItem value={'all'}>All</MenuItem>
            <MenuItem value={'reimburse'}>Reimburse</MenuItem>
            <MenuItem value={'virtual_card'}>Virtual Card</MenuItem>
          </Select>
        </FormControl>
      </Hidden>
      <div className={classes.row}>
      <Hidden mdDown>
      <FormControl variant="outlined" style={{marginRight: '2%'}} className={classes.searchInput}>
          <Select
            labelId="demo-simple-select-outlined-label"
            margin="dense"
            id="demo-simple-select-outlined"
            name="type"
            value={props.type}
            onChange={handleTypeChange}
          >
            <MenuItem value={'all'}>All</MenuItem>
            <MenuItem value={'reimburse'}>Reimburse</MenuItem>
            <MenuItem value={'virtual_card'}>Virtual Card</MenuItem>
          </Select>
        </FormControl>
      </Hidden>
        <SearchInput
          className={classes.searchInput}
          placeholder="Search payment"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>
        <span className={classes.spacer} />
        <Hidden mdDown>
        <Tooltip title="Filter & Sort" placement="top">
          <Button
            onClick={handleClickOpenFilter}
            className={classes.default}
            variant="contained"
          >
            <FilterListIcon/>
          </Button>
        </Tooltip>
      </Hidden>
      </div>

      <Hidden mdUp>
      <Button
            onClick={handleClickOpenFilter}
            className={classes.buttonFilterMobile}
            variant="contained"
            startIcon={<FilterListIcon />}
            fullWidth
          >
            Filter & Sort
          </Button>
      </Hidden>
      <Dialog
              open={openFilter}
              onClose={handleCloseFilter}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="sm"
              fullWidth
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Filter & Sort
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseFilter}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <Card>
                  <CardContent>
                    <Typography variant="h5">Filter</Typography>
                    <div className={classes.row}>
                    <TextField
                                label="From"
                                type="date"
                                name="start"
                                fullWidth
                                value={filter.start}
                                onChange={handleFilterChange}
                            />
                      <span className={classes.spacer} />
                      <TextField
                                  label="To"
                                  type="date"
                                  name="end"
                                  fullWidth
                                  value={filter.end}
                                  onChange={handleFilterChange}
                              />
                    </div>
                  </CardContent>
                </Card>
                <Card>
                  <CardContent>
                    <Typography variant="h5">Sort</Typography>
                    <div className={classes.row}>
                      <TextField
                          select
                          margin="dense"
                          label="Order by"
                          name="orderBy"
                          onChange={handleFilterChange}
                          SelectProps={{
                            native: true,
                          }}
                          fullWidth
                          value={filter.orderBy}
                        >
                          <option value=""></option>
                          <option value="amount">Amount</option>
                          <option value="modify_date">Modify Date</option>
                      </TextField>
                      <TextField
                          select
                          margin="dense"
                          label="Direction"
                          name="direction"
                          onChange={handleFilterChange}
                          SelectProps={{
                            native: true,
                          }}
                          fullWidth
                          value={filter.direction}
                        >
                          <option value="asc">ASC</option>
                          <option value="desc">DESC</option>
                      </TextField>
                    </div>
                  </CardContent>
                </Card>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClearFilter} color="secondary" >
                  Clear Filter
                </Button>
                <Button onClick={handleFilter} color="primary" autoFocus>
                  Filter
                </Button>
              </DialogActions>
            </Dialog>
    </div>
  );
};

PaymentToolbar.propTypes = {
  className: PropTypes.string
};

export default PaymentToolbar;
