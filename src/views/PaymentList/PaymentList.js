import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';

import { PaymentToolbar, PaymentTable } from './components';
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography,
  CardContent,
  Card, Box, Grid, Tooltip, IconButton
} from '@material-ui/core';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';
import payment from 'services/payment'
import moment from 'moment';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";
import auth from 'services/auth'

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    loopCount: 0
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    // backgroundImage: `url("/images/danger.png")`,
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  },
  inCard: {
    marginTop: theme.spacing(2)
  },
  row2: {
    display: 'flex',
    alignItems: 'center',
    width: '100%'
  }
}));

const PaymentList = () => {
  const history = useHistory();
  const classes = useStyles();

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const [payments, setPayments] = useState([]);
  const [exportData, setExportData] = useState([]);
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [keyword, setKeyword]=useState("");
  const [type, setType]=useState('all');
  var generated_date = moment(new Date()).format('DD/MM/YYYY');
  var date = new Date();
  var today = date.toISOString().substr(0,10);
  var lastMonth = moment().subtract(1, 'months').toISOString().substr(0,10);
  const [start, setStart]=useState(lastMonth);
  const [end, setEnd]=useState(today);
  const [orderBy, setOrderBy]=useState('modify_date');
  const [direction, setDirection]=useState('desc');

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleError401= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;

  const getData = () => {
    payment.get(`/api/payment?type=${type}&size=${size}&page=${page}&search=${keyword}&start=${start}&end=${end}&order_by=${orderBy}&direction=${direction}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.payments
        setPayments(data.content);
        setCount(data.total_elements);
        setPages(data.total_pages);
        setLoading(false);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })

     payment.get(`/api/payment?type=${type}&size=99999&page=0&search=${keyword}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const data = res.data.output_schema.payments
        setExportData(data.content);
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  }

  useEffect(()=>{
    document.title = "Corona | Payment"
    getData()
    setLoading(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[type, size,page,keyword,start,end,orderBy,direction])

  const keywordChange = (word) => {
    setKeyword(word)
  }

  const filterChange = ( start, end, orderBy, direction) => {
    setStart(start);
    setEnd(end);
    setOrderBy(orderBy);
    setDirection(direction);
  }

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "portrait";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    var today = moment(new Date()).format('DD/MM/YYYY');
    const title = "Transaction History";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Transaction Date", "Type", "Amount"]];
    const date = `Generated on ${today}`;

    const data = exportData.map(data=> [moment(data.created_date).format('DD/MM/YYYY'), data.type, data.amount]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };

    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 495, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`transaction_history_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
      <Grid>    
      <div className={classes.package}>
          <Box boxShadow={5} className={classes.row}>
            <Typography variant="h2" className={classes.iconColor}>
                <AccountBalanceWalletIcon className={classes.icon}/>
              </Typography>
          </Box>
            <Typography variant="h4" className={classes.title} >
              Payments
            </Typography>
        </div>  
      <Card style={{paddingTop: '45px'}}>
        <CardContent>
          <div className={classes.export}>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`payment_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="payment_list">
                <ExcelColumn label="Transaction Date" value="created_date"/>
                <ExcelColumn label="Type" value="type"/>
                <ExcelColumn label="Amount" value="amount"/>
              </ExcelSheet>
      </ExcelFile>
    </div>
          <PaymentToolbar handleSuccess={handleSuccess} handleError={handleError} handleError401={handleError401} getData={getData} onKeywordChange={keywordChange} onTypeChange={setType} onFilterChange={filterChange} onLoadingChange={setLoading}  onPageChange={setPage} type={type}/>
      <div className={classes.content}>
        <PaymentTable payments={payments} handleSuccess={handleSuccess} handleError={handleError} handleError401={handleError401} getData={getData} loading={loading} onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages}/>
      </div>
      
      <Dialog
        onClose={handleFlashClose}
        open={openFlash}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
            <Typography variant="h2" style={{textAlign: "center"}}>
              Success!
            </Typography>
            
          <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
        </DialogContent>
      </Dialog>

      <Dialog
        onClose={handleFlashErrClose}
        open={openFlashErr}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>
            <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
            <Typography variant="h2" style={{textAlign:'center'}}>
                Error!
            </Typography>
            
            <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
            </DialogContentText>
        </DialogContent>
        </Dialog>
      
      </CardContent>
      </Card>
    </Grid>
    </div>
  );
};

export default PaymentList;
