import React, { useState, Fragment, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from '@material-ui/styles';
import {
  Table,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  IconButton,
  Typography,
  Select,
  MenuItem,
  Card,
  CardContent,
} from '@material-ui/core';
import { StatusBullet } from 'components';
import Skeleton from 'react-loading-skeleton';
import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Cards from 'react-credit-cards';
import transactionService from 'services/transaction';
import CurrencyFormat from 'react-currency-format';
import validate from 'validate.js';
import Geocode from "react-geocode";
import GoogleMapReact from 'google-map-react';

const schema = {
  note: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  active: {
    marginTop: theme.spacing(1)
  },
  status: {
    marginRight: theme.spacing(1)
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  actions: {
    justifyContent: 'flex-end'
  },
  table: {
    width: "25%",
    backgroundColor: '#FAFAFA'
  },
  loading: {
    maxHeight: 75,
    marginLeft:450,
    marginTop:10,
    marginBottom:10,
  },
  pagination:{
    marginLeft: theme.spacing(3)
  },
  text: {
    paddingBottom: '1%',
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
      marginTop: theme.spacing(2)
    }
  }, gridMapsKiri: {
    paddingRight: '1%',
    [theme.breakpoints.down('md')]: {
      padding: '0'
    }
  }, gridMapsKanan: {
    paddingLeft: '1%',
    [theme.breakpoints.down('md')]: {
      padding: '0'
    }
  }
}));

const TransactionTable = props => {
  const { getData, transactions, loading, handleSuccess, size, page, count, pages, handleError, handleError401} = props;

  const classes = useStyles();
  const location = useLocation();

  const [openDetail, setOpenDetail] = useState(false);
  const [transaction, setTransaction] = useState({});
  const [user, setUser] = useState({});
  const [virtualcard, setVirtualCard] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {}
  });
  const [active, setActive] = useState();
  const [openDeactivate, setOpenDeactivate] = useState(false);

  useEffect(() => {
    const errors = validate(virtualcard.values, schema);

    setVirtualCard(virtualcard => ({
      ...virtualcard,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [virtualcard.values]);

  const handleOnChange = (event) => {
    event.persist();

    setVirtualCard(virtualcard => ({
      ...virtualcard,
      values: {
        ...virtualcard.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...virtualcard.touched,
        [event.target.name]: true
      }
    }));
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token
  const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

  useEffect(()=>{
    let url = location.search
    let token = url.split("?")

    if(url !== "" && token[1] !== "") {
      transactionService.get(`/api/transaction/${token[1]}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
       }).then(res =>{
         console.log(res.data)
        const data = res.data.output_schema
          setTransaction(data.transaction)
          setUser(data.user)
          setVirtualCard({
            isValid: false,
            values: data.virtual_card,
            touched: {},
            errors: {}
          })
          setActive(data.virtual_card.is_active)
         setOpenDetail(true);
  
          googleMaps(data.transaction)
  
       }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleError401("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleError401(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleError401("Something went wrong")
        }
       })
    }
    location.search = ""
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[location.search])

  const [coorStart, setCoorStart] = useState({})
  const [coorEnd, setCoorEnd] = useState({})
  const [loginLocation, setLoginLocation] = useState();
  const [transactionLocation, setTransactionLocation] = useState();
  const [center, setCenter] = useState({});
  const [zoom, setZoom] = useState();

  // GeoCoding
  Geocode.setApiKey("AIzaSyC914Z_xet-plrGvaCX9fAj2yAtcvXN_sA");
  Geocode.setLanguage("en");
  Geocode.enableDebug();

  const AnyReactComponent = ({ text }) => (
    <Tooltip title={`${text}`} placement="top">
      <img src="/images/marker_2.png" height="50px" style={{marginTop: '-50px'}} alt="loading"/>
    </Tooltip>
  );

  const AnyReactComponentTransaction = ({ text }) => (
    <Tooltip title={`${text}`} placement="top">
      <img src="/images/marker_1.png" height="50px" style={{marginTop: '-50px'}} alt="loading"/>
    </Tooltip>
  );

  const handleClickOpenDetail = (id) => {
    transactionService.get(`/api/transaction/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
      const data = res.data.output_schema
        setTransaction(data.transaction)
        setUser(data.user)
        setVirtualCard({
          isValid: false,
          values: data.virtual_card,
          touched: {},
          errors: {}
        })
        setActive(data.virtual_card.is_active)
       setOpenDetail(true);

        googleMaps(data.transaction)

     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  };

  const googleMaps = maps => {
    let latStart = Number(maps.coordinate_start.split(",")[0])
    let lngStart = Number(maps.coordinate_start.split(",")[1])
    let latEnd = Number(maps.coordinate_end.split(",")[0])
    let lngEnd = Number(maps.coordinate_end.split(",")[1])

    setCoorStart({
      lat: latStart,
      lng: lngStart
    })

    setCoorEnd({
      lat: latEnd,
      lng: lngEnd
    })

    setCenter({
      lat: (latStart + latEnd) / 2,
      lng: (lngStart + lngEnd) / 2
    })

    Geocode.fromLatLng(latStart, lngStart).then(
      response => {
        const address = response.results[0].formatted_address;
        setLoginLocation(address)
      },
      error => {
        console.error(error);
      }
    );

    Geocode.fromLatLng(latEnd, lngEnd).then(
      response => {
        const address = response.results[0].formatted_address;
        setTransactionLocation(address)
      },
      error => {
        console.error(error);
      }
    );

    if (Math.abs(latStart - latEnd) <= 0.001 || Math.abs(lngStart - lngEnd) <=0.001) {
      setZoom(22)
    } else if (Math.abs(latStart - latEnd) <= 1 || Math.abs(lngStart - lngEnd) <= 1) {
      setZoom(14)
    } else if (Math.abs(latStart - latEnd) <= 1.5 || Math.abs(lngStart - lngEnd) <= 1.5) {
      setZoom(7)
    } else if (Math.abs(latStart - latEnd) <= 2 || Math.abs(lngStart - lngEnd) <= 2) {
      setZoom(5)
    } else {
      setZoom(1)
    }
  }

  const handleCloseDetail = () => {
    setOpenDetail(false);
  };

  const handleClickOpenDeactivate = () => {
    setVirtualCard({
        isValid: false,
        values: {
          uuid: virtualcard.values.uuid,
          card_number: virtualcard.values.card_number,
          note: virtualcard.values.note,
          is_active: false
        },
        touched: {},
        errors: {}
      });
      setOpenDeactivate(true);
  };

  const handleClickOpenReport = () => {
      setOpenDeactivate(true);
  };
  
  const handleCloseDeactivate = () => {
      setOpenDeactivate(false);
  };

  const handleDeactivate = () => {
    transactionService({
      method: 'PUT',
      url: '/api/admin/virtual-card',
      data: virtualcard.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenDeactivate(false);
        setOpenDetail(false);
        getData();
        handleSuccess('Virtual card successfully deactivated');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  }

  const handleReport = () => {
    transactionService({
      method: 'POST',
      url: '/api/admin/virtual-card/report',
      data: virtualcard.values,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
      setOpenDeactivate(false);
      setOpenDetail(false);
      getData();
      handleSuccess('Report has been submitted');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleError401("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleError401(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleError401("Something went wrong")
      }
     })
  }

  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  const hasError = field =>
  virtualcard.touched[field] && virtualcard.errors[field] ? true : false;

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell>Card Number</TableCell>
                  <TableCell>Merchant</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Status</TableCell>
                  <TableCell>Transaction Date</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
                <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                </TableBody>
              : 
              <TableBody>
                {transactions.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='7' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                  transactions.slice(0, size).map(transaction => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={transaction.uuid}
                  >
                    <TableCell style={{width: '17%'}}>
                      <Tooltip title={`Detail ${transaction.card_number}`} placement="top">
                        <IconButton onClick={() => { handleClickOpenDetail(transaction.uuid)}}><VisibilityIcon/></IconButton>
                      </Tooltip>
                    </TableCell>
                    <TableCell><CurrencyFormat value={transaction.card_number.slice(0,4) + transaction.card_number.slice(12,16)} format={'#### **** **** ####'} displayType={'text'}  renderText={value => <div>{value}</div>} /></TableCell>
                    <TableCell>{transaction.merchant}</TableCell>
                    <TableCell><CurrencyFormat value={transaction.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                    <TableCell>{transaction.flag_qr ? "QR" : "Non QR"}</TableCell>
                    <TableCell>
                      {transaction.transaction_success ? 
                      (
                        <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={'success'}
                          size="sm"
                        />
                        SUCCESS
                      </div>
                      ) : 
                      (
                        <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={'danger'}
                          size="sm"
                        />
                        FAILED
                      </div>
                      )}
                      </TableCell>
                    <TableCell>
                      {moment(transaction.created_date).format('DD/MM/YYYY')}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            }
            </Table>
            <Dialog
              open={openDetail}
              onClose={handleCloseDetail}
              fullWidth
              maxWidth="md"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
              <div className={classes.row}>
                  <Typography variant="h4" >
                   Detail Transaction
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDetail}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
            <DialogContent>
                    <Card>
                    <CardContent>
                    {virtualcard.values.card_number && user.name ? (
                      <Fragment>
                      <Cards
                      number={virtualcard.values.card_number}
                      name={user.name}
                      expiry={moment(virtualcard.values.end_date).format('MM/YY')}
                      cvc="342"
                      focused="number"
                      preview={true}
                  />
                  <center className={classes.active}>
                  {active ? 
            (
              
              <Typography variant="body1">
              <StatusBullet
                className={classes.status}
                color={'success'}
                size="sm"
              />
              Active</Typography>
            ) : 
            (
              
              <Typography variant="body1">
              <StatusBullet
                className={classes.status}
                color={'danger'}
                size="sm"
              />
              Nonactive</Typography>
            )}
            </center>
            </Fragment>
                    ): null}
                    
                        <div className={classes.dialog}>
                        <Table style={{marginTop: 20}}>
                    <TableHead>
                      <TableRow>
                        <TableCell align="center" colSpan={2}>Transaction Detail</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Merchant</TableCell>
                        <TableCell>{transaction.merchant}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Account Number</TableCell>
                        <TableCell>{transaction.account_number}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Amount</TableCell>
                        <TableCell><CurrencyFormat value={transaction.amount} displayType={'text'} thousandSeparator={true} prefix={'Rp '} renderText={value => <div>{value}</div>} /></TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Type</TableCell>
                        <TableCell>{transaction.flag_qr ? "QR" : "Non QR"}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Status</TableCell>
                        <TableCell>
                      {transaction.transaction_success ? 
                      (
                        <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={'success'}
                          size="sm"
                        />
                        SUCCESS
                      </div>
                      ) : 
                      (
                        <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={'danger'}
                          size="sm"
                        />
                        FAILED
                      </div>
                      )}
                      </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Date</TableCell>
                        <TableCell>
                            {moment(transaction.created_date).format('DD/MM/YYYY')} 
                          </TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                        </div>
                    </CardContent>
                </Card>

              {

                transaction.flag_qr ? 

                <div
                style={{
                  marginTop: '3%',
                  width: '100%',
                  height: '60vh'
                }}>

                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyC914Z_xet-plrGvaCX9fAj2yAtcvXN_sA" }}
                    defaultCenter={center}
                    defaultZoom={zoom}
                >
                    <AnyReactComponent
                        lat={coorStart.lat}
                        lng={coorStart.lng}
                        text={`${loginLocation}`}
                    />

                    <AnyReactComponentTransaction
                        lat={coorEnd.lat}
                        lng={coorEnd.lng}
                        text={`${transactionLocation}`}
                    />

                </GoogleMapReact>
              </div>

                :

                null

              }

                </DialogContent>
                <DialogActions>
                { virtualcard.values.is_active ? rank_level===4 ? (
                      <Button onClick={handleClickOpenDeactivate} color="secondary" autoFocus>
                          Deactivate
                      </Button>
                  ): (
                      <Button onClick={handleClickOpenReport} color="secondary" autoFocus>
                          Report
                      </Button>
                  ):
                  null
                  }
                </DialogActions>
                </Dialog>
                <Dialog
                open={openDeactivate}
                onClose={handleCloseDeactivate}
                fullWidth
                maxWidth="sm"
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
                <DialogTitle id="alert-dialog-title">{"Reason"}</DialogTitle>
                <DialogContent>
                    <Card>
                        <CardContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            error={hasError('note')}
                            helperText={
                              hasError('note') ? virtualcard.errors.note[0] : null
                            }
                            name="note"
                            label="Description"
                            type="text"
                            value={virtualcard.values.note || ''}
                            onChange={handleOnChange}
                            fullWidth
                        />
                        </CardContent>
                    </Card>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDeactivate} color="default">
                    Close
                    </Button>
                    {  rank_level===4 ? (
                        <Button onClick={handleDeactivate} color="secondary" autoFocus disabled={!virtualcard.isValid}>
                            Deactivate
                        </Button>
                    ): (
                        <Button onClick={handleReport} color="secondary" autoFocus disabled={!virtualcard.isValid}>
                            Report
                        </Button>
                    )}
                    
                </DialogActions>
                </Dialog>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
  </Fragment>
  );
};

TransactionTable.propTypes = {
  className: PropTypes.string,
  transactions: PropTypes.array.isRequired
};

export default TransactionTable;
