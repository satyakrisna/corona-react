import React, { useState, useEffect, Fragment, useRef } from 'react';
import {storage} from '../../firebase';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import ReCAPTCHA from "react-google-recaptcha";
import validate from 'validate.js';
import {
  Grid,
  InputAdornment,
  Button,
  TextField,
  Typography,
  Card,
  Dialog,
  DialogContent,
  DialogContentText
} from '@material-ui/core';
import Axios from 'axios';

const schema = {
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 64
    }
  },
  phone: {
    presence: { allowEmpty: false, isNumber: true, message: 'is required' },
    length: {
      maximum: 64
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      maximum: 64
    }
  },
  account_number: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 64
    }
  },
  location: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 256
    }
  },
  logo: {
    presence: { allowEmpty: false }
  }
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: 'url(/images/bg-polos-new.png)',
    backgroundSize: 'auto 100%',
    backgroundPosition: 'right',
    backgroundRepeat: 'no-repeat',
    minHeight: '100%',
    paddingTop: '20%',
    paddingBottom: '5%',
    [theme.breakpoints.down('md')]: {
      background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 73%)',
      paddingLeft: '5%',
      paddingRight: '5%',
      paddingBottom: '20%',
      height: 'auto'
    },
    '& label.Mui-focused': {
      color: '#9FA7D8',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: '#a6a6a6',
      },
      '&:hover fieldset': {
        borderColor: '#B3B3B3',
      },
      '&.Mui-focused fieldset': {
        borderColor: '#9FA7D8',
      },
    },
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  card: {
    padding: '2%',
    paddingTop: '1%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 5%',
    }
  },
  buttonRegister: {
    margin: '0.75%',
    marginTop: '2%',
    width: '98.5%',
    background: 'linear-gradient(90deg, rgba(170,190,223  ,1) 0%, rgba(161,169,216,1) 80%)',
    color: '#FFFFFF',
    // borderRadius: '25px',
    transition: "all ease 0.8s",
    '&:hover' : {
      boxShadow: "inset 1500px 0 0 0 #6f7cc3",
    },
  },
  buttonDisabled: {
    margin: '0.75%',
    marginTop: '2%',
    width: '98.5%',
    background: '#d9d9d9',
    color: '#FFFFFF',
  },
  grid: {
    paddingLeft: '0.8%',
    paddingRight: '0.8%',
    [theme.breakpoints.down('md')]: {
      padding: '0',
    }
  },
  textfieldMargin: {
    [theme.breakpoints.down('md')]: {
      marginBottom: '5%'
    }
  },
  titleRegister: {
    textAlign: 'center',
    marginBottom: '1%',
    // color: '#9FA7D8',
    fontWeight: '300',
    color: '#a6a6a6',
    lineHeight: '1.5',
    [theme.breakpoints.down('md')]: {
      color: '#272a3d',
    }
  },
  upload: {
    position: 'relative',
    backgroundColor: '#F2F2F2',
    padding: '3%',
    borderRadius: '5px',
    cursor: 'pointer',
    transition: "all ease 0.5s",
    '&:hover' : {
      backgroundColor: 'rgba(166, 166, 166, 0.4)',
      // opacity: '0.5',
    },
  },
  dragEnter: {
    transition: "all ease 0.5s",
    backgroundColor: '#ccffcc !important',
    opacity: '0.5'
  },
  title: {
    // marginBottom: theme.spacing(1),
    marginTop: theme.spacing(2),
    textAlign: 'center',
    color: '#8c8c8c',
    fontWeight: '300'
  },
  loadingDialog: {
    padding: '4%',
    textAlign: 'center',
  },
  uploaded: {
    position: 'relative',
    padding: '2%',
    backgroundColor: 'transparent',
    transition: "all ease 0.5s",
    '&:hover' : {
      backgroundColor: '#F2F2F2',
      opacity: '0.5',
      "&::after": {
        position: 'absolute',
        content: `'DRAG / CLICK YOUR COMPANY LOGO HERE'`,
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        fontFamily: 'roboto',
        color: '#000000',
        fontSize: '10pt'
      }
    },
  }
}));

const Register = props => {
  const classes = useStyles();
  const [progress, setProgress] = useState(0);
  const [recaptchaResponse, setRecaptchaResponse] = useState(false);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [highlighted, setHighlighted] = useState(false);
  const [loading, setLoading] = useState(false);
  const inputFile = useRef(null);

  const makeId = (length) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleCloseLoading = () => {
    setLoading(false)
  }

  // const [form, setForm] = useState({
  //   name: "",
  //   phone: "",
  //   email: "",
  //   accountNumber: "",
  //   location: "",
  //   logo: "https://3.bp.blogspot.com/-ZK6W9UlA3lw/V15RGexr3yI/AAAAAAAAAJ4/nkyM9ebn_qg3_rQWyBZ1se5L_SSuuxcDACLcB/s1600/Bank_Central_Asia.png"
  // });

  const [formState, setFormState] = useState({
    isValid: false,
    values: {
      name: "",
      phone: "",
      email: "",
      account_number: "",
      location: "",
      // logo: "https://3.bp.blogspot.com/-ZK6W9UlA3lw/V15RGexr3yI/AAAAAAAAAJ4/nkyM9ebn_qg3_rQWyBZ1se5L_SSuuxcDACLcB/s1600/Bank_Central_Asia.png"
    },
    touched: {},
    errors: {}
  });

  useEffect(() => {
    document.title = "Corona | Register"
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    if(event.target.name === "phoneNum") {
      if(event.target.value.startsWith("0")) {
        var newPhone = event.target.value.replace('0','')
      } else {
        newPhone = event.target.value
      }
      setFormState(formState => ({
        ...formState,
        values: {
          ...formState.values,
          'phone': `${newPhone}`
        },
        touched: {
          ...formState.touched,
          [event.target.name]: true
        }
      }));
    } else {
      setFormState(formState => ({
        ...formState,
        values: {
          ...formState.values,
          [event.target.name]:
            event.target.type === 'checkbox'
              ? event.target.checked
              : event.target.value
        },
        touched: {
          ...formState.touched,
          [event.target.name]: true
        }
      }));
    }
  };

  const onChange = event => {
    setRecaptchaResponse(true)
  }

  const handleDragOver = event => {
    event.preventDefault();
  }

  const handleOnDrop = event => {
    event.preventDefault();
    setHighlighted(false);

    let type = (event.dataTransfer.files[0].type).split('/')
    console.log(type[0])

    if(type[0] === "image") {
      setLoading(true)
      const image = event.dataTransfer.files[0]

      const name = makeId(20);

      const uploadTask = storage.ref(`images/${name}`).put(image);

      uploadTask.on(
        "state_changed",
        snapshot => {
            const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            )
            setProgress(progress)
        },
        error => {
            console.log(error);
        },
        () => {
            storage
                .ref("images")
                .child(name)
                .getDownloadURL()
                .then(url => {
                    console.log(url)
                    setFormState(formState=>({
                      ...formState,
                      values: {
                        ...formState.values,
                        'logo': url
                      }
                    }))
                    setLoading(false)
                })
        }
      )
    } else {
      handleError("File type is not supported")
    }
  };

  const onButtonClick = () => {
    inputFile.current.click();
  };

  const handleClickUpload = event => {
    event.stopPropagation()
    event.preventDefault()

    const image = event.target.files[0]
    const fileLength = event.target.files.length
    const name = makeId(20);

    if(fileLength > 0) {
      setLoading(true)
      const uploadTask = storage.ref(`images/${name}`).put(image);

      uploadTask.on(
        "state_changed",
        snapshot => {
            const progress = Math.round(
                (snapshot.bytesTransferred / snapshot.totalBytes) * 100
            )
            setProgress(progress)
        },
        function(error){
            console.log(error);
        },
        () => {
            storage
                .ref("images")
                .child(name)
                .getDownloadURL()
                .then(url => {
                    console.log(url)
                    setFormState(formState=>({
                      ...formState,
                      values: {
                        ...formState.values,
                        'logo': url
                      }
                    }))
                    setLoading(false)
                })
        }
      )
    }
  };

  const handleSubmit = event => {
    event.preventDefault()
    console.log(formState)
    Axios({
      method: 'POST',
      url: 'https://resource-bcadashboardcorona.apps.pcf.dti.co.id/api/company-request',
      data: {
        name: formState.values.name,
        phone: `+62${formState.values.phone}`,
        email: formState.values.email,
        account_number: formState.values.account_number,
        location: formState.values.location,
        logo: formState.values.logo
      }   
    }).then(res=>{
      handleSuccess("We will inform you soon")
    }).catch(err=> {
        const error = err.response.data.error_schema.error_message.indonesian
        handleError(error)
    })
  }

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item
          lg={1}
        >
        </Grid>

        <Grid item
          lg={10}
        >
          <Typography
              className={classes.titleRegister}
              variant="h2"
            >
              REGISTER YOUR COMPANY TO CORONA
            </Typography>

          <Card
            className={classes.card}
          >
            <form onSubmit={handleSubmit}>
            <Grid container>
            <Grid
                className={classes.grid}
                style={{
                  marginTop: '2%',
                  marginBottom: '2%'
                }}
                item lg={12} md={12} sm={12} xs={12}>
              
              <input type='file' id='file' ref={inputFile} style={{display: 'none'}} onChange={handleClickUpload} accept="image/*"/>

              <div
                className={`${classes.upload} ${formState.values.logo !== undefined ? classes.uploaded : null} ${highlighted ? classes.dragEnter : null}`}
                // style={formState.values.logo !== undefined ? {backgroundColor: 'transparent'} : null}
                onDragEnter={()=>{setHighlighted(true)}}
                onDragLeave={()=>{setHighlighted(false)}}
                onDragOver={handleDragOver}
                onDrop={handleOnDrop}
                onClick={onButtonClick}
              >
              
              {
                formState.values.logo !== undefined ?

                <img
                  style={{pointerEvents: 'none', marginLeft: 'auto', marginRight: 'auto', display: 'block'}}
                  height='100'
                  src={formState.values.logo}
                  alt="Logo" />

                :

                <Fragment/>
              }


              {
                formState.values.logo === undefined ?
                
                <Fragment>
                  <img
                  style={{pointerEvents: 'none', marginLeft: 'auto', marginRight: 'auto', display: 'block'}}
                  // className={`${highlighted ? classes.dragEnter : ''}`}
                  width='100'
                  src="/images/outbox.png"
                  alt="Drop Here" />

                  <Typography
                    style={{pointerEvents: 'none'}}
                    className={`${classes.title}`}
                    // className={`${classes.title} ${highlighted ? classes.dragEnter : ''}`}
                    variant="h6"
                  >
                    DRAG / CLICK YOUR COMPANY LOGO HERE
                  </Typography>
                </Fragment>

                :

                <Fragment/>
              }
              </div>

            </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  name="name"
                  onChange={handleChange}
                  error={hasError('name')}
                  // onChange={handleFormChange}
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Company Name"
                  value={formState.values.name || ''}
                  helperText={
                    hasError('name') ? formState.errors.name[0] : null
                  }
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  name="phoneNum"
                  error={hasError('phone')}
                  onChange={handleChange}
                  // onChange={handleFormChange}
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Phone Number"
                  type="number"
                  value={formState.values.phone || ''}
                  InputProps={{
                    startAdornment: <InputAdornment position="start">+62</InputAdornment>,
                  }}
                  helperText={
                    hasError('phone') ? formState.errors.phone[0] : null
                  }
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  name="email"
                  error={hasError('email')}
                  onChange={handleChange}
                  // onChange={handleFormChange}
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Email"
                  value={formState.values.email || ''}
                  helperText={
                    hasError('email') ? formState.errors.email[0] : null
                  }
                  variant="outlined" />
              </Grid>

              <Grid
                className={classes.grid}
                item lg={3} md={12} sm={12} xs={12}>
                <TextField 
                  name="account_number"
                  error={hasError('account_number')}
                  onChange={handleChange}
                  // onChange={handleFormChange}
                  className={classes.textfieldMargin}
                  fullWidth
                  label="Account Number"
                  type="number"
                  value={formState.values.account_number || ''}
                  helperText={
                    hasError('account_number') ? formState.errors.account_number[0] : null
                  }
                  variant="outlined" />
              </Grid>
            </Grid>
            <Grid container>
            <Grid
                className={classes.grid}
                style={{marginTop: '2%'}}
                item lg={12} md={12} sm={12} xs={12}>
                <TextField 
                  name="location"
                  error={hasError('location')}
                  onChange={handleChange}
                  // onChange={handleFormChange}
                  className={classes.textfieldMargin}
                  fullWidth
                  value={formState.values.location || ''}
                  helperText={
                    hasError('location') ? formState.errors.location[0] : null
                  }
                  label="Address"
                  multiline
                  rows={2}
                  variant="outlined" />
              </Grid>

            </Grid>

            <center>
              <ReCAPTCHA
                style={{
                  marginTop: '2%'
                }}
                sitekey="6LdmP7MZAAAAAJdDAAcDItvEQn2CIlrKTq0w9oaS"
                render="explicit"
                onChange={onChange}
              />
            </center>

            <Button
              type="submit"
              disabled={!formState.isValid || !recaptchaResponse }
              className={!formState.isValid || !recaptchaResponse ? classes.buttonDisabled : classes.buttonRegister}
            >
              REGISTER NOW <ArrowRightAltIcon style={{marginLeft: '1%'}} />
            </Button>
          </form>
          </Card>
        </Grid>

      </Grid>


      <Dialog
        className={classes.loadingDialog}
        open={loading}
        fullWidth={true}
        maxWidth={'xs'}
        onClose={handleCloseLoading}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>

          <img
            style={{pointerEvents: 'none', marginLeft: 'auto', marginRight: 'auto', display: 'block'}}
            // className={`${highlighted ? classes.dragEnter : ''}`}
            height='200'
            src="/images/walking.gif"
            alt="Loading" />

            <Typography
                style={{pointerEvents: 'none', marginBottom: '5%', fontWeight: '900', color: '#000000'}}
                className={`${classes.title}`}
                variant="h4"
              >
                {progress}%
              </Typography>

        </DialogContent>
        </Dialog>

        <Dialog
            open={openFlash}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogContent>
              <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
                <Typography variant="h2" style={{textAlign:'center'}}>
                  Success!
                </Typography>
                <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
            </DialogContent>
          </Dialog>


        <Dialog
          open={openFlashErr}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          >
          <DialogContent>
              <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
              <Typography variant="h2" style={{textAlign:'center'}}>
                  Error!
              </Typography>
              
              <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
              {message}
              </DialogContentText>
          </DialogContent>
        </Dialog>

    </div>
  );
};

Register.propTypes = {
  history: PropTypes.object
};

export default withRouter(Register);
