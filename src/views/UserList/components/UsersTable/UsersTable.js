import React, { useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { makeStyles } from '@material-ui/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import CreateIcon from '@material-ui/icons/Create';
import VisibilityIcon from '@material-ui/icons/Visibility';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  IconButton,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  TextField,
  Select,
  Tooltip,
  MenuItem,Card, CardContent
} from '@material-ui/core';
import auth from 'services/auth'
import { StatusBullet } from 'components';
import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Skeleton from 'react-loading-skeleton';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  status: {
    marginRight: theme.spacing(1)
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  table: {
    width: "25%",
    backgroundColor: '#FAFAFA'
  },
  pagination:{
    marginLeft: theme.spacing(3)
  }
}));

const statusColors = {
  Active: 'success',
  Nonactive: 'danger'
};

const UsersTable = props => {
  const classes = useStyles();

  const { getData, handleSuccess, handleError, handleErrorLogout, branches, divisions, loading, users, size, page, count, pages, ranks} = props;
  const [open, setOpen] = useState(false);
  const [openDetail, setOpenDetail] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [openPermission, setOpenPermission] = useState(false);
  const [user, setUser] = useState({});

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  const handleClickOpenDetail = (id) => {
    auth.get(`/api/user/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
    }).then(res =>{
      const data =  res.data.output_schema
      setUser(data.user)
      setOpenDetail(true);
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  const handleCloseDetail = () => {
    setOpenDetail(false);
  }

  const handleClickOpenEdit = (id) => {
    auth.get(`/api/user/${id}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
    }).then(res =>{
      const data =  res.data.output_schema
      setUser(data.user)
      setOpen(true)
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  };

  const handleClickOpenDelete = (id,name) => {
    setOpenDelete(true);
    setUser({
      'uuid': id,
      'name': name,
      'is_deleted': true
    })
  };

  const handleClickOpenPermission = () => {
    setOpenPermission(true);
  };

  const handleSave = () => {
    console.log(user)
    auth({
      method: 'PUT',
      url: '/api/change-identity-employee',
      data: user,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
    }).then(res=>{
      setOpen(false);
      getData();
      handleSuccess('Data successfully updated');
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  const handleOnChange = (event) => {
    const { name, value } = event.target;

    if(name === "rank") {
      let role
      if(value === "Karyawan") {
        role = "EMPLOYEE"
      } else if (value === "Kepala Divisi" || value === "Kepala Cabang") {
        role = "ADMIN_DELEGATE"
      } else if (value === "Kepala Pusat") {
        role = "ADMIN_MASTER"
      }
      setUser({ ...user, "rank": event.target.value, "role": role });
    } else {
      setUser({ ...user, [name]: value });
    }
  }

  const handleDelete = () => {
    auth({
      method: 'PUT',
      url: '/api/change-identity-employee',
      data: user,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenDelete(false);
        getData();
        handleSuccess('Data successfully deleted');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  
  const handleBackButtonClick = () => {
    if(page>0){
      props.onPageChange(page-1);
      props.onLoadingChange(true);
    }else{
      alert("this is min page")
    }
  };

  const handleNextButtonClick = () => {
    if(page+1<pages){
      props.onPageChange(page+1);
      props.onLoadingChange(true);
    }else{
      alert("this is max page")
    }
  };

  const handleRowsPerPageChange = event => {
    props.onSizeChange(event.target.value);
    props.onPageChange(0);
    props.onLoadingChange(true);
  };

  const handleClosePermission = () => {
    setOpenPermission(false)
  }

  const handleReset = () => {
    console.log(user.uuid)
    auth({
      method: 'PUT',
      url: '/api/reset-user',
      data: {
        uuid: user.uuid,
        is_actived: false
      },
      headers: {
        'Authorization': `Bearer ${access_token}`, 
      }
    }).then(res=>{
      getData();
      handleSuccess('Password successfully reset');
      setOpenPermission(false);
      setOpenDetail(false);
    }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
    })
  }

  return (
    <Fragment>
        <PerfectScrollbar>
          <div className={classes.inner}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell></TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Branch</TableCell>
                  <TableCell>Rank</TableCell>
                  <TableCell>Division</TableCell>
                  <TableCell>Role</TableCell>
                  <TableCell>Active</TableCell>
                </TableRow>
              </TableHead>
              {loading ?
                <TableBody>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                    <TableCell><Skeleton/></TableCell>
                  </TableRow>
                </TableBody>
              : 
              <TableBody>
                {users.length === 0 ?
                    <TableRow>
                      <TableCell colSpan='7' style={{textAlign: 'center'}}> No data available </TableCell>
                    </TableRow>
                  :
                users.slice(0, size).map(user => (
                  <TableRow
                    className={classes.tableRow}
                    hover
                    key={user.uuid}
                  >
                    <TableCell style={{width: '17%'}}>
                    <Tooltip title={`Detail ${user.name}`} placement="top">
                      <IconButton onClick={() => handleClickOpenDetail(user.uuid)}>
                        <VisibilityIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={`Edit ${user.name}`} placement="top">
                      <IconButton onClick={() => { handleClickOpenEdit(user.uuid) }}>
                        <CreateIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={`Delete ${user.name}`} placement="top">
                      <IconButton onClick={() => handleClickOpenDelete(user.uuid, user.name)}>
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                    </TableCell>
                    <TableCell>{user.name}</TableCell>
                    <TableCell>{user.branch}</TableCell>
                    <TableCell>{user.rank}</TableCell>
                    <TableCell>{user.division}</TableCell>
                    <TableCell>{user.role}</TableCell>
                    <TableCell>
                    <div className={classes.statusContainer}>
                        <StatusBullet
                          className={classes.status}
                          color={statusColors[user.actived ? "Active": "Nonactive"]}
                          size="sm"
                        />
                        {user.actived? "Active": "Nonactive"}
                      </div></TableCell>
                  </TableRow>
                ))}
              </TableBody>
              } 
            </Table>
            <Dialog
              open={openDetail}
              onClose={handleCloseDetail}
              fullWidth
              maxWidth="md"
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">
              <div className={classes.row}>
                  <Typography variant="h4" >
                   Detail User
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseDetail}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <Card>
                <CardContent>
                      
                    <Table>
                    <TableBody>
                      <TableRow>
                        <TableCell className={classes.table}>Employee ID</TableCell>
                        <TableCell>{user.employee_id}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Employee Name</TableCell>
                        <TableCell>{user.name}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>ID Number</TableCell>
                        <TableCell>{user.identity_number}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Email</TableCell>
                        <TableCell>{user.email}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Phone</TableCell>
                        <TableCell>{user.phone}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Address</TableCell>
                        <TableCell>{user.address}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Branch</TableCell>
                        <TableCell>{user.branch}</TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell className={classes.table}>Division</TableCell>
                        <TableCell>{user.division}</TableCell>
                      </TableRow>
                      </TableBody>
                  </Table>
                  </CardContent>
              </Card>
              </DialogContent>
              <DialogActions>
              </DialogActions>
            </Dialog>
            <Dialog
              open={open}
              fullWidth={true}
              maxWidth={'sm'}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{`${user.employee_id} ${user.name}`}</DialogTitle>
              <DialogContent>
                    <TextField
                      onChange={handleOnChange}
                      select
                      margin="dense"
                      label="Branch"
                      name="branch"
                      value={user.branch}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                    >
                      {branches.map((branch) => (
                        <option key={branch.uuid} value={branch.name}>
                          {branch.name}
                        </option>
                      ))
                      }
                    </TextField>

                    <TextField
                      onChange={handleOnChange}
                      select
                      margin="dense"
                      label="Division"
                      name="division"
                      value={user.division}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                    >
                      {divisions.map((division) => (
                        <option key={division.uuid} value={division.name}>
                          {division.name}
                        </option>
                      ))
                      }
                    </TextField>

                    <TextField
                      onChange={handleOnChange}
                      select
                      margin="dense"
                      label="Position"
                      name="rank"
                      value={user.rank}
                      SelectProps={{
                        native: true,
                      }}
                      fullWidth
                    >
                      {ranks.map((rank) => rank.name === "Kepala Pusat" ? null : (
                        <option key={rank.uuid} value={rank.name}>
                          {rank.name}
                        </option>
                      ))
                      }
                    </TextField>
              </DialogContent>
              <DialogActions>
              <Button
                color='secondary'
                onClick={handleClickOpenPermission} autoFocus>
                Reset Password
              </Button>
              <div style={{
                flexGrow: 1
              }}/>
              <Button onClick={handleClose} color='secondary' autoFocus>
                Cancel
              </Button>
              <Button onClick={handleSave} color='primary' autoFocus>
                Save
              </Button>
            </DialogActions>
            </Dialog>
            <Dialog
              open={openDelete}
              onClose={handleCloseDelete}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  {`Are you sure want to delete ${user.name}?`}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDelete} color="default">
                  Cancel
                </Button>
                <Button onClick={handleDelete} color="secondary" autoFocus>
                  Delete
                </Button>
              </DialogActions>
            </Dialog>

            <Dialog
              open={openPermission}
              onClose={handleClosePermission}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"Confirmation"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  {`Are you sure want to reset ${user.name} password?`}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClosePermission} color="default">
                  Cancel
                </Button>
                <Button onClick={handleReset} color="secondary" autoFocus>
                  Reset
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        </PerfectScrollbar>
        <div className={classes.row}>
          <Typography className={classes.pagination}>Total data : {count}</Typography>
          <span className={classes.spacer} />
          <Typography className={classes.pagination}>Rows per page : </Typography>
          <Select
            name="size"
            value={size}
            onChange={handleRowsPerPageChange}
            className={classes.pagination}
          >
            <MenuItem value={"5"}>5</MenuItem>
            <MenuItem value={"10"}>10</MenuItem>
            <MenuItem value={"25"}>25</MenuItem>
          </Select>
          <Typography className={classes.pagination}>Page {page+1} of {pages}</Typography>
          <IconButton onClick={handleBackButtonClick} disabled={page>0?false:true}>
             <KeyboardArrowLeftIcon />
          </IconButton>
          <IconButton onClick={handleNextButtonClick} disabled={page+1<pages?false:true}> 
            <KeyboardArrowRightIcon />
          </IconButton>
        </div>
    </Fragment>
  );
};

UsersTable.propTypes = {
  className: PropTypes.string,
  users: PropTypes.array.isRequired
};

export default UsersTable;
