import React, {useState, useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';
import { parse } from 'papaparse';
import auth from 'services/auth';
import validate from 'validate.js';
import { 
  Typography,
  Button, 
  Dialog,
  DialogTitle,
  DialogContent,
  InputAdornment,
  TextField,
  DialogActions,
  Hidden,
  Grid,
  Tooltip, IconButton} from '@material-ui/core';

import SearchIcon from '@material-ui/icons/Search';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import PostAddIcon from '@material-ui/icons/PostAdd';
import FilterListIcon from '@material-ui/icons/FilterList';
import CloseIcon from '@material-ui/icons/Close';
import { SearchInput } from 'components';

const schema = {
  employee_id: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  identity_number: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: true,
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  name: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  phone: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 128
    }
  },
  address: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 255
    }
  },
  rank: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 255
    }
  },
  branch: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 255
    }
  },
  division: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 4,
      maximum: 255
    }
  }
};

const useStyles = makeStyles(theme => ({
  root: {},
  row: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginTop: theme.spacing(1)
  },
  spacer: {
    flexGrow: 1
  },
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  searchInput: {
    marginRight: theme.spacing(1)
  },
  table: {
    minWidth: 270
  },
  textField: {
    marginBottom: theme.spacing(1)
  },
  title: {
    // marginBottom: theme.spacing(1),
    marginTop: theme.spacing(2)
  },
  box: {
    textAlign: 'center',
    paddingTop: '10%',
    paddingBottom: '10%',
    borderRadius: '5px',
    // backgroundImage: 'url(/images/document.svg)',
    backgroundSize: '150px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    cursor: 'pointer'
  },
  default: {
    background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    margin: theme.spacing(0.5)
  },
  success: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    float: 'right'
  },
  primary: {
    background: 'linear-gradient(45deg, rgba(54,123,195,1) 0%, rgba(44,100,160,1) 49%)',
    padding: theme.spacing(1.25),
    backgroundSize: "cover",
    backgroundPosition: "center center",
    color: '#FFFFFF',
    margin: theme.spacing(0.5),
    marginRight: '0'
  },
  buttonFilterMobile: {
    background: 'linear-gradient(45deg, rgba(242,242,242,1) 0%, rgba(230,230,230,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    width: '82%'
  },
  addMobile: {
    background: 'linear-gradient(45deg, rgba(130,172,38,1) 0%, rgba(110,146,32,1) 49%)',
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
    marginLeft: theme.spacing(1),
    color: '#FFFFFF'
  },
  dragEnter: {
    opacity: '0.5',
    // transition: 'opacity 0.2s ease-in-out',
    // backgroundColor: 'black'
  }
}));

const UsersToolbar = props => {
  const { getData, handleSuccess, handleError, handleErrorLogout, branches, divisions, ranks } = props;
  const [openForm, setOpenForm] = useState(false);
  const [openImport, setOpenImport] = useState(false);
  const [highlighted, setHighlighted] = useState(false);
  const [employee, setEmployee] = useState([]);
  const [upload, setUpload] = useState(false);
  const [uploadTotal, setUploadTotal] = useState(0);
  const inputFile = useRef(null);
  const [word, setWord] = useState("");
  const [data, setData] = useState([]);
  const [openFilter, setOpenFilter] = useState(false);
  const [filter, setFilter] = useState({});
  const [openAddMenu, setOpenAddMenu]= useState(false);

  const company_id = JSON.parse(localStorage.getItem("data")).company_id

  useEffect(() => {
    if(employee.length > 0){
      for(var i=0; i<employee.length; i++) {
        const rank = employee[i].position;
        const phone = employee[i].phone;
        let role;
        if(rank === "Karyawan") {
          role = "EMPLOYEE"
        } else if(rank === "Kepala Divisi" || rank === "Kepala Cabang") {
          role = "ADMIN_DELEGATE"
        } else if(rank === "Kepala Pusat") {
          role = "ADMIN_MASTER"
        }

        let newPhone;
        if(phone.startsWith("0")) {
          newPhone = phone.replace('0','')
        } else {
          newPhone = phone
        }
        delete employee[i].position
        delete employee[i].phone
        data.push({...employee[i], company_id: company_id, rank: rank, role: role, phone: `+62${newPhone}` })
      }
      setUploadTotal(employee.length);
      setUpload(true);
    } else {
      setUpload(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[employee])

  const access_token = JSON.parse(localStorage.getItem("data")).access_token

  const initialUserState = {
    uuid: null,
    company_id: company_id,
    identity_number: "",
    employee_id: "",
    email: "",
    name: "",
    phone: "",
    address: "",
    role: null,
    branch: null,
    division: null,
    rank: null
  };

  const initialFilterState = {
    division: "",
    branch: "",
    active:""
  };

  // const [user, setUser] = useState(initialUserState);
  const [user, setUser] = useState({
    isValid: false,
    values: initialUserState,
    touched: {},
    errors: {}
  });

  const classes = useStyles();

  useEffect(() => {
    const errors = validate(user.values, schema);

    setUser(user => ({
      ...user,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [user.values]);

  useEffect(()=> {
    setFilter(initialFilterState);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[user.values])

  const handleClickOpenForm = () => {
    setOpenForm(true);
  };

  const handleCloseForm = () => {
    setOpenForm(false);
    // setUser(initialUserState);
    setUser({
      isValid: false,
      values: initialUserState,
      touched: {},
      errors: {}
    })
  };

  const handleClickOpenImport = () => {
    setOpenImport(true);
  }

  const handleCloseImport = () => {
    setUpload(false);
    setOpenImport(false);
  }

  const handleDragOver = (event) => {
    event.preventDefault();
  }

  const handleOnDrop = (event) => {
    event.preventDefault();
    setData([]);
    setHighlighted(false);
    Array.from(event.dataTransfer.files)
      .forEach(async (file) => {
        const text = await file.text();
        const result = parse(text, {header:true, skipEmptyLines:true});
        const result1 = parse(text);
        const header = result1.data[0];
        if(header.includes('identity_number') &&
            header.includes('employee_id') &&
            header.includes('email') &&
            header.includes('name') &&
            header.includes('phone') &&
            header.includes('address') &&
            header.includes('branch') &&
            header.includes('division') &&
            header.includes('position')) {
          setEmployee(result.data)
        } else {
          alert('Header Tidak Sesuai');
          setUpload(false);
        }
    });
  }

  const handleCloseAddMenu = () => {
    setOpenAddMenu(false)
  }

  const handleUpload = () => {
    console.log(data)

    auth({
      method: 'POST',
      url: '/api/registration',
      data: data,
      // data: data,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
          handleCloseImport();
          getData();
          setData([]);
          handleSuccess('Data successfully added');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })
  }

  const onButtonClick = () => {
    inputFile.current.click();
  };

  const handleClickUpload = (event) => {
    event.stopPropagation();
    event.preventDefault();
    setData([]);
    Array.from(event.target.files)
      .forEach(async(file)=> {
        const text = await file.text();
        const result = parse(text, {header:true, skipEmptyLines:true});
        const result1 = parse(text);
        const header = result1.data[0];
        if(header.includes('identity_number') &&
            header.includes('employee_id') &&
            header.includes('email') &&
            header.includes('name') &&
            header.includes('phone') &&
            header.includes('address') &&
            header.includes('branch') &&
            header.includes('division') &&
            header.includes('position')) {
          setEmployee(result.data)
        } else {
          alert('Header Tidak Sesuai');
          setUpload(false);
        }
      })
  }
  const handleSearchChange = (event) => {
    setWord(event.target.value);
  };

  const handleSearch = () => {
    props.onKeywordChange(word);
    props.onPageChange(0);
  };

  const handleFilterChange = event => {
    const { name, value } = event.target;
    setFilter({ ...filter, [name]: value });
  };

  const handleClickOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleCloseFilter = () => {
    setOpenFilter(false);
  };

  const handleFilter = () => {
    setOpenFilter(false);
    console.log(filter.status);
    props.onFilterChange(filter.division, filter.branch, filter.active);
    props.onPageChange(0);
  };

  const handleClearFilter = () => {
    setOpenFilter(false);
    props.onFilterChange('','','');
    props.onPageChange(0);
  };

  const handleInputChange = event => {

    if(event.target.name === "rank") {
      let role
      if(event.target.value === "Karyawan") {
        role = "EMPLOYEE"
      } else if (event.target.value === "Kepala Divisi" || event.target.value === "Kepala Cabang") {
        role = "ADMIN_DELEGATE"
      } else if (event.target.value === "Kepala Pusat") {
        role = "ADMIN_MASTER"
      }

      setUser(user => ({
        ...user,
        values: {
          ...user.values,
          "rank": event.target.value, "role": role
        },
        touched: {
          ...user.touched,
          [event.target.name]: true
        }
      }));

    } else if(event.target.name === "phoneNum") {
      if(event.target.value.startsWith("0")) {
        var newPhone = event.target.value.replace('0','')
      } else {
        newPhone = event.target.value
      }
      setUser(user => ({
        ...user,
        values: {
          ...user.values,
          'phone': newPhone
        },
        touched: {
          ...user.touched,
          'phone': true
        }
      }));

    }else {
      const { name, value } = event.target;
      // setUser({ ...user, [name]: value });

      setUser(user => ({
        ...user,
        values: {
          ...user.values,
          [name]: value
        },
        touched: {
          ...user.touched,
          [name]: true
        }
      }));
    }
  };

  const handleAddUser = () => {
    let dataArray = [{
      company_id: user.values.company_id,
      identity_number: user.values.identity_number,
      employee_id: user.values.employee_id,
      email: user.values.email,
      name: user.values.name,
      phone: `+62${user.values.phone}`,
      address: user.values.address,
      role: user.values.role,
      branch: user.values.branch,
      division: user.values.division,
      rank: user.values.rank
    }]
    auth({
      method: 'POST',
      url: '/api/registration',
      data: dataArray,
      headers: {
          'Authorization': `Bearer ${access_token}`, 
      }
     }).then(res =>{
        setOpenForm(false);
        getData();
        setUser({
          isValid: false,
          values: initialUserState,
          touched: {},
          errors: {}
        });
        handleSuccess('Data successfully added');
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })
  };

  let footerMenu;
  if(upload) {
    footerMenu = <DialogActions>
                  <Button>
                    Total : {uploadTotal}
                  </Button>
                  <Button onClick={handleCloseImport} color="primary" autoFocus>
                    Cancel
                  </Button>
                  <Button onClick={handleUpload} color="primary" autoFocus style={{backgroundColor: '#30318b', color: '#ffffff'}}>
                    Upload
                  </Button>
                </DialogActions>
  }

  const hasError = field =>
    user.touched[field] && user.errors[field] ? true : false;

  return (
    <div
      className={classes.root}
    >
      <div className={classes.row}>
        <SearchInput
          className={classes.searchInput}
          placeholder="Search user"
          onChange={handleSearchChange}
          value={word}
        />
        <Tooltip title="Search" placement="top">
          <Button
            className={classes.primary}
            variant="contained"
            onClick={handleSearch}
          >
            <SearchIcon/>
          </Button>
        </Tooltip>

        <Hidden mdDown>
        <span className={classes.spacer} />

        <Tooltip title="Import CSV" placement="top">
          <Button
            className={classes.importButton}
            onClick={handleClickOpenImport}
          >
            <NoteAddIcon />
          </Button>
        </Tooltip>

        <Tooltip title="Add User" placement="top">
          <Button
            variant="contained"
            className={classes.success}
            onClick={handleClickOpenForm}
          >
            <PostAddIcon />
          </Button>
        </Tooltip>

        <Tooltip title="Filter" placement="top">
            <Button
              className={classes.default}
              variant="contained"
              onClick={handleClickOpenFilter}
            >
              <FilterListIcon/>
            </Button>
            </Tooltip>
      </Hidden>
      </div>

      <Hidden mdUp>
        <Button
          onClick={handleClickOpenFilter}
          className={classes.buttonFilterMobile}
          variant="contained"
          startIcon={<FilterListIcon />}
        >
          Filter & Sort
        </Button>

        <Button
          onClick={()=>{setOpenAddMenu(true)}}
          className={classes.addMobile}
          variant="contained"
        >
          <NoteAddIcon />
        </Button>
      </Hidden>

{/* Dialog Add User */}
      <Dialog
        open={openForm}
        fullWidth={true}
        maxWidth={'sm'}
        onClose={handleCloseForm}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogTitle id="alert-dialog-title">
          <Typography
            className={classes.title}
            variant="h3"
          >
            Add User
          </Typography>
        </DialogTitle>
        <DialogContent>

          <TextField
            className={classes.textField}
            fullWidth
            label="Employee ID"
            name="employee_id"
            type="text"
            margin="dense"
            onChange={handleInputChange}
            error={hasError('employee_id')}
            helperText={
              hasError('employee_id') ? user.errors.employee_id[0] : null
            }
            value={user.values.employee_id}
          />
          
          <TextField
            className={classes.textField}
            fullWidth
            label="ID Number"
            name="identity_number"
            type="text"
            onChange={handleInputChange}
            error={hasError('identity_number')}
            helperText={
              hasError('identity_number') ? user.errors.identity_number[0] : null
            }
            value={user.values.identity_number}
          />

          <TextField
            className={classes.textField}
            fullWidth
            label="Email"
            name="email"
            type="email"
            onChange={handleInputChange}
            error={hasError('email')}
            helperText={
              hasError('email') ? user.errors.email[0] : null
            }
            value={user.values.email}
          />
          
          <TextField
            className={classes.textField}
            fullWidth
            label="Name"
            name="name"
            type="text"
            onChange={handleInputChange}
            error={hasError('name')}
            helperText={
              hasError('name') ? user.errors.name[0] : null
            }
            value={user.values.name}
          />

          <TextField
            className={classes.textField}
            fullWidth
            label="Phone Number"
            name="phoneNum"
            type="number"
            onChange={handleInputChange}
            InputProps={{
              startAdornment: <InputAdornment position="start">+62</InputAdornment>,
            }}
            error={hasError('phone')}
            helperText={
              hasError('phone') ? user.errors.phone[0] : null
            }
            value={user.values.phone}
          />

          <TextField
            className={classes.textField}
            fullWidth
            label="Address"
            name="address"
            type="text"
            onChange={handleInputChange}
            error={hasError('address')}
            helperText={
              hasError('address') ? user.errors.address[0] : null
            }
            value={user.values.address}
          />

          <TextField            
            className={classes.textField}
            select
            margin="dense"
            label="Position"
            name="rank"
            onChange={handleInputChange}
            SelectProps={{
              native: true,
            }}
            fullWidth
            error={hasError('rank')}
            helperText={
              hasError('rank') ? user.errors.rank[0] : null
            }
            value={user.values.rank}
          >
            <option value=""></option>
          {ranks.map((rank) => rank.name === "Kepala Pusat" ? null : (
            <option key={rank.uuid} value={rank.name}>
              {rank.name}
            </option>
          ))
          }
        </TextField>

          <TextField
            
            className={classes.textField}
            select
            margin="dense"
            label="Branch"
            name="branch"
            onChange={handleInputChange}
            // value={user.branch}
            SelectProps={{
              native: true,
            }}
            fullWidth
            error={hasError('branch')}
            helperText={
              hasError('branch') ? user.errors.branch[0] : null
            }
            value={user.values.branch}
          >
            <option value=""></option>
          {branches.map((branch) => (
            <option key={branch.uuid} value={branch.name}>
              {branch.name}
            </option>
          ))
          }
        </TextField>

          <TextField
            className={classes.textField}
            select
            margin="dense"
            label="Division"
            name="division"
            onChange={handleInputChange}
            SelectProps={{
              native: true,
            }}
            fullWidth
            error={hasError('division')}
            helperText={
              hasError('division') ? user.errors.division[0] : null
            }
            value={user.values.division}
          >
            <option value=""></option>
            {divisions.map((division) => (
              <option key={division.uuid} value={division.name}>
                {division.name}
              </option>
            ))
            }
          </TextField>

        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseForm} color="primary" autoFocus>
            Cancel
          </Button>
          <Button onClick={handleAddUser} color="primary" autoFocus style={{backgroundColor: '#30318b', color: '#ffffff'}}>
            Add
          </Button>
        </DialogActions>
        </Dialog>

{/* Dialog Import User */}
      <Dialog
        open={openImport}
        fullWidth={true}
        maxWidth={'sm'}
        onClose={handleCloseImport}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        <DialogContent>

          <div
            className={`${classes.box}`}
            onDragOver={handleDragOver}
            onDragEnter={()=>{setHighlighted(true)}}
            onDragLeave={()=>{setHighlighted(false)}}
            onDrop={handleOnDrop}
            onClick={onButtonClick}
            >

              <img
                style={{pointerEvents: 'none'}}
                className={`${highlighted ? classes.dragEnter : ''}`}
                width='150'
                src="/images/document.svg"
                alt="Drop Here" />
              <Typography
                style={{pointerEvents: 'none'}}
                className={`${classes.title} ${highlighted ? classes.dragEnter : ''}`}
                variant="h5"
              >
                CLICK / DROP HERE
              </Typography>

          </div>

          <input type='file' id='file' ref={inputFile} style={{display: 'none'}} onChange={handleClickUpload}/>

        </DialogContent>
        {footerMenu}
        </Dialog>

        <Dialog
              open={openFilter}
              onClose={handleCloseFilter}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="sm"
              fullWidth
            >
              <DialogTitle id="alert-dialog-title">
                <div className={classes.row}>
                  <Typography variant="h4" >
                    Filter
                  </Typography>
                  <span className={classes.spacer} />
                  <IconButton onClick={handleCloseFilter}>
                    <CloseIcon />
                  </IconButton>
                </div>
              </DialogTitle>
              <DialogContent>
                <TextField
                    className={classes.textField}
                    select
                    margin="dense"
                    label="Division"
                    name="division"
                    onChange={handleFilterChange}
                    SelectProps={{
                      native: true,
                    }}
                    fullWidth
                    value={filter.division}
                  >
                    <option value=""></option>
                  {divisions.map((division) => (
                    <option key={division.uuid} value={division.uuid}>
                      {division.name}
                    </option>
                  ))
                  }
                </TextField>
                <TextField
                    select
                    margin="dense"
                    label="Branch"
                    name="branch"
                    onChange={handleFilterChange}
                    SelectProps={{
                      native: true,
                    }}
                    fullWidth
                    value={filter.branch}
                  >
                    <option value=""></option>
                  {branches.map((branch) => (
                    <option key={branch.uuid} value={branch.uuid}>
                      {branch.name}
                    </option>
                  ))
                  }
                </TextField>
                <TextField
                    select
                    margin="dense"
                    label="Active"
                    name="active"
                    onChange={handleFilterChange}
                    SelectProps={{
                      native: true,
                    }}
                    fullWidth
                    value={filter.active}
                  >
                    <option value=""></option>
                    <option value="1">Active</option>
                    <option value="0">Nonactive</option>
                  ))
                </TextField>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClearFilter} color="secondary" >
                  Clear Filter
                </Button>
                <Button onClick={handleFilter} color="primary" autoFocus>
                  Filter
                </Button>
              </DialogActions>
            </Dialog>

            <Dialog
              onClose={handleCloseAddMenu}
              open={openAddMenu}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              maxWidth="sm"
              fullWidth
            >
              <DialogContent>

              <Grid container>
                <Grid item sm={12} xs={12}>
                  <Button className={classes.default}
                    onClick={handleClickOpenImport}
                    style={{
                      marginLeft: '0',
                      marginRight: '0'
                    }}
                    fullWidth startIcon={<NoteAddIcon />}>
                    IMPORT FROM CSV
                  </Button>
                </Grid>

                <Grid item sm={12} xs={12}>
                  <Button className={classes.success}
                    onClick={handleClickOpenForm}
                    style={{
                      marginLeft: '0',
                      marginRight: '0'
                    }}
                    fullWidth startIcon={<PostAddIcon />}>
                    ADD MANUALLY
                  </Button>
                </Grid>
              </Grid>
                
              </DialogContent>
            </Dialog>
    </div>

    
  );
};

UsersToolbar.propTypes = {
  className: PropTypes.string
};

export default UsersToolbar;
