import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import auth from 'services/auth'
import {
  Dialog,
  DialogContent,
  DialogContentText,
  Typography,
  CardContent,
  Card,
  Grid,
  Box, Tooltip, IconButton
} from '@material-ui/core'

import PeopleIcon from '@material-ui/icons/People';
import { UsersToolbar, UsersTable } from './components';
import moment from 'moment';
import ReactExport from "react-export-excel";
import jsPDF from "jspdf";
import "jspdf-autotable";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3),
  },
  content: {
    marginTop: theme.spacing(2)
  },
  flash: {
    height : 200,
    display: 'block',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  row: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: theme.spacing(2),
    zIndex: "100",
    background: "linear-gradient(45deg, rgba(255,102,102,1) 0%, rgba(255,51,51,1) 49%)",
    // backgroundImage: `url("/images/danger.png")`,
    // background: "linear-gradient(25deg, rgba(238,81,78,1) 0%, rgba(232,65,61,1) 35%)",
    backgroundSize: "cover",
    backgroundPosition: "center center",
    borderRadius: '5px',
    padding: '12px',
    width: 57
  },
  iconColor: {
    color: '#FFFFFF'
  },
  export: {
    height: '42px',
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2)
  },
  spacer: {
    flexGrow: 1
  },
  title: {
    color: '#1A2038',
    paddingLeft: '16px',
    marginTop: "auto",
  },
  icon:{
    marginLeft:5, 
    marginRight:10
  },
  package: {
    display: "flex",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(-4.8),
    zIndex: "100"
  },
  exportIcons: {
    maxWidth: 30,
    margin: '5%'
  }
}));

const UserList = () => {
  const history = useHistory();
  const classes = useStyles();

  const ExcelFile = ReactExport.ExcelFile;
  const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
  const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

  const [users, setUsers] = useState([]);
  const [exportData, setExportData] = useState([]);
  const [loading, setLoading]= useState(true);
  const [openFlash, setOpenFlash]= useState(false);
  const [openFlashErr, setOpenFlashErr]= useState(false);
  const [message, setMessage]= useState('');
  const [imgAge, setImgAge]=useState(Date.now);
  const [branches, setBranches] = useState([]);
  const [divisions, setDivisions] = useState([]);
  const [ranks, setRanks] = useState([]);
  const [size, setSize]=useState(10);
  const [page, setPage]=useState(0);
  const [count, setCount]=useState(0);
  const [pages, setPages]=useState(0);
  const [keyword, setKeyword]=useState("");
  const [division, setDivision]=useState('');
  const [branch, setBranch]=useState('');
  const [active, setActive]=useState('');
  var generated_date = moment(new Date()).format('DD/MM/YYYY');

  const handleLogout = () => {
    auth({
        method: 'POST',
        url: '/revoke',
        headers: {
            'Authorization': `Bearer ${access_token}`
        }
    }).then(res => {
        localStorage.clear()
        history.push("/")
    }).catch(err => {
        
      localStorage.clear()
      history.push("/")
    })
  }

  const access_token = JSON.parse(localStorage.getItem("data")).access_token;

  const handleError= (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleErrorLogout = (msg) =>{
    setMessage(msg);
    setOpenFlashErr(true);
    setTimeout(
      function() {
        setOpenFlashErr(false);
        handleLogout();
      },
      3000
    );
    setImgAge(Date.now());
  }

  const handleFlashClose=()=>{
    setOpenFlash(false);
  }

  const handleFlashErrClose=()=>{
    setOpenFlashErr(false);
  }

  const getData = () => {
    auth.get(`/api/employee?size=${size}&page=${page}&search=${keyword}&division=${division}&branch=${branch}&active=${active}`, {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const baseData = res.data.output_schema
        setUsers(baseData.users.content)
        setCount(baseData.users.total_elements)
        setPages(baseData.users.total_pages)
        setLoading(false)
     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })

      auth.get(`/api/employee?size=99999&page=0&search=${keyword}&division=${division}&branch=${branch}&active=${active}`, {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
      }).then(res =>{
        const baseData = res.data.output_schema
        setExportData(baseData.users.content)
      }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
      })
  }

  const getDivision = () => {
    auth.get('/api/division?size=1000', {
      headers: {
        'Authorization': `Bearer ${access_token}`
      }
     }).then(res =>{
        const baseData = res.data.output_schema
        setDivisions(baseData.division.content)

     }).catch(err=>{
      if(err.response){
        if(401 === err.response.status ) {
          handleErrorLogout("Your session was invalid")
        } else if (403 === err.response.status) {
          const message = err.response.data.error_schema.error_message.indonesian
          handleErrorLogout(message)
        } else{
          const message = err.response.data.error_schema.error_message.indonesian
          handleError(message)
        }
      }else{
        handleErrorLogout("Something went wrong")
      }
     })
  }

  const getBranch = () => {
    auth.get('/api/branch?size=1000', {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
      }).then(res =>{
          const baseData = res.data.output_schema
          setBranches(baseData.branch.content)
      }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
    })
  }

  const getRank = () => {
    auth.get('/api/rank/', {
        headers: {
          'Authorization': `Bearer ${access_token}`
        }
      }).then(res =>{
        setRanks(res.data)
      }).catch(err=>{
        if(err.response){
          if(401 === err.response.status ) {
            handleErrorLogout("Your session was invalid")
          } else if (403 === err.response.status) {
            const message = err.response.data.error_schema.error_message.indonesian
            handleErrorLogout(message)
          } else{
            const message = err.response.data.error_schema.error_message.indonesian
            handleError(message)
          }
        }else{
          handleErrorLogout("Something went wrong")
        }
    })
  }
  
  useEffect(()=>{
    document.title = "Corona | User"
    getData();
    getBranch();
    getDivision();
    getRank();
    setLoading(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[size,page,keyword, division, branch, active])

  const handleSuccess= (msg) =>{
    setMessage(msg);
    setOpenFlash(true);
    setTimeout(
      function() {
        setOpenFlash(false);
      },
      1500
    );
    setImgAge(Date.now());
  }

  const keywordChange = (word) => {
    setKeyword(word)
  }

  const filterChange = (division, branch, active) => {
    setDivision(division);
    setBranch(branch);
    setActive(active);
  }

  const exportPDF = () => {
    const unit = "pt";
    const size = "A4";
    const orientation = "landcsape";

    const doc = new jsPDF(orientation, unit, size);

    doc.setFontSize(15);

    var today = moment(new Date()).format('DD/MM/YYYY');
    const title = "User";
    const offset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(title) * doc.internal.getFontSize() / 2);
    const headers = [["Name", "Branch", "Rank", "Division", "Email", "Phone"]];
    const date = `Generated on ${today}`;

    const data = exportData.map(data=> [data.name, data.branch, data.rank, data.division, data.email, data.phone]);

    let content = {
      startY: 70,
      head: headers,
      body: data
    };

    doc.text(title, offset, 50);
    doc.setFontSize(5);
    doc.text(date, 740, 20, {
        styles: { fontSize: 12 },
    });
    doc.autoTable(content);
    doc.save(`user_list_${generated_date}.pdf`)
  }

  return (
    <div className={classes.root}>
    <Grid>
      <div className={classes.package}>
        <Box boxShadow={5} className={classes.row}>
          <Typography variant="h2" className={classes.iconColor}>
            <PeopleIcon className={classes.icon}/>
          </Typography>
        </Box>
          <Typography variant="h4" className={classes.title}>
          User
        </Typography>
      </div>  
      <Card style={{paddingTop: '45px'}}>
        <CardContent>
        <div className={classes.export}>
    <span className={classes.spacer} />
      <Tooltip title="Export as PDF" placement="top">
        <IconButton onClick={exportPDF}>
          <img src="/images/pdf.png" alt="PDF" className={classes.exportIcons}/>
        </IconButton>
      </Tooltip>
      <ExcelFile filename={`user_list_${generated_date}`} element={
        <Tooltip title="Export as XLS" placement="top">
          <IconButton>
            <img src="/images/xls.png" alt="XLS" className={classes.exportIcons}/>
          </IconButton>
        </Tooltip>
        }>
        <ExcelSheet data={exportData} name="user_list">
                <ExcelColumn label="Name" value="name"/>
                <ExcelColumn label="Branch" value="branch"/>
                <ExcelColumn label="Rank" value="rank"/>
                <ExcelColumn label="Division" value="division"/>
                <ExcelColumn label="Email" value="email"/>
                <ExcelColumn label="Phone" value="phone"/>
              </ExcelSheet>
      </ExcelFile>
    </div>
          <UsersToolbar getData={getData} onKeywordChange={keywordChange} onFilterChange={filterChange} onLoadingChange={setLoading} branches={branches} divisions={divisions} ranks={ranks} handleSuccess={handleSuccess} handleError={handleError} handleErrorLogout={handleErrorLogout} onPageChange={setPage} />
          <div className={classes.content}>
            <UsersTable branches={branches} divisions={divisions} users={users} handleSuccess={handleSuccess} handleError={handleError} handleErrorLogout={handleErrorLogout} getData={getData} loading={loading} onLoadingChange={setLoading} size={size} onSizeChange={setSize} page={page} onPageChange={setPage} count={count} pages={pages} ranks={ranks}/>
          </div>
          <Dialog
            onClose={handleFlashClose}
            open={openFlash}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
          >
            <DialogContent>
              <img src={`/images/success.gif?${imgAge}`} className={classes.flash} alt="Success"/>
                <Typography variant="h2" style={{textAlign:'center'}}>
                  Success!
                </Typography>
                <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
            {message}
          </DialogContentText>
            </DialogContent>
          </Dialog>


        <Dialog
          onClose={handleFlashErrClose}
          open={openFlashErr}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          >
          <DialogContent>
              <img src={`/images/err.jpg?${imgAge}`} className={classes.flash} alt="Error"/>
              <Typography variant="h2" style={{textAlign:'center'}}>
                  Error!
              </Typography>
              
              <DialogContentText id="alert-dialog-description" style={{textAlign:'center'}}>
              {message}
              </DialogContentText>
          </DialogContent>
        </Dialog>


        </CardContent>
      </Card>
    </Grid>
    </div>
  );
};

export default UserList;
