import uuid from 'uuid/v1';

export default [
  {
    id: '010290201010',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Willy Mardianto',
    employee_id: 'u068725',
    email: 'willymardianto97@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  },
  {
    id: '010290201011',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Satya Krisna',
    employee_id: 'u068725',
    email: 'satyakrisna@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  },
  {
    id: '010290201012',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Stefanus Anugrah Pribadi',
    employee_id: 'u068725',
    email: 'stefanus@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  },
  {
    id: '010290201013',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Nurul Khotimah',
    employee_id: 'u068725',
    email: 'nurulkhotimah@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  },
  {
    id: '010290201014',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Ronaldo Kristianto',
    employee_id: 'u068725',
    email: 'ronaldokristianto@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  },
  {
    id: '010290201015',
    company_id: 'Bank Central Asia, Tbk',
    branchesuuid: '0810801901',
    divisionuuid: '9100292001',
    titlesuuid: '9120930210',
    rolesuuid: '9120930211',
    identity_number: '127019201901',
    name: 'Nicola',
    employee_id: 'u068725',
    email: 'nicola@gmail.com',
    password: '98183180281092091',
    phone: '082287893991',
    address: 'Jl. Rahmadsyah 338 E / 6',
    created_date: '06/08/2020',
    last_updated: null,
    status: 'Active'
  }
  // {
  //   id: uuid(),
  //   name: 'Cao Yu',
  //   address: {
  //     country: 'USA',
  //     state: 'Bristow',
  //     city: 'Iowa',
  //     street: '1865  Pleasant Hill Road'
  //   },
  //   email: 'cao.yu@devias.io',
  //   avatarUrl: '/images/avatars/avatar_4.png',
  //   phone: '712-351-5711',
  //   createdAt: 1555016400000
  // },
  // {
  //   id: uuid(),
  //   name: 'Alexa Richardson',
  //   address: {
  //     country: 'USA',
  //     state: 'Georgia',
  //     city: 'Atlanta',
  //     street: '4894  Lakeland Park Drive'
  //   },
  //   email: 'alexa.richardson@devias.io',
  //   phone: '770-635-2682',
  //   avatarUrl: '/images/avatars/avatar_2.png',
  //   createdAt: 1555016400000
  // },
  // {
  //   id: uuid(),
  //   name: 'Anje Keizer',
  //   address: {
  //     country: 'USA',
  //     state: 'Ohio',
  //     city: 'Dover',
  //     street: '4158  Hedge Street'
  //   },
  //   email: 'anje.keizer@devias.io',
  //   avatarUrl: '/images/avatars/avatar_5.png',
  //   phone: '908-691-3242',
  //   createdAt: 1554930000000
  // },
  // {
  //   id: uuid(),
  //   name: 'Clarke Gillebert',
  //   address: {
  //     country: 'USA',
  //     state: 'Texas',
  //     city: 'Dallas',
  //     street: '75247'
  //   },
  //   email: 'clarke.gillebert@devias.io',
  //   phone: '972-333-4106',
  //   avatarUrl: '/images/avatars/avatar_6.png',
  //   createdAt: 1554757200000
  // },
  // {
  //   id: uuid(),
  //   name: 'Adam Denisov',
  //   address: {
  //     country: 'USA',
  //     state: 'California',
  //     city: 'Bakerfield',
  //     street: '317 Angus Road'
  //   },
  //   email: 'adam.denisov@devias.io',
  //   phone: '858-602-3409',
  //   avatarUrl: '/images/avatars/avatar_1.png',
  //   createdAt: 1554670800000
  // },
  // {
  //   id: uuid(),
  //   name: 'Ava Gregoraci',
  //   address: {
  //     country: 'USA',
  //     state: 'California',
  //     city: 'Redondo Beach',
  //     street: '2188  Armbrester Drive'
  //   },
  //   email: 'ava.gregoraci@devias.io',
  //   avatarUrl: '/images/avatars/avatar_7.png',
  //   phone: '415-907-2647',
  //   createdAt: 1554325200000
  // },
  // {
  //   id: uuid(),
  //   name: 'Emilee Simchenko',
  //   address: {
  //     country: 'USA',
  //     state: 'Nevada',
  //     city: 'Las Vegas',
  //     street: '1798  Hickory Ridge Drive'
  //   },
  //   email: 'emilee.simchenko@devias.io',
  //   phone: '702-661-1654',
  //   avatarUrl: '/images/avatars/avatar_8.png',
  //   createdAt: 1523048400000
  // },
  // {
  //   id: uuid(),
  //   name: 'Kwak Seong-Min',
  //   address: {
  //     country: 'USA',
  //     state: 'Michigan',
  //     city: 'Detroit',
  //     street: '3934  Wildrose Lane'
  //   },
  //   email: 'kwak.seong.min@devias.io',
  //   avatarUrl: '/images/avatars/avatar_9.png',
  //   phone: '313-812-8947'
  // },
  // {
  //   id: uuid(),
  //   name: 'Merrile Burgett',
  //   address: {
  //     country: 'USA',
  //     state: 'Utah',
  //     city: 'Salt Lake City',
  //     street: '368 Lamberts Branch Road'
  //   },
  //   email: 'merrile.burgett@devias.io',
  //   phone: '801-301-7894',
  //   avatarUrl: '/images/avatars/avatar_10.png',
  //   createdAt: 1522702800000
  // }
];
