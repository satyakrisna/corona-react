import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api-corona.apps.pcf.dti.co.id/dashboard'
    // baseURL: 'https://auth-servercorona.cfapps.io'
  });

export default instance;