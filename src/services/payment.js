import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api-corona.apps.pcf.dti.co.id/trx-company'
  });

export default instance;