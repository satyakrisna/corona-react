import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://resource-bcadashboardcorona.apps.pcf.dti.co.id'
  });

export default instance;