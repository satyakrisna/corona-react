import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://api-corona.apps.pcf.dti.co.id/auth'
    // baseURL: 'https://api-corona.intra.bca.co.id/auth'
  });

export default instance;