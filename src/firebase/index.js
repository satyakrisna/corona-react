import firebase from "firebase/app";
import "firebase/storage";

const firebaseConfig = {
    apiKey: "AIzaSyAKMCiPeD5DlJ7Xx_0I3oA3mSlMY14AtM0",
    authDomain: "corona-upload.firebaseapp.com",
    databaseURL: "https://corona-upload.firebaseio.com",
    projectId: "corona-upload",
    storageBucket: "corona-upload.appspot.com",
    messagingSenderId: "959037150764",
    appId: "1:959037150764:web:55827a96041167ff44e389",
    measurementId: "G-QRBWSNSK7T"
};

firebase.initializeApp(firebaseConfig)

const storage = firebase.storage();

export {storage, firebase as default};