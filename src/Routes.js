import React from 'react';
import { Switch, Redirect } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Main as MainLayout, Minimal as MinimalLayout, Minimal1 as Minimal1Layout } from './layouts';

import {
  Dashboard as DashboardView,
  OnGoingReimburseList as OnGoingReimburseListView,
  FinishedReimburseList as FinishedReimburseListView,
  UserList as UserListView,
  CategoryList as CategoryListView,
  BranchList as BranchListView,
  DivisionList as DivisionListView,
  SignIn as SignInView,
  NotFound as NotFoundView,
  OnGoingVirtualCardRequestList as OnGoingVirtualCardRequestListView,
  FinishedVirtualCardRequestList as FinishedVirtualCardRequestListView,
  VirtualCardList as VirtualCardListView,
  TransactionList as TransactionListView,
  SetVirtualCard as SetVirtualCardView,
  PaymentList as PaymentView,
  BillList as BillView,
  Confirmation as ConfirmationView,
  Notification as NotificationView,
  ChangePassword as ChangePasswordView,
  Home as HomeView,
  Register as RegisterView,
} from './views';

const Routes = () => {

  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/admin/sign-in"
      />
      <RouteWithLayout
        component={DashboardView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/dashboard"
      />
      <RouteWithLayout
        component={NotificationView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/notifications"
      />
      <RouteWithLayout
        component={OnGoingReimburseListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/reimburses-ongoing"
      />
      <RouteWithLayout
        component={FinishedReimburseListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/reimburses-finished"
      />
      <RouteWithLayout
        component={FinishedVirtualCardRequestListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/virtual-card-request-finished"
      />
      <RouteWithLayout
        component={OnGoingVirtualCardRequestListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/virtual-card-request-ongoing"
      />
      <RouteWithLayout
        component={VirtualCardListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/virtual-card"
      />
      <RouteWithLayout
        component={TransactionListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/transactions"
      />
      <RouteWithLayout
        component={SetVirtualCardView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/set-virtual-card"
      />
      <RouteWithLayout
        component={CategoryListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/categories"
      />
      <RouteWithLayout
        component={BillView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/payment"
      />
      <RouteWithLayout
        component={PaymentView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/payment-history"
      />
      <RouteWithLayout
        component={BranchListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/branches"
      />
      <RouteWithLayout
        component={DivisionListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"master"}
        path="/admin/divisions"
      />
      <RouteWithLayout
        component={UserListView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path="/admin/users"
      />
      <RouteWithLayout
        component={ChangePasswordView}
        exact
        layout={MainLayout}
        access={"private"}
        allow={"both"}
        path
        ="/admin/settings"
      />
      <RouteWithLayout
        component={SignInView}
        exact
        layout={MinimalLayout}
        access={"public"}
        allow={"both"}
        path="/admin/sign-in"
      />
      <RouteWithLayout
        component={RegisterView}
        exact
        layout={Minimal1Layout}
        access={"public"}
        allow={"both"}
        path="/register"
      />
      <RouteWithLayout
        component={HomeView}
        exact
        layout={Minimal1Layout}
        access={"public"}
        allow={"both"}
        path="/home"
      />
      <RouteWithLayout
        component={ConfirmationView}
        exact
        layout={MinimalLayout}
        access={"public"}
        allow={"both"}
        path="/confirmation"
      />
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        access={"public"}
        allow={"both"}
        path="/not-found"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;

// export default Routes;
// import React, { Fragment } from 'react';
// import { Switch, Redirect } from 'react-router-dom';

// import { RouteWithLayout } from './components';
// import { Main as MainLayout, Minimal as MinimalLayout } from './layouts';

// import {
//   Dashboard as DashboardView,
//   OnGoingReimburseList as OnGoingReimburseListView,
//   FinishedReimburseList as FinishedReimburseListView,
//   UserList as UserListView,
//   CategoryList as CategoryListView,
//   BranchList as BranchListView,
//   DivisionList as DivisionListView,
//   SignIn as SignInView,
//   NotFound as NotFoundView,
//   OnGoingVirtualCardRequestList as OnGoingVirtualCardRequestListView,
//   FinishedVirtualCardRequestList as FinishedVirtualCardRequestListView,
//   VirtualCardList as VirtualCardListView,
//   SetVirtualCard as SetVirtualCardView
// } from './views';

// const Routes = () => {

// const rank_level = JSON.parse(localStorage.getItem("data")).rank_level

//   return (
//     <Switch>
//       <Redirect
//         exact
//         from="/"
//         to="/sign-in"
//       />
//       <RouteWithLayout
//         component={DashboardView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/dashboard"
//       />
//       <RouteWithLayout
//         component={OnGoingReimburseListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/reimburses-ongoing"
//       />
//       <RouteWithLayout
//         component={FinishedReimburseListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/reimburses-finished"
//       />
//       <RouteWithLayout
//         component={FinishedVirtualCardRequestListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/virtual-card-request-finished"
//       />
//       <RouteWithLayout
//         component={OnGoingVirtualCardRequestListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/virtual-card-request-ongoing"
//       />
//       <RouteWithLayout
//         component={VirtualCardListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/virtual-card"
//       />
//       {rank_level===4
//       ?
//       <Fragment>
//       <RouteWithLayout
//         component={SetVirtualCardView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/set-virtual-card"
//       />
//       <RouteWithLayout
//         component={CategoryListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/categories"
//       />
//       <RouteWithLayout
//         component={BranchListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/branches"
//       />
//       <RouteWithLayout
//         component={DivisionListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path="/divisions"
//       />
//       <RouteWithLayout
//         component={UserListView}
//         exact
//         layout={MainLayout}
//         access={"private"}
//         path
//         ="/users"
//       />
//       </Fragment>
//       :
//       null}
      
//       <RouteWithLayout
//         component={SignInView}
//         exact
//         layout={MinimalLayout}
//         access={"public"}
//         path="/sign-in"
//       />
//       <RouteWithLayout
//         component={NotFoundView}
//         exact
//         layout={MinimalLayout}
//         access={"public"}
//         path="/not-found"
//       />
//       <Redirect to="/not-found" />
//     </Switch>
//   );
// };

// export default Routes;
